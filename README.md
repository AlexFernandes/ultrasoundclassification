# UltrasoundClassification

Thesis project (recreated and cleaned up from old UltrasoundExperiment) to perform pattern classification and machine learning methods with ultrasound data.

## Git Clone Installation
1. Download Git for the operating system
    * https://www.linode.com/docs/development/version-control/how-to-install-git-on-linux-mac-and-windows/
2. Contact me through GitLab or email me at alexanderfernandes@cmail.carleton.ca for me to add your GitLab account to this project
3. Set up your Git environment
    * https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup
3. Using any git bash/cli to clone this repository by entering the following:
>> git clone https://gitlab.com/AlexFernandes/ultrasoundclassification
4. (Optional) I use PyCharm as my IDE due to its built in git interface
    * https://www.jetbrains.com/pycharm/download/

## Project Structure
All python code is located under the [python](python) directory. It is recommended to create a data directory
which is where any of the code in will access.
* data - an external directory that will contain your ultrasound *.zrf files
* python
    * src - the source code for all python related functions / modules
    * scripts - single file scripts designed to make use of the python src modules
* Documents
    * external presentations, documents, etc.
    * Recommend looking at [CMBEC_Ottawa2019.pptx](Documents/CMBEC_Ottawa2019.pptx)

## Authors
* **[Alexander Fernandes](https://gitlab.com/AlexFernandes)** - *Main Author*


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


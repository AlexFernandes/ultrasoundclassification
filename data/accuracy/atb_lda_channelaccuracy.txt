/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/Python/Ultrasound/venv/bin/python /media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/ultrasoundclassification/python/scripts/ATBClassification.py
######################################################################

Trial 0
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[ 14   0 106   0   0]
 [ 14 106   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.8

Trial 1
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0  14   0 106]]
0.9766666666666667

Trial 2
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 3
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0  77  43   0   0]
 [  0 120   0   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.7283333333333334

Trial 4
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 5
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [ 28  92   0   0   0]
 [ 14 106   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0   0 120]]
0.5533333333333333

Trial 6
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0  14  85  21]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.8233333333333334

Trial 7
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0  57   0  63]]
0.905

Trial 8
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0  85  35   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.9416666666666667

Trial 9
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0  77  43   0   0]
 [  0   0 105  15   0]
 [  0   0   0 120   0]
 [  0   0   0  14 106]]
0.88
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 5
Channels [0]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 6000
# features: 142
LDA
[[1094    0  106    0    0]
 [  42 1037  121    0    0]
 [  14  226  839  100   21]
 [   0    0  120 1080    0]
 [   0    0   71   14 1115]]
Class Acc: 86.08 +/- 13.35 %
F1 Macro:  86.14 +/- 4.12 %
Thumb  f1: 79.94 +/- 21.44 %
Index  f1: 87.46 +/- 6.03 %
Middle f1: 70.94 +/- 7.39 %
Ring   f1: 94.82 +/- 5.25 %
Pinky  f1: 97.55 +/- 1.60 %
######################################################################

Trial 0
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [120   0   0   0   0]
 [  0  99  21   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.635

Trial 1
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 2
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 3
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[106  14   0   0   0]
 [  0 120   0   0   0]
 [  0  28  92   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.93

Trial 4
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 5
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 6
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 7
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 8
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 9
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[105  15   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.975
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 5
Channels [2]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 6000
# features: 142
LDA
[[1171   29    0    0    0]
 [ 120 1080    0    0    0]
 [   0  127 1073    0    0]
 [   0    0    0 1200    0]
 [   0    0    0    0 1200]]
Class Acc: 95.40 +/- 10.84 %
F1 Macro:  88.04 +/- 10.43 %
Thumb  f1: 87.49 +/- 8.12 %
Index  f1: 70.30 +/- 25.71 %
Middle f1: 82.40 +/- 18.48 %
Ring   f1: 100.00 +/- 0.00 %
Pinky  f1: 100.00 +/- 0.00 %
######################################################################

Trial 0
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[  0 120   0   0   0]
 [ 42  78   0   0   0]
 [ 99   0  21   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.565

Trial 1
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0  35  85   0]
 [  0   0   0   0 120]]
0.9416666666666667

Trial 2
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0 106  14   0   0]
 [  0   0 105  15   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.9516666666666667

Trial 3
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [ 14 106   0   0   0]
 [  0   0 120   0   0]
 [  0   0  63  57   0]
 [  0   0   0   0 120]]
0.8716666666666667

Trial 4
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[ 43  77   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0  72  48   0]
 [  0   0   0 106  14]]
0.575

Trial 5
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [ 43  77   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.9283333333333333

Trial 6
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [ 14 106   0   0   0]
 [  0   0  15 105   0]
 [  0  14   0 106   0]
 [  0   0   0   0 120]]
0.7783333333333333

Trial 7
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[ 99  21   0   0   0]
 [ 14 106   0   0   0]
 [  0   0  49  71   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.8233333333333334

Trial 8
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [  0   0  14 106   0]
 [  0   0 120   0   0]
 [  0   0  14 106   0]
 [  0   0   0   0 120]]
0.7766666666666666

Trial 9
Train samples: 5400 Test samples: 600
Features: 142 -> 142
[[120   0   0   0   0]
 [ 77  29  14   0   0]
 [  0   0  30  90   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.6983333333333334
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 5
Channels [3]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 6000
# features: 142
LDA
[[ 982  218    0    0    0]
 [ 204  848   42  106    0]
 [  99    0  820  281    0]
 [   0   14  184 1002    0]
 [   0    0    0  106 1094]]
Class Acc: 79.10 +/- 13.48 %
F1 Macro:  77.64 +/- 7.55 %
Thumb  f1: 63.86 +/- 23.04 %
Index  f1: 75.18 +/- 9.27 %
Middle f1: 70.42 +/- 13.87 %
Ring   f1: 82.74 +/- 8.98 %
Pinky  f1: 96.00 +/- 3.53 %
######################################################################

Trial 0
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [120   0   0   0   0]
 [  0  34  86   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.7433333333333333

Trial 1
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 2
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 3
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[ 63  57   0   0   0]
 [  0 120   0   0   0]
 [  0  91  29   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.7533333333333333

Trial 4
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 5
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 6
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 7
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 8
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 9
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0  57   0  63]]
0.905
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 5
Channels [0, 2]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 6000
# features: 284
LDA
[[1143   57    0    0    0]
 [ 120 1080    0    0    0]
 [   0  125 1075    0    0]
 [   0    0    0 1200    0]
 [   0    0   57    0 1143]]
Class Acc: 94.02 +/- 10.00 %
F1 Macro:  89.26 +/- 6.88 %
Thumb  f1: 85.51 +/- 7.39 %
Index  f1: 70.46 +/- 24.72 %
Middle f1: 90.59 +/- 3.61 %
Ring   f1: 100.00 +/- 0.00 %
Pinky  f1: 99.76 +/- 0.73 %
######################################################################

Trial 0
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[ 35   0  85   0   0]
 [120   0   0   0   0]
 [  0   0  63  57   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.5633333333333334

Trial 1
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 2
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0  28  92   0]
 [  0   0   0   0 120]]
0.9533333333333334

Trial 3
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 4
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[ 63  57   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.905

Trial 5
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0  29  91   0   0]
 [  0   0 120   0   0]
 [  0   0   0   0 120]]
0.7516666666666667

Trial 6
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0   0 120   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.8

Trial 7
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0  91  29   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [ 28  92   0   0   0]]
0.7516666666666667

Trial 8
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 9
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0  91   0   0  29]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.9516666666666667
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 5
Channels [0, 3]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 6000
# features: 284
LDA
[[1058   57   85    0    0]
 [ 120 1022   29    0   29]
 [   0   29  994  177    0]
 [   0    0  148 1052    0]
 [  28   92    0    0 1080]]
Class Acc: 86.77 +/- 13.87 %
F1 Macro:  81.73 +/- 10.74 %
Thumb  f1: 74.09 +/- 17.96 %
Index  f1: 74.31 +/- 25.40 %
Middle f1: 75.72 +/- 10.36 %
Ring   f1: 86.42 +/- 3.97 %
Pinky  f1: 98.10 +/- 2.91 %
######################################################################

Trial 0
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [ 28  92   0   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.8

Trial 1
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0  57  63]]
0.905

Trial 2
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 3
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 4
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 5
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0  29  91   0]
 [  0   0   0   0 120]]
0.9516666666666667

Trial 6
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [ 57  63   0   0   0]
 [ 85  35   0   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.705

Trial 7
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 8
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  0   6 114   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.81

Trial 9
Train samples: 5400 Test samples: 600
Features: 284 -> 284
[[120   0   0   0   0]
 [  6 114   0   0   0]
 [  0   0  92  28   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.9433333333333334
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 5
Channels [2, 3]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 6000
# features: 284
LDA
[[1200    0    0    0    0]
 [  63 1023  114    0    0]
 [ 113  127  932   28    0]
 [   0    0   29 1171    0]
 [   0    0    0   57 1143]]
Class Acc: 91.15 +/- 9.97 %
F1 Macro:  89.13 +/- 6.18 %
Thumb  f1: 94.18 +/- 2.87 %
Index  f1: 87.62 +/- 5.90 %
Middle f1: 73.95 +/- 25.36 %
Ring   f1: 94.80 +/- 2.54 %
Pinky  f1: 95.08 +/- 3.61 %
######################################################################

Trial 0
Train samples: 5400 Test samples: 600
Features: 426 -> 426
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0  35   0  85]]
0.9416666666666667

Trial 1
Train samples: 5400 Test samples: 600
Features: 426 -> 426
[[120   0   0   0   0]
 [ 77  43   0   0   0]
 [  0   0 120   0   0]
 [  0   0  42   0  78]
 [  0   0   0   0 120]]
0.6716666666666666

Trial 2
Train samples: 5400 Test samples: 600
Features: 426 -> 426
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0  77  43]
 [  0   0   0   0 120]]
0.9283333333333333

Trial 3
Train samples: 5400 Test samples: 600
Features: 426 -> 426
[[  0  77   0   0  43]
 [  0 120   0   0   0]
 [  0  42  78   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.73

Trial 4
Train samples: 5400 Test samples: 600
Features: 426 -> 426
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0  35  42  43]]
0.8716666666666667

Trial 5
Train samples: 5400 Test samples: 600
Features: 426 -> 426
[[120   0   0   0   0]
 [ 35  85   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.9416666666666667

Trial 6
Train samples: 5400 Test samples: 600
Features: 426 -> 426
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0  77  43   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.9283333333333333

Trial 7
Train samples: 5400 Test samples: 600
Features: 426 -> 426
[[120   0   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
1.0

Trial 8
Train samples: 5400 Test samples: 600
Features: 426 -> 426
[[ 78  42   0   0   0]
 [  0 120   0   0   0]
 [  0   0 120   0   0]
 [  0   0   0  78  42]
 [  0   0   0   0 120]]
0.86

Trial 9
Train samples: 5400 Test samples: 600
Features: 426 -> 426
[[120   0   0   0   0]
 [ 85  35   0   0   0]
 [  0   0  78   0  42]
 [  0   0   0 120   0]
 [  0   0   0   0 120]]
0.7883333333333333
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 5
Channels [0, 2, 3]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 6000
# features: 426
LDA
[[1038  119    0    0   43]
 [ 197 1003    0    0    0]
 [   0   42 1073   43   42]
 [   0    0   42  995  163]
 [   0    0   70   42 1088]]
Class Acc: 86.62 +/- 9.98 %
F1 Macro:  85.46 +/- 3.77 %
Thumb  f1: 86.81 +/- 5.32 %
Index  f1: 86.44 +/- 5.20 %
Middle f1: 88.88 +/- 1.46 %
Ring   f1: 82.65 +/- 8.74 %
Pinky  f1: 82.49 +/- 2.83 %

Process finished with exit code 0

/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/Python/Ultrasound/venv/bin/python /media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/ultrasoundclassification/python/scripts/ATBClassification.py
######################################################################

Trial 0
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[ 7  0 53  0  0]
 [ 7 53  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.8

Trial 1
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  6  0 54]]
0.98

Trial 2
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 3
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 38 22  0  0]
 [ 0 60  0  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.7266666666666667

Trial 4
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 5
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [14 46  0  0  0]
 [ 7 53  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0  0 60]]
0.5533333333333333

Trial 6
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0  7 43 10]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.8233333333333334

Trial 7
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0 28  0 32]]
0.9066666666666666

Trial 8
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 43 17  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.9433333333333334

Trial 9
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 39 21  0  0]
 [ 0  0 53  7  0]
 [ 0  0  0 60  0]
 [ 0  0  0  7 53]]
0.8833333333333333
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 10
Channels [0]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 3000
# features: 142
LDA
[[547   0  53   0   0]
 [ 21 519  60   0   0]
 [  7 113 420  50  10]
 [  0   0  60 540   0]
 [  0   0  34   7 559]]
Class Acc: 86.17 +/- 13.42 %
F1 Macro:  86.19 +/- 4.15 %
Thumb  f1: 79.94 +/- 21.44 %
Index  f1: 87.42 +/- 6.05 %
Middle f1: 71.05 +/- 7.45 %
Ring   f1: 94.80 +/- 5.27 %
Pinky  f1: 97.75 +/- 1.56 %
######################################################################

Trial 0
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [60  0  0  0  0]
 [ 0 50 10  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.6333333333333333

Trial 1
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 2
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 3
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[53  7  0  0  0]
 [ 0 60  0  0  0]
 [ 0 14 46  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.93

Trial 4
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 5
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 6
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 7
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 8
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 9
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[52  8  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.9733333333333334
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 10
Channels [2]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 3000
# features: 142
LDA
[[585  15   0   0   0]
 [ 60 540   0   0   0]
 [  0  64 536   0   0]
 [  0   0   0 600   0]
 [  0   0   0   0 600]]
Class Acc: 95.37 +/- 10.89 %
F1 Macro:  87.98 +/- 10.49 %
Thumb  f1: 87.48 +/- 8.11 %
Index  f1: 70.24 +/- 25.70 %
Middle f1: 82.17 +/- 18.82 %
Ring   f1: 100.00 +/- 0.00 %
Pinky  f1: 100.00 +/- 0.00 %
######################################################################

Trial 0
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[ 0 60  0  0  0]
 [22 38  0  0  0]
 [50  0 10  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.56

Trial 1
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0 17 43  0]
 [ 0  0  0  0 60]]
0.9433333333333334

Trial 2
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0 53  7  0  0]
 [ 0  0 53  7  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.9533333333333334

Trial 3
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 7 53  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0 32 28  0]
 [ 0  0  0  0 60]]
0.87

Trial 4
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[22 38  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0 36 24  0]
 [ 0  0  0 53  7]]
0.5766666666666667

Trial 5
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [21 39  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.93

Trial 6
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 7 53  0  0  0]
 [ 0  0  8 52  0]
 [ 0  7  0 53  0]
 [ 0  0  0  0 60]]
0.78

Trial 7
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[50 10  0  0  0]
 [ 7 53  0  0  0]
 [ 0  0 24 36  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.8233333333333334

Trial 8
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [ 0  0  7 53  0]
 [ 0  0 60  0  0]
 [ 0  0  7 53  0]
 [ 0  0  0  0 60]]
0.7766666666666666

Trial 9
Train samples: 2700 Test samples: 300
Features: 142 -> 142
[[60  0  0  0  0]
 [39 14  7  0  0]
 [ 0  0 14 46  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.6933333333333334
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 10
Channels [3]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 3000
# features: 142
LDA
[[492 108   0   0   0]
 [103 423  21  53   0]
 [ 50   0 409 141   0]
 [  0   7  92 501   0]
 [  0   0   0  53 547]]
Class Acc: 79.07 +/- 13.61 %
F1 Macro:  77.57 +/- 7.67 %
Thumb  f1: 63.79 +/- 23.05 %
Index  f1: 74.97 +/- 9.53 %
Middle f1: 70.29 +/- 14.24 %
Ring   f1: 82.83 +/- 9.02 %
Pinky  f1: 96.00 +/- 3.53 %
######################################################################

Trial 0
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [60  0  0  0  0]
 [ 0 17 43  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.7433333333333333

Trial 1
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 2
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 3
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[32 28  0  0  0]
 [ 0 60  0  0  0]
 [ 0 46 14  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.7533333333333333

Trial 4
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 5
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 6
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 7
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 8
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 9
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0 29  0 31]]
0.9033333333333333
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 10
Channels [0, 2]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 3000
# features: 284
LDA
[[572  28   0   0   0]
 [ 60 540   0   0   0]
 [  0  63 537   0   0]
 [  0   0   0 600   0]
 [  0   0  29   0 571]]
Class Acc: 94.00 +/- 10.00 %
F1 Macro:  89.26 +/- 6.88 %
Thumb  f1: 85.55 +/- 7.41 %
Index  f1: 70.46 +/- 24.72 %
Middle f1: 90.53 +/- 3.62 %
Ring   f1: 100.00 +/- 0.00 %
Pinky  f1: 99.75 +/- 0.74 %
######################################################################

Trial 0
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[17  0 43  0  0]
 [60  0  0  0  0]
 [ 0  0 31 29  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.56

Trial 1
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 2
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0 15 45  0]
 [ 0  0  0  0 60]]
0.95

Trial 3
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 4
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[32 28  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.9066666666666666

Trial 5
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0 14 46  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0  0 60]]
0.7533333333333333

Trial 6
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.8

Trial 7
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 46 14  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [14 46  0  0  0]]
0.7533333333333333

Trial 8
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 9
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 46  0  0 14]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.9533333333333334
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 10
Channels [0, 3]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 3000
# features: 284
LDA
[[529  28  43   0   0]
 [ 60 512  14   0  14]
 [  0  14 497  89   0]
 [  0   0  75 525   0]
 [ 14  46   0   0 540]]
Class Acc: 86.77 +/- 13.91 %
F1 Macro:  81.62 +/- 10.82 %
Thumb  f1: 73.97 +/- 18.15 %
Index  f1: 74.38 +/- 25.44 %
Middle f1: 75.45 +/- 10.50 %
Ring   f1: 86.19 +/- 3.95 %
Pinky  f1: 98.10 +/- 2.90 %
######################################################################

Trial 0
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [14 46  0  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.8

Trial 1
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0 28 32]]
0.9066666666666666

Trial 2
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 3
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 4
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 5
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0 14 46  0]
 [ 0  0  0  0 60]]
0.9533333333333334

Trial 6
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [28 32  0  0  0]
 [43 17  0  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.7066666666666667

Trial 7
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 8
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 0  3 57  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.81

Trial 9
Train samples: 2700 Test samples: 300
Features: 284 -> 284
[[60  0  0  0  0]
 [ 3 57  0  0  0]
 [ 0  0 46 14  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.9433333333333334
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 10
Channels [2, 3]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 3000
# features: 284
LDA
[[600   0   0   0   0]
 [ 31 512  57   0   0]
 [ 57  63 466  14   0]
 [  0   0  14 586   0]
 [  0   0   0  28 572]]
Class Acc: 91.20 +/- 9.94 %
F1 Macro:  89.18 +/- 6.19 %
Thumb  f1: 94.18 +/- 2.87 %
Index  f1: 87.66 +/- 5.90 %
Middle f1: 73.98 +/- 25.37 %
Ring   f1: 94.90 +/- 2.50 %
Pinky  f1: 95.17 +/- 3.53 %
######################################################################

Trial 0
Train samples: 2700 Test samples: 300
Features: 426 -> 426
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0 17  0 43]]
0.9433333333333334

Trial 1
Train samples: 2700 Test samples: 300
Features: 426 -> 426
[[60  0  0  0  0]
 [39 21  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0 22  0 38]
 [ 0  0  0  0 60]]
0.67

Trial 2
Train samples: 2700 Test samples: 300
Features: 426 -> 426
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 39 21]
 [ 0  0  0  0 60]]
0.93

Trial 3
Train samples: 2700 Test samples: 300
Features: 426 -> 426
[[ 0 39  0  0 21]
 [ 0 60  0  0  0]
 [ 0 22 38  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.7266666666666667

Trial 4
Train samples: 2700 Test samples: 300
Features: 426 -> 426
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0 17 22 21]]
0.87

Trial 5
Train samples: 2700 Test samples: 300
Features: 426 -> 426
[[60  0  0  0  0]
 [17 43  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.9433333333333334

Trial 6
Train samples: 2700 Test samples: 300
Features: 426 -> 426
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 39 21  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.93

Trial 7
Train samples: 2700 Test samples: 300
Features: 426 -> 426
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0

Trial 8
Train samples: 2700 Test samples: 300
Features: 426 -> 426
[[40 20  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 38 22]
 [ 0  0  0  0 60]]
0.86

Trial 9
Train samples: 2700 Test samples: 300
Features: 426 -> 426
[[60  0  0  0  0]
 [42 18  0  0  0]
 [ 0  0 38  0 22]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.7866666666666666
Classifying: 1kPRF_FingerFlexions_134channel/ PRF: 1000 frameAvg: 10 timeGroupSize: 10
Channels [0, 2, 3]
levels: [3, 4, 5, 6]
Trials: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Depth range: 320 ~ 1520
Complete Confusion matrix
# samples: 3000
# features: 426
LDA
[[520  59   0   0  21]
 [ 98 502   0   0   0]
 [  0  22 535  21  22]
 [  0   0  22 497  81]
 [  0   0  34  22 544]]
Class Acc: 86.60 +/- 10.11 %
F1 Macro:  85.46 +/- 3.82 %
Thumb  f1: 86.79 +/- 5.33 %
Index  f1: 86.28 +/- 5.28 %
Middle f1: 88.79 +/- 1.46 %
Ring   f1: 82.67 +/- 8.70 %
Pinky  f1: 82.78 +/- 2.72 %

Process finished with exit code 0

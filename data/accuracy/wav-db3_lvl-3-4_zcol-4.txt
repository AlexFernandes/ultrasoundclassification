# zcols: 4
# samples: 2700
# features: 2336
# numZColumns = [4]
# numTrials = 3
# numFrames = 180
# waveletname = 'db3'
# levels = [3,4]

AlexFingerMovement
[[436 104   0   0   0]
 [  5 509  26   0   0]
 [  0  45 331 132  32]
 [  0   0  43 495   2]
 [  0   0   2  27 511]]
Accuracy: 0.8451851851851852
Elapsed Time: 00:05:28.07

AlexRightFingerMovement
[[360 180   0   0   0]
 [  3 333  20 184   0]
 [  0  74 377  89   0]
 [  0  61 170 138 171]
 [  0  25   0  86 429]]
Accuracy: 0.6062962962962963
Elapsed Time: 00:04:53.43

fm_sub1_l
[[520  20   0   0   0]
 [ 84 337 114   5   0]
 [  0 101 244 194   1]
 [  0   1 130 379  30]
 [  0   0   7  56 477]]
Accuracy: 0.7248148148148148
Elapsed Time: 00:05:17.66

fm_sub1_r
[[253  54 143  35  55]
 [  7 421  95   8   9]
 [  0 117 378  45   0]
 [  0  31 190 283  36]
 [  2   0   0  38 500]]
Accuracy: 0.6796296296296296
Elapsed Time: 00:05:03.05

fm_sub2_l
[[354 186   0   0   0]
 [  1 530   9   0   0]
 [  0  27 511   2   0]
 [  0   0  25 514   1]
 [  0   0   0   6 534]]
Accuracy: 0.9048148148148148
Elapsed Time: 00:05:11.71

fm_sub3_l
[[536   0   2   0   2]
 [  0 529   7   4   0]
 [  1  14 524   1   0]
 [  1   0 136 398   5]
 [  0  26   0  52 462]]
Accuracy: 0.9070370370370371
Elapsed Time: 00:05:23.88

fm_sub3_r
[[540   0   0   0   0]
 [  0 493  45   2   0]
 [  1   1 538   0   0]
 [  0   0   0 501  39]
 [  0   0   0 148 392]]
Accuracy: 0.9125925925925926
Elapsed Time: 00:05:11.96

pdfm_sub1_l
[[223 125 106  33  53]
 [ 24 370 122  20   4]
 [ 17 212 248  58   5]
 [ 38  94  11 396   1]
 [ 67   2   1   1 469]]
Accuracy: 0.6318518518518519
Elapsed Time: 00:05:03.86

pdfm_sub1_r
[[358   0   0   2 180]
 [  0 235 223  28  54]
 [  0 109 421   2   8]
 [  0  10   3 428  99]
 [  4  48   3 256 229]]
Accuracy: 0.6188888888888889
Elapsed Time: 00:04:57.31


# zcols: 4
# samples: 1800
# features: 2336
# numZColumns = [4]
# numTrials = 3
# frameStart = 30
# frameEnd = 150
# numFrames = frameEnd - frameStart
# waveletname = 'db3'
# levels = [3,4]

AlexFingerMovement
[[188 169   3   0   0]
 [ 48 280  27   3   2]
 [  4  70 187  68  31]
 [  2   9  76 245  28]
 [  6   3  23  67 261]]
Accuracy: 0.645
Elapsed Time: 00:03:06.70

AlexRightFingerMovement
[[193 138  14  12   3]
 [ 26 104  92 125  13]
 [ 12  56 210  74   8]
 [ 12  73 130  62  83]
 [ 20  46  11  38 245]]
Accuracy: 0.45222222222222225
Elapsed Time: 00:03:18.25

fm_sub2_l
[[204 147   9   0   0]
 [ 18 326  15   1   0]
 [  5  43 284  26   2]
 [  0   0  22 324  14]
 [  0   1   2   5 352]]
Accuracy: 0.8277777777777777
Elapsed Time: 00:03:15.26

# Unsupervised Learning: Independent Component Analysis
# zcols: 4
# samples: 2700
# features: 1000
# numZColumns = [4]
# numTrials = 3
# frameStart = 0
# frameEnd = 180
# numFrames = frameEnd - frameStart
# waveletname = 'db3'
# levels = [3,4]

AlexFingerMovement
[[384 155   1   0   0]
 [  7 531   2   0   0]
 [ 12  76 168 202  82]
 [  0   0 148 380  12]
 [  0   0  17  14 509]]
Accuracy: 0.7303703703703703
Elapsed Time: 00:06:30.23

AlexRightFingerMovement
[[512  26   2   0   0]
 [  6 321 131  82   0]
 [  0 130 336  74   0]
 [  0 172 175  13 180]
 [  0   0   0  67 473]]
Accuracy: 0.6129629629629629
Elapsed Time: 00:06:42.24

fm_sub2_l
[[417 122   1   0   0]
 [  0 533   7   0   0]
 [  0  82 458   0   0]
 [  0   0  22 509   9]
 [  0   0   0  12 528]]
Accuracy: 0.9055555555555556
Elapsed Time: 00:06:46.02


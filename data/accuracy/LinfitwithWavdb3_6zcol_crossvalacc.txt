/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/Python/Ultrasound/venv/bin/python /media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/ultrasoundclassification/python/scripts/ClassifyResolution.py
Resolution: zcols = 6
AlexFingerMovement/FingerMovement/
/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/Python/Ultrasound/venv/lib/python3.5/site-packages/sklearn/discriminant_analysis.py:388: UserWarning: Variables are collinear.
  warnings.warn("Variables are collinear.")
Trial 1
[[42  0  0  0 18]
 [ 1 59  0  0  0]
 [17 10 32  0  1]
 [ 0  0  7 46  7]
 [ 0  0  0  1 59]]
0.7933333333333333
Trial 2
[[60  0  0  0  0]
 [ 0 58  0  2  0]
 [ 0  0 39 20  1]
 [ 0  0  9 48  3]
 [10  3  4  2 41]]
0.82
Trial 3
[[39 17  0  1  3]
 [ 0 53  6  1  0]
 [ 0  1 34 25  0]
 [ 0  0 18 42  0]
 [ 0  0  0  3 57]]
0.75
Complete Confusion matrix
# samples: 900
# features: 13488
[[141  17   0   1  21]
 [  1 170   6   3   0]
 [ 17  11 105  45   2]
 [  0   0  34 136  10]
 [ 10   3   4   6 157]]
Accuracy: 78.77777777777779 +/- 2.884612219054925%
Elapsed Time: 00:04:56.22
AlexRightFingerMovement/RightFingerMotions/
Trial 1
[[58  2  0  0  0]
 [ 8 50  0  2  0]
 [ 2  2 47  9  0]
 [ 0 17  1 42  0]
 [ 1 10  0  9 40]]
0.79
Trial 2
[[56  1  0  3  0]
 [ 2 28  7 23  0]
 [ 0  1 58  1  0]
 [ 0  2  6 52  0]
 [ 0  0  0  0 60]]
0.8466666666666667
Trial 3
[[44 11  0  0  5]
 [ 0 10 38 12  0]
 [ 0  0 58  2  0]
 [ 0 18  2 40  0]
 [ 4  7  0  0 49]]
0.67
Complete Confusion matrix
# samples: 900
# features: 13488
[[158  14   0   3   5]
 [ 10  88  45  37   0]
 [  2   3 163  12   0]
 [  0  37   9 134   0]
 [  5  17   0   9 149]]
Accuracy: 76.88888888888889 +/- 7.365250408522909%
Elapsed Time: 00:09:52.35
fm_sub1_l/FingerMotions/
Trial 1
[[60  0  0  0  0]
 [13 41  6  0  0]
 [ 0 30 30  0  0]
 [ 0  0 15 45  0]
 [ 0  0  0 31 29]]
0.6833333333333333
Trial 2
[[60  0  0  0  0]
 [ 0 42 18  0  0]
 [ 0 10 46  4  0]
 [ 0  0  6 54  0]
 [ 0  0  0  0 60]]
0.8733333333333333
Trial 3
[[49 11  0  0  0]
 [ 0 49 11  0  0]
 [ 0  0 42 18  0]
 [ 0  2  1 18 39]
 [ 0  6  3  0 51]]
0.6966666666666667
Complete Confusion matrix
# samples: 900
# features: 13488
[[169  11   0   0   0]
 [ 13 132  35   0   0]
 [  0  40 118  22   0]
 [  0   2  22 117  39]
 [  0   6   3  31 140]]
Accuracy: 75.11111111111111 +/- 8.659541230399427%
Elapsed Time: 00:14:56.64
fm_sub1_r/FingerMotions/
Trial 1
[[56  3  0  1  0]
 [ 5 55  0  0  0]
 [ 0 23 37  0  0]
 [ 0  1  3 56  0]
 [ 0  0  0 15 45]]
0.83
Trial 2
[[60  0  0  0  0]
 [ 0 55  5  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.9833333333333333
Trial 3
[[60  0  0  0  0]
 [ 0 54  6  0  0]
 [ 0  0 51  8  1]
 [ 0  0  0 58  2]
 [ 0  0  0  1 59]]
0.94
Complete Confusion matrix
# samples: 900
# features: 13488
[[176   3   0   1   0]
 [  5 164  11   0   0]
 [  0  23 148   8   1]
 [  0   1   3 174   2]
 [  0   0   0  16 164]]
Accuracy: 91.77777777777777 +/- 6.454015880647825%
Elapsed Time: 00:20:00.48
fm_sub2_l/FingerMotions/
Trial 1
[[58  1  0  1  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  9 51  0]
 [ 0  0  0  0 60]]
0.9633333333333334
Trial 2
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  2 58  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.9933333333333333
Trial 3
[[22 36  2  0  0]
 [ 0 60  0  0  0]
 [ 0  0 49 11  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.8366666666666667
Complete Confusion matrix
# samples: 900
# features: 13488
[[140  37   2   1   0]
 [  0 180   0   0   0]
 [  0   2 167  11   0]
 [  0   0   9 171   0]
 [  0   0   0   0 180]]
Accuracy: 93.1111111111111 +/- 6.789607163312084%
Elapsed Time: 00:25:04.48
fm_sub2_r/FingerMotions/
Trial 1
[[60  0  0  0  0]
 [25 35  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  9 51  0]
 [ 0  0  0  9 51]]
0.8566666666666667
Trial 2
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0
Trial 3
[[41 19  0  0  0]
 [ 0 19 39  2  0]
 [ 0  0 35 25  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.7166666666666667
Complete Confusion matrix
# samples: 900
# features: 13488
[[161  19   0   0   0]
 [ 25 114  39   2   0]
 [  0   0 155  25   0]
 [  0   0   9 171   0]
 [  0   0   0   9 171]]
Accuracy: 85.77777777777777 +/- 11.567301722346594%
Elapsed Time: 00:30:08.15
fm_sub3_l/FingerMotions/
Trial 1
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0 35 18  7]
 [36  1  0  0 23]]
0.7366666666666667
Trial 2
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  1 59  0]
 [ 0  0  0  0 60]]
0.9966666666666667
Trial 3
[[57  3  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 46 14  0]
 [ 0  0  0 60  0]
 [ 0  0 13  1 46]]
0.8966666666666666
Complete Confusion matrix
# samples: 900
# features: 13488
[[177   3   0   0   0]
 [  0 180   0   0   0]
 [  0   0 166  14   0]
 [  0   0  36 137   7]
 [ 36   1  13   1 129]]
Accuracy: 87.66666666666666 +/- 10.708252269472673%
Elapsed Time: 00:35:10.88
fm_sub3_r/FingerMotions/
Trial 1
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  1 59  0  0]
 [ 0  0  0 59  1]
 [ 0  0  0 22 38]]
0.92
Trial 2
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 34 26]
 [ 0  0  0  1 59]]
0.91
Trial 3
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 46 14]
 [ 0  0  0  0 60]]
0.9533333333333334
Complete Confusion matrix
# samples: 900
# features: 13488
[[180   0   0   0   0]
 [  0 180   0   0   0]
 [  0   1 179   0   0]
 [  0   0   0 139  41]
 [  0   0   0  23 157]]
Accuracy: 92.77777777777777 +/- 1.8525924445036739%
Elapsed Time: 00:40:13.86
fm_sub4_l/FingerMotions/
Trial 1
[[60  0  0  0  0]
 [ 1 59  0  0  0]
 [ 0  5 55  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0 58  2]]
0.7866666666666666
Trial 2
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 57  3  0]
 [ 0  0  1 59  0]
 [ 0  0  1  0 59]]
0.9833333333333333
Trial 3
[[13 47  0  0  0]
 [ 0 56  4  0  0]
 [ 0  0 51  9  0]
 [ 0  0  0 60  0]
 [ 0  0  2  0 58]]
0.7933333333333333
Complete Confusion matrix
# samples: 900
# features: 13488
[[133  47   0   0   0]
 [  1 175   4   0   0]
 [  0   5 163  12   0]
 [  0   0   1 179   0]
 [  0   0   3  58 119]]
Accuracy: 85.44444444444444 +/- 9.117883661746061%
Elapsed Time: 00:45:26.85
fm_sub4_r/FingerMotions/
Trial 1
[[60  0  0  0  0]
 [13 47  0  0  0]
 [ 0  8 51  0  1]
 [ 0  0  5 55  0]
 [ 0  0  0 12 48]]
0.87
Trial 2
[[60  0  0  0  0]
 [ 0 60  0  0  0]
 [ 0  0 60  0  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
1.0
Trial 3
[[60  0  0  0  0]
 [ 0  6 54  0  0]
 [ 0  0  1 59  0]
 [ 0  0  0 60  0]
 [ 0  0  0  0 60]]
0.6233333333333333
Complete Confusion matrix
# samples: 900
# features: 13488
[[180   0   0   0   0]
 [ 13 113  54   0   0]
 [  0   8 112  59   1]
 [  0   0   5 175   0]
 [  0   0   0  12 168]]
Accuracy: 83.1111111111111 +/- 15.621289682930303%
Elapsed Time: 00:50:41.52

Process finished with exit code 0


AlexFingerMovement/FingerMovement/ 78.77777777777779 +/- 2.884612219054925 %
AlexRightFingerMovement/RightFingerMotions/ 76.88888888888889 +/- 7.365250408522909 %
fm_sub1_l/FingerMotions/ 75.11111111111111 +/- 8.659541230399427 %
fm_sub1_r/FingerMotions/ 91.77777777777777 +/- 6.454015880647825 %
fm_sub2_l/FingerMotions/ 93.1111111111111 +/- 6.789607163312084 %
fm_sub2_r/FingerMotions/ 85.77777777777777 +/- 11.567301722346594 %
fm_sub3_l/FingerMotions/ 87.66666666666666 +/- 10.708252269472673 %
fm_sub3_r/FingerMotions/ 92.77777777777777 +/- 1.8525924445036739 %
fm_sub4_l/FingerMotions/ 85.44444444444444 +/- 9.117883661746061 %
fm_sub4_r/FingerMotions/ 83.1111111111111 +/- 15.621289682930303 %
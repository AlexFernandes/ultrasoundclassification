"""Combination of linear fit and wavelet feature extraction methods
"""

"""Classify finger motions using wavelet coefficients as features

"""
import xlsxwriter

from python.src.zrfManager import ZRFData
import python.src.patternClassification as pc
import numpy as np
import time
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import confusion_matrix, accuracy_score


def print_elapsed_time(startTime):
    elapsed_time = time.time()
    hours, rem = divmod(elapsed_time - startTime, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Elapsed Time: {:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))


def write_cnf_worksheet(worksheet, cnf_matrix, zcols, acc, row, col):
    worksheet.write(row, col, 'zcols:')
    worksheet.write(row, col + 1, zcols)
    worksheet.write(row, col + 2, 'acc:')
    worksheet.write(row, col + 3, acc)
    for thumb, index, middle, ring, pinky in cnf_matrix:
        worksheet.write(row + 1, col, thumb)
        worksheet.write(row + 1, col + 1, index)
        worksheet.write(row + 1, col + 2, middle)
        worksheet.write(row + 1, col + 3, ring)
        worksheet.write(row + 1, col + 4, pinky)
        row += 1


if __name__ == "__main__":
    # Time Start
    startTime = time.time()

    numTrials = 3
    frameStart = 0
    frameEnd = 180
    frameGroupSize = 3
    numFrames = frameEnd - frameStart
    waveletname = 'db3'
    levels = [2, 3, 4, 5]
    windowSize = 2  # dsize as in window size along the depth axis
    numSegments = 100

    dataDir = '/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/' \
              'ultrasoundclassification/data/'
    workbook = xlsxwriter.Workbook(dataDir + 'accuracy/combined-linfit-db3_test1.xlsx')

    fingerNames = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    fingerLabel = {'Thumb': 0, 'Index': 1, 'Middle': 2, 'Ring': 3, 'Pinky': 4}

    # numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 40, 50, 60, 70, 80, 90,
    #                100, 110, 120, 127]
    numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

    zrfDir = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/zrfFiles/"

    subjectPaths = ["AlexFingerMovement/FingerMovement/",
                    "AlexRightFingerMovement/RightFingerMotions/",
                    "fm_sub1_l/FingerMotions/",
                    "fm_sub1_r/FingerMotions/",
                    "fm_sub2_l/FingerMotions/",
                    "fm_sub2_r/FingerMotions/",
                    "fm_sub3_l/FingerMotions/",
                    "fm_sub3_r/FingerMotions/",
                    "fm_sub4_l/FingerMotions/",
                    "fm_sub4_r/FingerMotions/"]

    for subjectPath in subjectPaths:
        filePath = zrfDir + subjectPath

        print(subjectPath)
        print('numZcolumns', numZColumns)
        print('numTrials', numTrials)
        print('numFrames', numFrames)
        print('frameGroupSize', frameGroupSize)

        worksheet = workbook.add_worksheet(subjectPath.split('/')[0])
        for zcols in numZColumns:

            samples = pc.DataContainer()  # container to put extracted features and samples to then classify

            for fingerID in range(len(fingerNames)):
                for trial in range(1, numTrials + 1):
                    # print(fingerNames[fingerID], trial, end=' ')
                    # print_elapsed_time(startTime)
                    # load ultrasound .zrf file
                    zrf = ZRFData(filePath + fingerNames[fingerID] + str(trial) + '.zrf')

                    # extract wavelet coefficient features for each frame of the rf data
                    # list of a list: [numFrames][zcols*N]; N is based on the level of wavelet scales
                    exctractedFeatures = []
                    for frame in range(frameStart, frameEnd):  # frames are representative as samples
                        feature_per_sample = []
                        for feature in pc.exctract_linear_fit_segments(zrf.rfsig[:, :, frame],  # rf signal
                                                                       zcols,  # num z columns to average to
                                                                       numSegments):  # num depth segments
                            feature_per_sample.append(feature)
                        for feature in pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
                                                                       zcols,  # num z columns to average to
                                                                       waveletname,  # wavelet
                                                                       levels,  # levels
                                                                       windowSize):  # window size for mean(abs())
                            feature_per_sample.append(feature)
                        exctractedFeatures.append(feature_per_sample)  # num depth segments
                    # obtain difference between frames as features
                    differenceFeatures = []  # same as linearFitFeatures
                    for frame in range(numFrames):
                        frameFeatures = []
                        for feature in range(len(exctractedFeatures[frame])):
                            frameFeatures.append(
                                exctractedFeatures[frame][feature] - exctractedFeatures[frame - 1][feature])
                        differenceFeatures.append(frameFeatures)

                    # group frames into group size and obtain min/max values and add the samples to data set
                    # sample size reduces by frameGroupSize amount i.e. 540 samples / 3 frameGroupSize = 180 samples
                    for frame in range(0, numFrames, frameGroupSize):
                        frameFeatures = []
                        for feature in range(len(exctractedFeatures[frame])):
                            frameFeatures.append(
                                np.min([exctractedFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.max([exctractedFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.min([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.max([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))

                        # each frame is represented as a sample
                        samples.add_sample(np.asarray(frameFeatures), fingerID, trial)

            # Split into test / training data
            lda = LinearDiscriminantAnalysis()
            cnf_matrix = None
            y_pred = []
            y_test = []

            # classify with test samples represented as the current trial number
            for trial in range(1, numTrials + 1):
                samples.test_train_split_by_trial(trial)

                pred = lda.fit(samples.X_train, samples.y_train).predict(samples.X_test)

                for s in range(len(pred)):
                    y_pred.append(pred[s])
                    y_test.append(samples.y_test[s])

            print('# zcols:', zcols)
            print('# samples:', len(samples.X))
            print('# features:', len(samples.X[0]))
            cnf_matrix = confusion_matrix(y_test, y_pred)
            print(cnf_matrix)
            print('Accuracy:', accuracy_score(y_test, y_pred))
            print_elapsed_time(startTime)

            write_cnf_worksheet(worksheet, cnf_matrix, zcols, accuracy_score(y_test, y_pred), (zcols - 1) * 6, 0)
    workbook.close()





/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/Python/Ultrasound/venv/bin/python /media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/ultrasoundclassification/python/scripts/CombinedClassification.py
AlexFingerMovement/FingerMovement/
numZcolumns [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
numTrials 3
numFrames 180
frameGroupSize 3
/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/Python/Ultrasound/venv/lib/python3.5/site-packages/sklearn/discriminant_analysis.py:388: UserWarning: Variables are collinear.
  warnings.warn("Variables are collinear.")
# zcols: 1
# samples: 900
# features: 2248
[[68 52 24  5 31]
 [29 65 36 25 25]
 [33 27 47 31 42]
 [18  7 36 77 42]
 [26 17 25 46 66]]
Accuracy: 0.35888888888888887
Elapsed Time: 00:01:05.02
# zcols: 2
# samples: 900
# features: 4496
[[112  43   1   0  24]
 [ 11 156  11   1   1]
 [  0  59  62  37  22]
 [  1   1  32 125  21]
 [  4   3  37  17 119]]
Accuracy: 0.6377777777777778
Elapsed Time: 00:02:51.15
# zcols: 3
# samples: 900
# features: 6744
[[116  52   0   0  12]
 [  3 174   1   0   2]
 [  0   2 118  57   3]
 [  1   2  37 135   5]
 [  1   1  17  10 151]]
Accuracy: 0.7711111111111111
Elapsed Time: 00:05:27.63
# zcols: 4
# samples: 900
# features: 8992
[[154  18   0   1   7]
 [ 29 137   7   7   0]
 [  3  41  97  34   5]
 [  0   1  13 151  15]
 [  6   8  13  13 140]]
Accuracy: 0.7544444444444445
Elapsed Time: 00:08:56.56
# zcols: 5
# samples: 900
# features: 11240
[[150  26   0   2   2]
 [ 22 153   3   2   0]
 [  6  29  96  49   0]
 [  0   0  16 162   2]
 [  0   1  10  18 151]]
Accuracy: 0.7911111111111111
Elapsed Time: 00:13:18.00
# zcols: 6
# samples: 900
# features: 13488
[[141  17   0   1  21]
 [  1 170   6   3   0]
 [ 17  11 105  45   2]
 [  0   0  34 136  10]
 [ 10   3   4   6 157]]
Accuracy: 0.7877777777777778
Elapsed Time: 00:18:30.14
# zcols: 7
# samples: 900
# features: 15736
[[158  13   1   0   8]
 [ 20 148   6   6   0]
 [ 15   0 113  52   0]
 [  0   0  28 152   0]
 [ 18   1   6   7 148]]
Accuracy: 0.7988888888888889
Elapsed Time: 00:24:35.63
# zcols: 8
# samples: 900
# features: 17984
[[143  24   0   0  13]
 [  9 164   3   4   0]
 [  2   3 145  30   0]
 [  0   0  18 158   4]
 [ 14   2  14   2 148]]
Accuracy: 0.8422222222222222
Elapsed Time: 00:31:33.22
# zcols: 9
# samples: 900
# features: 20232
[[156  23   0   0   1]
 [ 11 165   2   2   0]
 [  5   0 138  37   0]
 [  0   0  21 149  10]
 [  6   6  13   7 148]]
Accuracy: 0.84
Elapsed Time: 00:39:21.98
# zcols: 10
# samples: 900
# features: 22480
[[150  21   1   0   8]
 [ 13 162   4   1   0]
 [  3   1 133  43   0]
 [  0   0  17 158   5]
 [ 11   9   7   7 146]]
Accuracy: 0.8322222222222222
Elapsed Time: 00:48:04.93
# zcols: 11
# samples: 900
# features: 24728
[[144  32   0   0   4]
 [ 17 160   0   3   0]
 [  9  12 123  36   0]
 [  0   0  19 151  10]
 [ 10   2  11  13 144]]
Accuracy: 0.8022222222222222
Elapsed Time: 00:57:42.79
# zcols: 12
# samples: 900
# features: 26976
[[134  26   0   0  20]
 [ 10 164   3   3   0]
 [  4   3 132  40   1]
 [  0   2  22 145  11]
 [ 17   4   6  13 140]]
Accuracy: 0.7944444444444444
Elapsed Time: 01:08:01.90
AlexRightFingerMovement/RightFingerMotions/
numZcolumns [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
numTrials 3
numFrames 180
frameGroupSize 3
# zcols: 1
# samples: 900
# features: 2248
[[ 83  39  17  34   7]
 [ 42  28  50  50  10]
 [  8  33 104  31   4]
 [ 13  37  45  82   3]
 [ 13  12  25  15 115]]
Accuracy: 0.4577777777777778
Elapsed Time: 01:09:04.01
# zcols: 2
# samples: 900
# features: 4496
[[117  39   0  16   8]
 [  5  60  24  85   6]
 [  1  26 121  32   0]
 [  1  53  37  65  24]
 [  2  14   5  27 132]]
Accuracy: 0.55
Elapsed Time: 01:10:48.16
# zcols: 3
# samples: 900
# features: 6744
[[107  34   1   7  31]
 [  8  93  53  26   0]
 [  0  21 140  19   0]
 [  3   7  56 105   9]
 [  7  18   0  15 140]]
Accuracy: 0.65
Elapsed Time: 01:13:25.93
# zcols: 4
# samples: 900
# features: 8992
[[156  18   0   5   1]
 [ 16  77  51  36   0]
 [  0  14 145  21   0]
 [  0  49  57  69   5]
 [  2   1   1  32 144]]
Accuracy: 0.6566666666666666
Elapsed Time: 01:16:55.67
# zcols: 5
# samples: 900
# features: 11240
[[147   2   0   2  29]
 [  1  93  59  27   0]
 [  0  10 136  34   0]
 [  7  29  47  77  20]
 [ 10   3   0  39 128]]
Accuracy: 0.6455555555555555
Elapsed Time: 01:21:15.69
# zcols: 6
# samples: 900
# features: 13488
[[158  14   0   3   5]
 [ 10  88  45  37   0]
 [  2   3 163  12   0]
 [  0  37   9 134   0]
 [  5  17   0   9 149]]
Accuracy: 0.7688888888888888
Elapsed Time: 01:26:24.88
# zcols: 7
# samples: 900
# features: 15736
[[148   3   0   3  26]
 [  0 114  29  37   0]
 [  0   4 163  13   0]
 [  0  44  44  72  20]
 [  0   0   0   7 173]]
Accuracy: 0.7444444444444445
Elapsed Time: 01:32:24.18
# zcols: 8
# samples: 900
# features: 17984
[[134  14   0   6  26]
 [  5 107  61   7   0]
 [  0  13 154  13   0]
 [  2  47  49  82   0]
 [  2   0   0   1 177]]
Accuracy: 0.7266666666666667
Elapsed Time: 01:39:19.37
# zcols: 9
# samples: 900
# features: 20232
[[144  36   0   0   0]
 [  6  95  48  31   0]
 [  0  19 137  24   0]
 [  1  30  59  90   0]
 [  4   0   0   0 176]]
Accuracy: 0.7133333333333334
Elapsed Time: 01:47:07.75
# zcols: 10
# samples: 900
# features: 22480
[[146  16   0   3  15]
 [ 10  91  60  19   0]
 [  0  15 148  17   0]
 [  0  71  27  82   0]
 [  0   1   0   2 177]]
Accuracy: 0.7155555555555555
Elapsed Time: 01:55:43.98
# zcols: 11
# samples: 900
# features: 24728
[[162  14   0   0   4]
 [  1  91  49  39   0]
 [  0   8 154  18   0]
 [  0  16  38 126   0]
 [  4   0   0   0 176]]
Accuracy: 0.7877777777777778
Elapsed Time: 02:05:13.60
# zcols: 12
# samples: 900
# features: 26976
[[145  12   0   0  23]
 [ 13  83  50  34   0]
 [  0  15 148  17   0]
 [  0  30  28 121   1]
 [  3   0   0   2 175]]
Accuracy: 0.7466666666666667
Elapsed Time: 02:15:39.11
fm_sub1_l/FingerMotions/
numZcolumns [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
numTrials 3
numFrames 180
frameGroupSize 3
# zcols: 1
# samples: 900
# features: 2248
[[128  41   2   6   3]
 [ 27  79  29  31  14]
 [ 11  26  73  47  23]
 [ 13  36  47  57  27]
 [  7   6  26  20 121]]
Accuracy: 0.5088888888888888
Elapsed Time: 02:16:41.57
# zcols: 2
# samples: 900
# features: 4496
[[143  35   1   1   0]
 [ 43 105  26   4   2]
 [  3  39  59  67  12]
 [  0   5  38 129   8]
 [  0   0   8  35 137]]
Accuracy: 0.6366666666666667
Elapsed Time: 02:18:25.44
# zcols: 3
# samples: 900
# features: 6744
[[172   8   0   0   0]
 [ 43 103  32   2   0]
 [  0  31 113  36   0]
 [  0   0  27 134  19]
 [  0   0   3  18 159]]
Accuracy: 0.7566666666666667
Elapsed Time: 02:20:59.89
# zcols: 4
# samples: 900
# features: 8992
[[166  14   0   0   0]
 [ 24 120  32   4   0]
 [  1  46  90  43   0]
 [  0   0  23 141  16]
 [  0   0   9  25 146]]
Accuracy: 0.7366666666666667
Elapsed Time: 02:24:29.65
# zcols: 5
# samples: 900
# features: 11240
[[173   7   0   0   0]
 [ 14 133  31   2   0]
 [  0  27 136  17   0]
 [  0   0  40  91  49]
 [  0   2  11  43 124]]
Accuracy: 0.73
Elapsed Time: 02:28:41.86
# zcols: 6
# samples: 900
# features: 13488
[[169  11   0   0   0]
 [ 13 132  35   0   0]
 [  0  40 118  22   0]
 [  0   2  22 117  39]
 [  0   6   3  31 140]]
Accuracy: 0.7511111111111111
Elapsed Time: 02:33:34.57
# zcols: 7
# samples: 900
# features: 15736
[[160  20   0   0   0]
 [ 10 137  33   0   0]
 [  0  39 133   8   0]
 [  0   3  37 110  30]
 [  0   3   2  24 151]]
Accuracy: 0.7677777777777778
Elapsed Time: 02:39:14.76
# zcols: 8
# samples: 900
# features: 17984
[[175   5   0   0   0]
 [ 34 127  19   0   0]
 [  0  34 119  27   0]
 [  0   0  26 128  26]
 [  0   1   3  23 153]]
Accuracy: 0.78
Elapsed Time: 02:45:45.50
# zcols: 9
# samples: 900
# features: 20232
[[176   4   0   0   0]
 [ 19 138  23   0   0]
 [  0  34 125  21   0]
 [  0   0  33 112  35]
 [  0   0   0  42 138]]
Accuracy: 0.7655555555555555
Elapsed Time: 02:53:04.46
# zcols: 10
# samples: 900
# features: 22480
[[147  33   0   0   0]
 [ 11 146  23   0   0]
 [  0  20 136  24   0]
 [  0   0  20 124  36]
 [  0   0   0  24 156]]
Accuracy: 0.7877777777777778
Elapsed Time: 03:01:10.72
# zcols: 11
# samples: 900
# features: 24728
[[179   1   0   0   0]
 [  8 151  21   0   0]
 [  0  26 140  14   0]
 [  0   0  24 108  48]
 [  0   0   0  44 136]]
Accuracy: 0.7933333333333333
Elapsed Time: 03:10:41.67
# zcols: 12
# samples: 900
# features: 26976
[[172   8   0   0   0]
 [ 14 130  36   0   0]
 [  0  39 127  14   0]
 [  0   0  24 128  28]
 [  0   8   0  43 129]]
Accuracy: 0.7622222222222222
Elapsed Time: 03:21:11.72
fm_sub1_r/FingerMotions/
numZcolumns [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
numTrials 3
numFrames 180
frameGroupSize 3
# zcols: 1
# samples: 900
# features: 2248
[[139  12  11  10   8]
 [ 12 105  37  16  10]
 [ 11  47  72  34  16]
 [  5   7  28 117  23]
 [  2   5  35  27 111]]
Accuracy: 0.6044444444444445
Elapsed Time: 03:22:15.39
# zcols: 2
# samples: 900
# features: 4496
[[169   1   0   5   5]
 [  3 132  34  10   1]
 [  1  46 113  19   1]
 [  3  12  35 120  10]
 [  0   3   1   4 172]]
Accuracy: 0.7844444444444445
Elapsed Time: 03:24:01.10
# zcols: 3
# samples: 900
# features: 6744
[[155  11   2  10   2]
 [  1 135  41   3   0]
 [  0  39 118  23   0]
 [  0  12  41  94  33]
 [  0   2   7   7 164]]
Accuracy: 0.74
Elapsed Time: 03:26:36.59
# zcols: 4
# samples: 900
# features: 8992
[[169   8   3   0   0]
 [ 10 133  36   1   0]
 [  0  45 120  13   2]
 [  0   0  12 154  14]
 [  0   0   0   0 180]]
Accuracy: 0.84
Elapsed Time: 03:29:59.01
# zcols: 5
# samples: 900
# features: 11240
[[162   5   0   8   5]
 [  2 164  12   2   0]
 [  0  41 125  14   0]
 [  0   0  19 160   1]
 [  0   0   0  11 169]]
Accuracy: 0.8666666666666667
Elapsed Time: 03:34:06.45
# zcols: 6
# samples: 900
# features: 13488
[[176   3   0   1   0]
 [  5 164  11   0   0]
 [  0  23 148   8   1]
 [  0   1   3 174   2]
 [  0   0   0  16 164]]
Accuracy: 0.9177777777777778
Elapsed Time: 03:39:09.13
# zcols: 7
# samples: 900
# features: 15736
[[176   4   0   0   0]
 [  0 156  18   6   0]
 [  0  28 137  15   0]
 [  0   0  23 154   3]
 [  0   0   0   6 174]]
Accuracy: 0.8855555555555555
Elapsed Time: 03:45:09.41
# zcols: 8
# samples: 900
# features: 17984
[[175   1   2   2   0]
 [  6 146  27   1   0]
 [  0  22 143  15   0]
 [  0   6   3 168   3]
 [  0   0   0   0 180]]
Accuracy: 0.9022222222222223
Elapsed Time: 03:52:01.67
# zcols: 9
# samples: 900
# features: 20232
[[164   7   0   2   7]
 [  1 155  24   0   0]
 [  0  25 138  17   0]
 [  0   3   8 154  15]
 [  0   0   0   2 178]]
Accuracy: 0.8766666666666667
Elapsed Time: 03:59:43.92
# zcols: 10
# samples: 900
# features: 22480
[[170   8   2   0   0]
 [  0 158  22   0   0]
 [  0  19 147  14   0]
 [  0   2  20 146  12]
 [  0   0   1   4 175]]
Accuracy: 0.8844444444444445
Elapsed Time: 04:08:26.42
# zcols: 11
# samples: 900
# features: 24728
[[176   4   0   0   0]
 [  1 157  22   0   0]
 [  0  30 139  11   0]
 [  0   0   8 155  17]
 [  0   0   0   0 180]]
Accuracy: 0.8966666666666666
Elapsed Time: 04:17:47.12
# zcols: 12
# samples: 900
# features: 26976
[[148   5   0   2  25]
 [  3 152  25   0   0]
 [  0  29 133  18   0]
 [  0   1   5 158  16]
 [  0   0   2   2 176]]
Accuracy: 0.8522222222222222
Elapsed Time: 04:27:40.80
fm_sub2_l/FingerMotions/
numZcolumns [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
numTrials 3
numFrames 180
frameGroupSize 3
# zcols: 1
# samples: 900
# features: 2248
[[121  24  26   0   9]
 [ 11 126  27   9   7]
 [  9  12 124  28   7]
 [  7  13  43 109   8]
 [  5  10  15   4 146]]
Accuracy: 0.6955555555555556
Elapsed Time: 04:28:44.89
# zcols: 2
# samples: 900
# features: 4496
[[122  32  26   0   0]
 [  0 172   5   3   0]
 [  2  13 149  16   0]
 [  0   8  19 153   0]
 [  0   2   0   0 178]]
Accuracy: 0.86
Elapsed Time: 04:30:35.37
# zcols: 3
# samples: 900
# features: 6744
[[127  52   1   0   0]
 [  2 177   1   0   0]
 [  0   0 175   4   1]
 [  0   1  10 169   0]
 [  0   0   0   0 180]]
Accuracy: 0.92
Elapsed Time: 04:33:14.21
# zcols: 4
# samples: 900
# features: 8992
[[151  29   0   0   0]
 [  0 176   3   1   0]
 [  0   2 168  10   0]
 [  0   0  12 168   0]
 [  0   0   0   1 179]]
Accuracy: 0.9355555555555556
Elapsed Time: 04:36:42.26
# zcols: 5
# samples: 900
# features: 11240
[[169   8   3   0   0]
 [  0 180   0   0   0]
 [  0   5 166   9   0]
 [  0   0   5 175   0]
 [  0   0   0   0 180]]
Accuracy: 0.9666666666666667
Elapsed Time: 04:41:02.05
# zcols: 6
# samples: 900
# features: 13488
[[140  37   2   1   0]
 [  0 180   0   0   0]
 [  0   2 167  11   0]
 [  0   0   9 171   0]
 [  0   0   0   0 180]]
Accuracy: 0.9311111111111111
Elapsed Time: 04:46:14.54
# zcols: 7
# samples: 900
# features: 15736
[[177   1   0   2   0]
 [  0 180   0   0   0]
 [  0   7 162  11   0]
 [  0   0   5 175   0]
 [  0   0   0   1 179]]
Accuracy: 0.97
Elapsed Time: 04:52:19.96
# zcols: 8
# samples: 900
# features: 17984
[[177   3   0   0   0]
 [  0 178   2   0   0]
 [  0   2 167  11   0]
 [  0   0   2 178   0]
 [  0   0   0   0 180]]
Accuracy: 0.9777777777777777
Elapsed Time: 04:59:17.69
# zcols: 9
# samples: 900
# features: 20232
[[176   4   0   0   0]
 [  0 179   1   0   0]
 [  0   4 162  14   0]
 [  0   0   6 174   0]
 [  0   0   0   2 178]]
Accuracy: 0.9655555555555555
Elapsed Time: 05:07:06.19
# zcols: 10
# samples: 900
# features: 22480
[[179   1   0   0   0]
 [  0 179   1   0   0]
 [  0   1 163  16   0]
 [  0   0   4 176   0]
 [  0   0   0   0 180]]
Accuracy: 0.9744444444444444
Elapsed Time: 05:15:30.95
# zcols: 11
# samples: 900
# features: 24728
[[177   0   0   3   0]
 [  0 180   0   0   0]
 [  0   0 164  16   0]
 [  0   0   1 179   0]
 [  0   0   0   0 180]]
Accuracy: 0.9777777777777777
Elapsed Time: 05:24:26.92
# zcols: 12
# samples: 900
# features: 26976
[[180   0   0   0   0]
 [  0 179   1   0   0]
 [  0   1 169  10   0]
 [  0   0   6 174   0]
 [  0   0   0   1 179]]
Accuracy: 0.9788888888888889
Elapsed Time: 05:34:22.01
fm_sub2_r/FingerMotions/
numZcolumns [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
numTrials 3
numFrames 180
frameGroupSize 3
# zcols: 1
# samples: 900
# features: 2248
[[158   7  10   4   1]
 [ 18  83  50  21   8]
 [  7  19  93  58   3]
 [  6  12  26 122  14]
 [  3  20   6  30 121]]
Accuracy: 0.6411111111111111
Elapsed Time: 05:35:25.46
# zcols: 2
# samples: 900
# features: 4496
[[179   0   0   1   0]
 [  3 172   2   3   0]
 [  0   6 126  48   0]
 [  0   0  16 163   1]
 [  0   0   0  32 148]]
Accuracy: 0.8755555555555555
Elapsed Time: 05:37:11.54
# zcols: 3
# samples: 900
# features: 6744
[[152  13   0  15   0]
 [  3 139  11  26   1]
 [  0   3 146  31   0]
 [  0   0  20 160   0]
 [  0   0   0  17 163]]
Accuracy: 0.8444444444444444
Elapsed Time: 05:39:47.89
# zcols: 4
# samples: 900
# features: 8992
[[180   0   0   0   0]
 [ 15 148  17   0   0]
 [  0   1 141  38   0]
 [  0   0  16 164   0]
 [  0   0   0   5 175]]
Accuracy: 0.8977777777777778
Elapsed Time: 05:43:15.78
# zcols: 5
# samples: 900
# features: 11240
[[178   2   0   0   0]
 [  0 146  34   0   0]
 [  0   0 137  43   0]
 [  0   0  22 158   0]
 [  0   0   0   4 176]]
Accuracy: 0.8833333333333333
Elapsed Time: 05:47:35.74
# zcols: 6
# samples: 900
# features: 13488
[[161  19   0   0   0]
 [ 25 114  39   2   0]
 [  0   0 155  25   0]
 [  0   0   9 171   0]
 [  0   0   0   9 171]]
Accuracy: 0.8577777777777778
Elapsed Time: 05:52:46.46
# zcols: 7
# samples: 900
# features: 15736
[[180   0   0   0   0]
 [  0 157  20   3   0]
 [  0   0 155  25   0]
 [  0   0   3 176   1]
 [  0   0   0  15 165]]
Accuracy: 0.9255555555555556
Elapsed Time: 05:58:42.51
# zcols: 8
# samples: 900
# features: 17984
[[180   0   0   0   0]
 [  0 154  26   0   0]
 [  0   0 158  22   0]
 [  0   0  11 169   0]
 [  0   0   0   7 173]]
Accuracy: 0.9266666666666666
Elapsed Time: 06:05:39.02
# zcols: 9
# samples: 900
# features: 20232
[[180   0   0   0   0]
 [  1 127  49   2   1]
 [  0   0 155  25   0]
 [  0   0   7 172   1]
 [  0   0   0   6 174]]
Accuracy: 0.8977777777777778
Elapsed Time: 06:13:27.58
# zcols: 10
# samples: 900
# features: 22480
[[180   0   0   0   0]
 [  4 139  28   9   0]
 [  0   0 153  27   0]
 [  0   0  13 166   1]
 [  0   0   0   7 173]]
Accuracy: 0.9011111111111111
Elapsed Time: 06:22:06.23
# zcols: 11
# samples: 900
# features: 24728
[[180   0   0   0   0]
 [  0 143  37   0   0]
 [  0   0 161  19   0]
 [  0   0   7 170   3]
 [  0   0   0  18 162]]
Accuracy: 0.9066666666666666
Elapsed Time: 06:31:40.38
# zcols: 12
# samples: 900
# features: 26976
[[164  16   0   0   0]
 [  0 121  54   5   0]
 [  0   0 152  28   0]
 [  0   0  14 162   4]
 [  0   0   0  12 168]]
Accuracy: 0.8522222222222222
Elapsed Time: 06:42:11.80
fm_sub3_l/FingerMotions/
numZcolumns [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
numTrials 3
numFrames 180
frameGroupSize 3
# zcols: 1
# samples: 900
# features: 2248
[[157   5   4   6   8]
 [ 24  82  18  29  27]
 [ 15  17  95  19  34]
 [  9  21  15  99  36]
 [ 11  28   5  89  47]]
Accuracy: 0.5333333333333333
Elapsed Time: 06:43:16.36
# zcols: 2
# samples: 900
# features: 4496
[[140   9   4  27   0]
 [ 16 112   4  45   3]
 [  0   2 169   9   0]
 [  0   0  14 123  43]
 [  0   1   1  49 129]]
Accuracy: 0.7477777777777778
Elapsed Time: 06:45:04.13
# zcols: 3
# samples: 900
# features: 6744
[[180   0   0   0   0]
 [  0 171   9   0   0]
 [  0   0 153  27   0]
 [  0   0  16 133  31]
 [  8   2  26   4 140]]
Accuracy: 0.8633333333333333
Elapsed Time: 06:47:41.28
# zcols: 4
# samples: 900
# features: 8992
[[179   1   0   0   0]
 [  3 150  27   0   0]
 [  0   0 166  13   1]
 [  0   0   3 161  16]
 [  5   0  45   1 129]]
Accuracy: 0.8722222222222222
Elapsed Time: 06:51:08.61
# zcols: 5
# samples: 900
# features: 11240
[[179   1   0   0   0]
 [  0 179   0   0   1]
 [  0   0 179   1   0]
 [  0   3  23 152   2]
 [  7  22   1  13 137]]
Accuracy: 0.9177777777777778
Elapsed Time: 06:55:27.86
# zcols: 6
# samples: 900
# features: 13488
[[177   3   0   0   0]
 [  0 180   0   0   0]
 [  0   0 166  14   0]
 [  0   0  36 137   7]
 [ 36   1  13   1 129]]
Accuracy: 0.8766666666666667
Elapsed Time: 07:00:42.35
# zcols: 7
# samples: 900
# features: 15736
[[180   0   0   0   0]
 [  0 154  25   0   1]
 [  0  10 157  13   0]
 [  0   4  42 120  14]
 [ 37  29  15   0  99]]
Accuracy: 0.7888888888888889
Elapsed Time: 07:06:45.20
# zcols: 8
# samples: 900
# features: 17984
[[180   0   0   0   0]
 [  0 180   0   0   0]
 [  0   2 140   4  34]
 [  0   0  19 161   0]
 [ 47   5   0   0 128]]
Accuracy: 0.8766666666666667
Elapsed Time: 07:13:40.94
# zcols: 9
# samples: 900
# features: 20232
[[177   2   0   0   1]
 [  0 176   4   0   0]
 [  0   0 175   1   4]
 [  0   0  22 158   0]
 [ 57  23   0   0 100]]
Accuracy: 0.8733333333333333
Elapsed Time: 07:21:30.76
# zcols: 10
# samples: 900
# features: 22480
[[169  11   0   0   0]
 [  0 145   2   0  33]
 [  0   2 172   5   1]
 [  0   0  35 142   3]
 [ 51  13   1   0 115]]
Accuracy: 0.8255555555555556
Elapsed Time: 07:30:10.67
# zcols: 11
# samples: 900
# features: 24728
[[178   2   0   0   0]
 [  0 158   0   0  22]
 [  0   0 176   4   0]
 [  0   0  36 142   2]
 [ 49  10   0   0 121]]
Accuracy: 0.8611111111111112
Elapsed Time: 07:39:41.20
# zcols: 12
# samples: 900
# features: 26976
[[179   1   0   0   0]
 [  0 150  10   0  20]
 [  0   1 136  41   2]
 [  0   0  30 149   1]
 [ 60  11   6   0 103]]
Accuracy: 0.7966666666666666
Elapsed Time: 07:50:06.57
fm_sub3_r/FingerMotions/
numZcolumns [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
numTrials 3
numFrames 180
frameGroupSize 3
# zcols: 1
# samples: 900
# features: 2248
[[115  25   2  23  15]
 [ 19  77  46  25  13]
 [ 12  39 111   3  15]
 [  4   6   2  88  80]
 [  7   3   1  58 111]]
Accuracy: 0.5577777777777778
Elapsed Time: 07:51:09.54
# zcols: 2
# samples: 900
# features: 4496
[[180   0   0   0   0]
 [  7 168   1   2   2]
 [  8  12 160   0   0]
 [  0   1   0 116  63]
 [  0   0   0  31 149]]
Accuracy: 0.8588888888888889
Elapsed Time: 07:52:54.92
# zcols: 3
# samples: 900
# features: 6744
[[180   0   0   0   0]
 [  2 153  24   1   0]
 [  0   8 171   1   0]
 [  0   0   0 122  58]
 [  0   0   0  39 141]]
Accuracy: 0.8522222222222222
Elapsed Time: 07:55:31.27
# zcols: 4
# samples: 900
# features: 8992
[[177   0   3   0   0]
 [  1 173   6   0   0]
 [  0  10 170   0   0]
 [  0   1   0 127  52]
 [  0   0   0  16 164]]
Accuracy: 0.9011111111111111
Elapsed Time: 07:58:58.62
# zcols: 5
# samples: 900
# features: 11240
[[180   0   0   0   0]
 [  0 171   9   0   0]
 [  0   9 171   0   0]
 [  0   0   0 153  27]
 [  0   0   0  12 168]]
Accuracy: 0.9366666666666666
Elapsed Time: 08:03:18.37
# zcols: 6
# samples: 900
# features: 13488
[[180   0   0   0   0]
 [  0 180   0   0   0]
 [  0   1 179   0   0]
 [  0   0   0 139  41]
 [  0   0   0  23 157]]
Accuracy: 0.9277777777777778
Elapsed Time: 08:08:29.67
# zcols: 7
# samples: 900
# features: 15736
[[180   0   0   0   0]
 [  0 174   6   0   0]
 [  0   7 173   0   0]
 [  0   0   0 153  27]
 [  0   0   0  16 164]]
Accuracy: 0.9377777777777778
Elapsed Time: 08:14:33.08
# zcols: 8
# samples: 900
# features: 17984
[[180   0   0   0   0]
 [  0 175   5   0   0]
 [  0   8 172   0   0]
 [  0   0   0 142  38]
 [  0   0   0  18 162]]
Accuracy: 0.9233333333333333
Elapsed Time: 08:21:27.40
# zcols: 9
# samples: 900
# features: 20232
[[180   0   0   0   0]
 [  5 170   5   0   0]
 [  0   3 177   0   0]
 [  0   0   0 151  29]
 [  0   0   0   6 174]]
Accuracy: 0.9466666666666667
Elapsed Time: 08:29:16.10
# zcols: 10
# samples: 900
# features: 22480
[[180   0   0   0   0]
 [  0 176   4   0   0]
 [  0   8 172   0   0]
 [  0   0   0 143  37]
 [  0   0   0  29 151]]
Accuracy: 0.9133333333333333
Elapsed Time: 08:38:03.73
# zcols: 11
# samples: 900
# features: 24728
[[180   0   0   0   0]
 [  1 176   3   0   0]
 [  0   7 173   0   0]
 [  0   0   0 146  34]
 [  0   0   0  24 156]]
Accuracy: 0.9233333333333333
Elapsed Time: 08:47:37.17
# zcols: 12
# samples: 900
# features: 26976
[[180   0   0   0   0]
 [  1 173   6   0   0]
 [  0   6 174   0   0]
 [  0   0   0 152  28]
 [  0   0   0  15 165]]
Accuracy: 0.9377777777777778
Elapsed Time: 08:57:50.84
fm_sub4_l/FingerMotions/
numZcolumns [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
numTrials 3
numFrames 180
frameGroupSize 3
# zcols: 1
# samples: 900
# features: 2248
[[55 59 29 14 23]
 [49 99 20  7  5]
 [29 14 76 38 23]
 [18  9 48 67 38]
 [ 6 12 27 64 71]]
Accuracy: 0.4088888888888889
Elapsed Time: 08:58:50.56
# zcols: 2
# samples: 900
# features: 4496
[[149  31   0   0   0]
 [  5 175   0   0   0]
 [  0   1 171   8   0]
 [  3   0   3 162  12]
 [  1   1   1  46 131]]
Accuracy: 0.8755555555555555
Elapsed Time: 09:00:36.82
# zcols: 3
# samples: 900
# features: 6744
[[144  24  11   1   0]
 [ 19 159   1   0   1]
 [  0   4 166   6   4]
 [  1   0  29 138  12]
 [  1   1   5  22 151]]
Accuracy: 0.8422222222222222
Elapsed Time: 09:03:12.21
# zcols: 4
# samples: 900
# features: 8992
[[153  27   0   0   0]
 [  4 165   9   0   2]
 [  0   0 174   6   0]
 [  0   0   2 178   0]
 [  0   0  11  25 144]]
Accuracy: 0.9044444444444445
Elapsed Time: 09:06:42.59
# zcols: 5
# samples: 900
# features: 11240
[[153  27   0   0   0]
 [  0 179   1   0   0]
 [  0   2 170   8   0]
 [  3   0  10 165   2]
 [  0   1   0  29 150]]
Accuracy: 0.9077777777777778
Elapsed Time: 09:10:59.32
# zcols: 6
# samples: 900
# features: 13488
[[133  47   0   0   0]
 [  1 175   4   0   0]
 [  0   5 163  12   0]
 [  0   0   1 179   0]
 [  0   0   3  58 119]]
Accuracy: 0.8544444444444445
Elapsed Time: 09:16:10.15
# zcols: 7
# samples: 900
# features: 15736
[[139  41   0   0   0]
 [  0 179   1   0   0]
 [  0   0 173   7   0]
 [  0   0   5 175   0]
 [  0   0   0  40 140]]
Accuracy: 0.8955555555555555
Elapsed Time: 09:22:09.28
# zcols: 8
# samples: 900
# features: 17984
[[135  45   0   0   0]
 [  1 179   0   0   0]
 [  0  15 160   5   0]
 [  0   0   3 177   0]
 [  0   0   1  38 141]]
Accuracy: 0.88
Elapsed Time: 09:28:49.59
# zcols: 9
# samples: 900
# features: 20232
[[156  24   0   0   0]
 [  3 177   0   0   0]
 [  0   1 173   6   0]
 [  0   0   2 175   3]
 [  0   0   0  54 126]]
Accuracy: 0.8966666666666666
Elapsed Time: 09:36:22.82
# zcols: 10
# samples: 900
# features: 22480
[[143  37   0   0   0]
 [  0 178   2   0   0]
 [  0   1 175   4   0]
 [  0   0   3 175   2]
 [  0   0   5  40 135]]
Accuracy: 0.8955555555555555
Elapsed Time: 09:44:53.75
# zcols: 11
# samples: 900
# features: 24728
[[133  47   0   0   0]
 [  0 179   1   0   0]
 [  0   1 169  10   0]
 [  0   0   5 163  12]
 [  0   0   0  56 124]]
Accuracy: 0.8533333333333334
Elapsed Time: 09:54:28.09
# zcols: 12
# samples: 900
# features: 26976
[[149  31   0   0   0]
 [  7 173   0   0   0]
 [  0   1 175   4   0]
 [  0   0   1 177   2]
 [  0   0   0  60 120]]
Accuracy: 0.8822222222222222
Elapsed Time: 10:04:55.87
fm_sub4_r/FingerMotions/
numZcolumns [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
numTrials 3
numFrames 180
frameGroupSize 3
# zcols: 1
# samples: 900
# features: 2248
[[124  29  17   7   3]
 [ 16  90  21  16  37]
 [ 42  16  45  54  23]
 [  8  11  27 123  11]
 [  4  19   6  22 129]]
Accuracy: 0.5677777777777778
Elapsed Time: 10:05:58.86
# zcols: 2
# samples: 900
# features: 4496
[[169  11   0   0   0]
 [ 13 131  13   6  17]
 [  1  20  66  82  11]
 [  9   2   6 154   9]
 [  0   0  18  47 115]]
Accuracy: 0.7055555555555556
Elapsed Time: 10:07:44.66
# zcols: 3
# samples: 900
# features: 6744
[[178   2   0   0   0]
 [  5 175   0   0   0]
 [  0   1 118  57   4]
 [  0   0  34 145   1]
 [  0   0   0  16 164]]
Accuracy: 0.8666666666666667
Elapsed Time: 10:10:20.60
# zcols: 4
# samples: 900
# features: 8992
[[178   0   0   0   2]
 [  8 142  30   0   0]
 [  0  40  69  61  10]
 [  0   0  13 167   0]
 [  2   0  17   8 153]]
Accuracy: 0.7877777777777778
Elapsed Time: 10:13:45.26
# zcols: 5
# samples: 900
# features: 11240
[[180   0   0   0   0]
 [  1 125  54   0   0]
 [  0  17 103  58   2]
 [  0   0  15 165   0]
 [  0   0   2  36 142]]
Accuracy: 0.7944444444444444
Elapsed Time: 10:18:05.28
# zcols: 6
# samples: 900
# features: 13488
[[180   0   0   0   0]
 [ 13 113  54   0   0]
 [  0   8 112  59   1]
 [  0   0   5 175   0]
 [  0   0   0  12 168]]
Accuracy: 0.8311111111111111
Elapsed Time: 10:23:14.85
# zcols: 7
# samples: 900
# features: 15736
[[180   0   0   0   0]
 [ 14 106  57   3   0]
 [  0  22  98  60   0]
 [  0   0  16 161   3]
 [  0   0   2  14 164]]
Accuracy: 0.7877777777777778
Elapsed Time: 10:29:11.80
# zcols: 8
# samples: 900
# features: 17984
[[180   0   0   0   0]
 [ 17 104  59   0   0]
 [  0  10 110  59   1]
 [  0   0   0 180   0]
 [  0   0   3   3 174]]
Accuracy: 0.8311111111111111
Elapsed Time: 10:36:03.38
# zcols: 9
# samples: 900
# features: 20232
[[171   9   0   0   0]
 [  6 116  58   0   0]
 [  0  11 112  57   0]
 [  0   0   9 171   0]
 [  0   0   0  30 150]]
Accuracy: 0.8
Elapsed Time: 10:43:49.38
# zcols: 10
# samples: 900
# features: 22480
[[139  41   0   0   0]
 [  2 118  60   0   0]
 [  0  35  81  61   3]
 [  0   0  13 167   0]
 [  0   0   7  22 151]]
Accuracy: 0.7288888888888889
Elapsed Time: 10:52:29.45
# zcols: 11
# samples: 900
# features: 24728
[[180   0   0   0   0]
 [ 21  99  60   0   0]
 [  0  50  74  56   0]
 [  0   0  12 167   1]
 [  0   0   0  24 156]]
Accuracy: 0.7511111111111111
Elapsed Time: 11:02:00.29
# zcols: 12
# samples: 900
# features: 26976
[[170  10   0   0   0]
 [ 22  98  60   0   0]
 [  0  37  83  60   0]
 [  0   0  24 156   0]
 [  0   0  10   9 161]]
Accuracy: 0.7422222222222222
Elapsed Time: 11:11:49.78

Process finished with exit code 0

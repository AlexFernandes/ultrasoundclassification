from xlrd import open_workbook
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy import stats
from python.src.zrfPlot import plot_confusion_matrix

def loadScores(excelFile, numZcolumns):
    wb = open_workbook(excelFile)
    f1scores = np.zeros([len(numZcolumns), wb.nsheets - 1, 5])
    classacc = np.zeros([len(numZcolumns), wb.nsheets - 1])
    s = -1
    for sheet in wb.sheets():
        if sheet.name != 'Statistics':
            s = s + 1
            z = -1
            for zcol in numZcolumns:
                z += 1
                cnf_matrix = np.zeros([5, 5], np.uint32)
                for row in range(5):
                    for col in range(5):
                        cnf_matrix[row][col] = sheet.cell((zcol - 1) * 6 + row + 1, col).value
                acc = 0
                for i in range(5):
                    for j in range(5):
                        if i == j:
                            acc = acc + cnf_matrix[i][j]
                acc = acc / np.sum(cnf_matrix)
                f1 = np.zeros([5], np.double)
                for f in range(5):
                    p = cnf_matrix[f][f] / np.sum(cnf_matrix, 0, np.uint32)[f]
                    r = cnf_matrix[f][f] / np.sum(cnf_matrix, 1, np.uint32)[f]
                    f1[f] = 2 * p * r / (p + r)
                    f1scores[z, s, f] = f1[f]
                classacc[z, s] = acc
    return f1scores, classacc


def loadSumCnf(excelFile, numZcolumns):
    wb = open_workbook(excelFile)
    cnf_matricies = np.zeros([len(numZColumns), 5, 5], np.uint32)
    for sheet in wb.sheets():
        if sheet.name != 'Statistics':
            z = -1
            for zcol in numZcolumns:
                z += 1
                for row in range(5):
                    for col in range(5):
                        cnf_matricies[z, row, col] += sheet.cell((zcol - 1) * 6 + row + 1, col).value
    return cnf_matricies


if __name__ == "__main__":
    ###########
    """Setup"""
    ###########

    # dataDir = 'D:/Code/ultrasoundclassification/data/accuracy/'
    dataDir = '/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/ultrasoundclassification/' \
              'data/accuracy/'

    # numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 18, 20, 25, 40, 60, 127]
    # numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 40, 50, 60, 70, 80, 90, 100, 110, 120, 127]
    # numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
    numZColumns = [1, 2, 3, 4, 6, 8, 12, 16, 25, 40, 60, 127]

    linfit_file = 'linfit_segs-100_zcol-1to127_groupsize-3.xlsx'
    linfit_f1, linfit_ca = loadScores(dataDir + linfit_file, numZColumns)
    linfit_cnf_matricies = loadSumCnf(dataDir + linfit_file, numZColumns)

    linfit_fwe_file = 'linfit_fwe-a5.xlsx'
    linfit_fwe_f1, linfit_fwe_ca = loadScores(dataDir + linfit_fwe_file, numZColumns)
    linfit_fwe_cnf_matricies = loadSumCnf(dataDir + linfit_fwe_file, numZColumns)

    nowavelet_file = 'wav-db3_lvl0_fwe-a5.xlsx'
    nowavelet_f1, nowavelet_ca = loadScores(dataDir + nowavelet_file, numZColumns)
    nowavelet_cnf_matricies = loadSumCnf(dataDir + nowavelet_file, numZColumns)

    haar_file = 'wav-haar_lvl2to5_zcol-1to127_groupsize-3_dsize2.xlsx'
    haar_f1, haar_ca = loadScores(dataDir + haar_file, numZColumns)
    haar_cnf_matricies = loadSumCnf(dataDir + haar_file, numZColumns)

    db2_file = 'wav-db2_lvl2to5_groupsize-3_dsize-2.xlsx'
    db2_f1, db2_ca = loadScores(dataDir + db2_file, numZColumns)
    db2_cnf_matricies = loadSumCnf(dataDir + db2_file, numZColumns)

    db3_file = 'wav-db3_lvl2to5_zcol-1to127_groupsize-3_dsize-2.xlsx'
    db3_f1, db3_ca = loadScores(dataDir + db3_file, numZColumns)
    db3_cnf_matricies = loadSumCnf(dataDir + db3_file, numZColumns)

    db4_file = 'wav-db4_lvl2to5_groupsize-3_dsize-2.xlsx'
    db4_f1, db4_ca = loadScores(dataDir + db4_file, numZColumns)
    db4_cnf_matricies = loadSumCnf(dataDir + db4_file, numZColumns)

    db5_file = 'wav-db5_lvl2to5_groupsize-3_dsize-2.xlsx'
    db5_f1, db5_ca = loadScores(dataDir + db5_file, numZColumns)
    db5_cnf_matricies = loadSumCnf(dataDir + db5_file, numZColumns)

    # db3_fwe_file = 'wav-db3_lvl2to5_fwe-a5.xlsx'
    db3_fwe_file = 'wav-db3_lvl1to3_groupsize-3_dsize-2_fwe-a5.xlsx'
    db3_fwe_f1, db3_fwe_ca = loadScores(dataDir + db3_fwe_file, numZColumns)
    db3_fwe_cnf_matricies = loadSumCnf(dataDir + db3_fwe_file, numZColumns)

    # altmethodname = 'Alt wavelet'
    alt_file = 'linfit_seg10_fwe-a5.xlsx'
    alt_f1, alt_ca = loadScores(dataDir + alt_file, numZColumns)
    alt_cnf_matricies = loadSumCnf(dataDir + alt_file, numZColumns)

    font = {'family': 'normal',
            'weight': 'normal',
            'size': 16}
    matplotlib.rc('font', **font)

    colour_linfit = 'blue'
    colour_linfit_fwe = 'cyan'
    colour_nowavelet = 'black'
    colour_haar = 'red'
    colour_db2 = 'green'
    colour_db3 = 'purple'
    colour_db3_fwe = 'magenta'
    colour_db4 = 'brown'
    colour_db5 = 'orange'
    colour_alt = 'pink'

    dashed_line_weight = 0.5

    colour_fingers = ['blue', 'red', 'green', 'purple', 'black']
    finger_names = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']

    ##################################
    """Plot Classification Accuracy"""
    ##################################

    # fig1, ax1 = plt.subplots()
    # ubnd = 75
    # lbnd = 25
    # ax1.plot(numZColumns, np.percentile(linfit_ca, ubnd, axis=1), color=colour_linfit, linestyle='dashed',
    #          alpha=dashed_line_weight, label='linfit +' + str(ubnd) + 'th percentile')
    # ax1.plot(numZColumns, np.mean(linfit_ca, axis=1), color=colour_linfit, label='linfit mean')
    # ax1.plot(numZColumns, np.percentile(linfit_ca, lbnd, axis=1), color=colour_linfit,
    #          linestyle='dashed', alpha=dashed_line_weight, label='linfit -' + str(lbnd) + 'th percentile')

    # ax1.plot(numZColumns, np.percentile(linfit_fwe_ca, ubnd, axis=1), color=colour_linfit_fwe, linestyle='dashed',
    #          alpha=dashed_line_weight, label='linfit fwe +' + str(ubnd) + 'th percentile')
    # ax1.plot(numZColumns, np.mean(linfit_fwe_ca, axis=1), color=colour_linfit_fwe, label='linfit fwe mean')
    # ax1.plot(numZColumns, np.percentile(linfit_fwe_ca, lbnd, axis=1), color=colour_linfit_fwe,
    #          linestyle='dashed', alpha=dashed_line_weight, label='linfit fwe -' + str(lbnd) + 'th percentile')

    # ax1.plot(numZColumns, np.percentile(nowavelet_ca, ubnd, axis=1), color=colour_nowavelet, linestyle='dashed',
    #          alpha=np.sqrt(dashed_line_weight), label='without wavelet +' + str(ubnd) + 'th percentile')
    # ax1.plot(numZColumns, np.mean(nowavelet_ca, axis=1), color=colour_nowavelet, label='without wavelet mean')
    # ax1.plot(numZColumns, np.percentile(nowavelet_ca, lbnd, axis=1), color=colour_nowavelet, linestyle='dashed',
    #          alpha=np.sqrt(dashed_line_weight), label='without wavelet -' + str(lbnd) + 'th percentile')
    #
    # ax1.plot(numZColumns, np.percentile(haar_ca, ubnd, axis=1), color=colour_haar, linestyle='dashed',
    #          alpha=dashed_line_weight, label='haar wavelet +' + str(ubnd) + 'th percentile')
    # ax1.plot(numZColumns, np.mean(haar_ca, axis=1), color=colour_haar, label='haar wavelet mean')
    # ax1.plot(numZColumns, np.percentile(haar_ca, lbnd, axis=1), color=colour_haar, linestyle='dashed',
    #          alpha=dashed_line_weight, label='haar wavelet-' + str(lbnd) + 'th percentile')
    #
    # ax1.plot(numZColumns, np.percentile(db2_ca, ubnd, axis=1), color=colour_db2, linestyle='dashed',
    #          alpha=dashed_line_weight, label='wavelet db2 +' + str(ubnd) + 'th percentile')
    # ax1.plot(numZColumns, np.mean(db2_ca, axis=1), color=colour_db2, label='wavelet db2 mean')
    # ax1.plot(numZColumns, np.percentile(db2_ca, lbnd, axis=1), color=colour_db2, linestyle='dashed',
    #          alpha=dashed_line_weight, label='wavelet db2 -' + str(lbnd) + 'th percentile')

    # ax1.plot(numZColumns, np.percentile(db3_ca, ubnd, axis=1), color=colour_db3, linestyle='dashed',
    #          alpha=dashed_line_weight, label='wavelet db3 +' + str(ubnd) + 'th percentile')
    # ax1.plot(numZColumns, np.mean(db3_ca, axis=1), color=colour_db3, label='wavelet db3 mean')
    # ax1.plot(numZColumns, np.percentile(db3_ca, lbnd, axis=1), color=colour_db3, linestyle='dashed',
    #          alpha=dashed_line_weight, label='wavelet db3 -' + str(lbnd) + 'th percentile')

    # ax1.plot(numZColumns, np.percentile(db3_fwe_ca, ubnd, axis=1), color=colour_db3_fwe, linestyle='dashed',
    #          alpha=dashed_line_weight, label='wavelet db3 fwe +' + str(ubnd) + 'th percentile')
    # ax1.plot(numZColumns, np.mean(db3_fwe_ca, axis=1), color=colour_db3_fwe, label='wavelet db3 fwe mean')
    # ax1.plot(numZColumns, np.percentile(db3_fwe_ca, lbnd, axis=1), color=colour_db3_fwe, linestyle='dashed',
    #          alpha=dashed_line_weight, label='wavelet db3 fwe -' + str(lbnd) + 'th percentile')

    # ax1.plot(numZColumns, np.percentile(db4_ca, ubnd, axis=1), color=colour_db4, linestyle='dashed',
    #          alpha=dashed_line_weight, label='wavelet db4 +' + str(ubnd) + 'th percentile')
    # ax1.plot(numZColumns, np.mean(db4_ca, axis=1), color=colour_db4, label='wavelet db4 mean')
    # ax1.plot(numZColumns, np.percentile(db4_ca, lbnd, axis=1), color=colour_db4, linestyle='dashed',
    #          alpha=dashed_line_weight, label='wavelet db4 -' + str(lbnd) + 'th percentile')
    #
    # ax1.plot(numZColumns, np.percentile(db5_ca, ubnd, axis=1), color=colour_db5, linestyle='dashed',
    #          alpha=dashed_line_weight, label='wavelet db5 +' + str(ubnd) + 'th percentile')
    # ax1.plot(numZColumns, np.mean(db5_ca, axis=1), color=colour_db5, label='wavelet db5 mean')
    # ax1.plot(numZColumns, np.percentile(db5_ca, lbnd, axis=1), color=colour_db5, linestyle='dashed',
    #          alpha=dashed_line_weight, label='wavelet db5 -' + str(lbnd) + 'th percentile')


    # ax1.plot(numZColumns, np.percentile(alt_ca, ubnd, axis=1), color=colour_alt, linestyle='dashed',
    #          alpha=dashed_line_weight, label=altmethodname + ' +' + str(ubnd) + 'th percentile')
    # ax1.plot(numZColumns, np.mean(alt_ca, axis=1), color=colour_alt, label=altmethodname + ' mean')
    # ax1.plot(numZColumns, np.percentile(alt_ca, lbnd, axis=1), color=colour_alt, linestyle='dashed',
    #          alpha=dashed_line_weight, label=altmethodname + ' -' + str(lbnd) + 'th percentile')

    # ax1.set_xscale('log', basex=2)
    # ax1.set_ylim(ymin=0.25, ymax=1)
    # ax1.set_xlim(xmin=0.7, xmax=135)
    # ax1.set_xticklabels(['1', '2', '4', '8', '16', '32', '64', '127'])
    # ax1.set_xticks([1, 2, 4, 8, 16, 32, 64, 127])
    # # ax1.set_xticklabels(['1', '2', '4', '8', '16'])
    # # ax1.set_xticks([1, 2, 4, 8, 16])
    # ax1.legend(loc='lower right')
    # plt.xlabel('Reconstructed RF Columns')
    # plt.ylabel('Classification accuracy')
    # plt.title('Classification accuracy comparison for all arms')
    # plt.grid()

    ###############################################
    """Scatter Plot of Classification Accuracies"""
    ###############################################

    # fig, axscat = plt.subplots()
    # numsubs = 10
    # scatter_weight = 0.25
    # # axscat.plot(numZColumns, np.median(linfit_ca, axis=1), color=colour_linfit, label='linfit median')
    # axscat.plot(numZColumns, np.median(nowavelet_ca, axis=1), color=colour_nowavelet, label='without wavelet median')
    # axscat.plot(numZColumns, np.median(haar_ca, axis=1), color=colour_haar, label='haar median')
    # axscat.plot(numZColumns, np.median(db2_ca, axis=1), color=colour_db2, label='db2 median')
    # axscat.plot(numZColumns, np.median(db3_ca, axis=1), color=colour_db3, label='db3 median')
    # axscat.plot(numZColumns, np.median(db4_ca, axis=1), color=colour_db4, label='db4 median')
    #
    # # axscat.scatter([numZColumns for i in range(numsubs)], [linfit_ca[:, i] for i in range(numsubs)], color=colour_linfit, label='linfit arms', alpha=scatter_weight)
    # axscat.scatter([numZColumns for i in range(numsubs)], [nowavelet_ca[:, i] for i in range(numsubs)], color=colour_nowavelet, label='without wavelet arms', alpha=scatter_weight)
    # axscat.scatter([numZColumns for i in range(numsubs)], [haar_ca[:, i] for i in range(numsubs)], color=colour_haar, label='haar arms', alpha=scatter_weight)
    # axscat.scatter([numZColumns for i in range(numsubs)], [db2_ca[:, i] for i in range(numsubs)], color=colour_db2, label='db2 arms', alpha=scatter_weight)
    # axscat.scatter([numZColumns for i in range(numsubs)], [db3_ca[:, i] for i in range(numsubs)], color=colour_db3, label='db3 arms', alpha=scatter_weight)
    # axscat.scatter([numZColumns for i in range(numsubs)], [db4_ca[:, i] for i in range(numsubs)], color=colour_db4, label='db4 arms', alpha=scatter_weight)
    #
    # axscat.set_xscale('log', basex=2)
    # axscat.set_ylim(ymin=0, ymax=1)
    # axscat.set_xlim(xmin=0.7, xmax=135)
    # axscat.set_xticklabels(['1', '2', '4', '8', '16', '32', '64', '128'])
    # axscat.set_xticks([1, 2, 4, 8, 16, 32, 64, 128])
    # # axscat.set_xticklabels(['1', '2', '4', '8', '16'])
    # # axscat.set_xticks([1, 2, 4, 8, 16])
    # axscat.legend(loc='lower right')
    # plt.xlabel('Reconstructed RF Columns')
    # plt.ylabel('Classification accuracy')
    # plt.title('Classification accuracy comparison across all subject arms')
    # plt.grid()

    #########################################
    """Per subject Classification Accuracy"""
    #########################################

    fig, axsub = plt.subplots()
    for sub in range(10):
        axsub.plot(numZColumns, alt_ca)
    axsub.set_xscale('log', basex=2)
    axsub.set_ylim(ymin=0, ymax=1)
    axsub.set_xlim(xmin=0.7, xmax=135)
    axsub.set_xticklabels(['1', '2', '4', '8', '16', '32', '64', '127'])
    axsub.set_xticks([1, 2, 4, 8, 16, 32, 64, 127])
    # axscat.set_xticklabels(['1', '2', '4', '8', '16'])

    # axscat.set_xticks([1, 2, 4, 8, 16])
    axsub.legend(['sub1 l arm',
                'sub1 r arm',
                'sub2 l arm',
                'sub2 r arm',
                'sub3 l arm',
                'sub3 r arm',
                'sub4 l arm',
                'sub4 r arm',
                'sub5 l arm',
                'sub5 r arm'],
                 loc='lower right')
    plt.xlabel('Reconstructed RF Columns')
    plt.ylabel('Overall classification accuracy')
    plt.title('Classification accuracy using\nlinear regression method')
    plt.grid()

    #####################################################
    """Statistics test for specific number of columns"""
    #####################################################

    # numsubjects = linfit_ca.shape[1]
    # alpha = 0.05
    # p_vals = np.empty([linfit_ca.shape[0]])
    # for c in range(linfit_ca.shape[0]):
    #     col = c - 1  # zero indexed
    #
    #     linfit_data = linfit_ca[col, :]
    #     db3_data = db3_ca[col, :]
    #     diff_data = db3_data - linfit_data
    #
    #     T, p_val = stats.wilcoxon(linfit_data, db3_data)
    #     p_vals[c] = p_val
    #     if p_val < alpha:
    #         dash = '-' * 51
    #         row_format = '{:<8d}{:<10.2f}{:<10.2f}{:>12.2f}'
    #         print('Classification Accuracy Wilcoxon test, num columns =', numZColumns[c])
    #         print(dash)
    #         print('{:<8s}{:<10s}{:<10s}{:>15s}'.format('Subject', 'Linfit', 'Db3', 'Linfit - Db3'))
    #         print(dash)
    #         for sub in range(numsubjects):
    #             print(row_format.format(sub, linfit_data[sub] * 100, db3_data[sub] * 100, diff_data[sub] * 100))
    #         print('col', numZColumns[c], 'T stat =', T, 'p val =', p_val)
    #
    # fig2, ax2 = plt.subplots()
    # ax2.plot(numZColumns, p_vals)
    # ax2.set_title('One tailed Wilcoxon p value')
    # ax2.set_xscale('log', basex=2)
    # # ax2.set_yscale('log', basey=10)
    # ax2.set_ylim(ymin=0, ymax=alpha)
    # ax2.set_xticklabels(['1', '2', '4', '8', '16', '32', '64', '128'])
    # ax2.set_xticks([1, 2, 4, 8, 16, 32, 64, 128])
    # plt.xlabel('Number Averaged Columns')
    # plt.ylabel('P value')
    # plt.grid()

    ##################################
    """Observe column specific data"""
    ##################################
    # linfit_mf1 = np.mean(linfit_f1, axis=2)
    # print(linfit_mf1.shape)
    # db3_mf1 = np.mean(db3_f1, axis=2)
    # alt_mf1 = np.mean(alt_f1, axis=2)
    # diff_mf1 = linfit_mf1 - db3_mf1
    # diff_f1 = linfit_f1 - db3_f1
    #
    # np.set_printoptions(precision=0, suppress=True)
    #
    # numsubjects = 10
    # dash = '-' * 80
    # row_format = '{:<10s}{:<12.2f}{:<12.2f}{:<12.2f}{:<12.2f}{:<12.2f}{:<12.2f}'
    # for column in range(len(numZColumns)):
    #     if 8 > numZColumns[column] > 3:
    #         diff_median_Tf1 = np.median(linfit_f1[column, :, 0]) - np.median(db3_f1[column, :, 0])
    #         diff_median_If1 = np.median(linfit_f1[column, :, 1]) - np.median(db3_f1[column, :, 1])
    #         diff_median_Mf1 = np.median(linfit_f1[column, :, 2]) - np.median(db3_f1[column, :, 2])
    #         diff_median_Rf1 = np.median(linfit_f1[column, :, 3]) - np.median(db3_f1[column, :, 3])
    #         diff_median_Pf1 = np.median(linfit_f1[column, :, 4]) - np.median(db3_f1[column, :, 4])
    #         diff_median_mf1 = np.median(linfit_mf1[column, :]) - np.median(db3_mf1[column, :])
    #
    #         print('\nF1 Score Difference (Linfit-Db3) with N =', numZColumns[column], 'averaged columns')
    #         print(dash)
    #         print('{:<10s}{:<12s}{:<12s}{:<12s}{:<12s}{:<12s}{:<12s}'.format('Subject',
    #                                                                          'Thumb',
    #                                                                          'Index',
    #                                                                          'Middle',
    #                                                                          'Ring',
    #                                                                          'Pinky',
    #                                                                          'Mean F1'))
    #         print(dash)
    #         for sub in range(numsubjects):
    #             print(row_format.format(str(sub),
    #                                     diff_f1[column, sub, 0] * 100,
    #                                     diff_f1[column, sub, 1] * 100,
    #                                     diff_f1[column, sub, 2] * 100,
    #                                     diff_f1[column, sub, 3] * 100,
    #                                     diff_f1[column, sub, 4] * 100,
    #                                     diff_mf1[column, sub] * 100))
    #         print(row_format.format('median',
    #                                 diff_median_Tf1 * 100,
    #                                 diff_median_If1 * 100,
    #                                 diff_median_Mf1 * 100,
    #                                 diff_median_Rf1 * 100,
    #                                 diff_median_Pf1 * 100,
    #                                 diff_median_mf1 * 100))
    #
    #         print('Linear Fit Combined Normalized Confusion Matrix')
    #         print(100 * linfit_cnf_matricies[column, :, :] / linfit_cnf_matricies[column, :, :].sum(axis=1))
    #         print('Wavlet Db3 Combined Normalized Confusion Matrix')
    #         print(100 * db3_cnf_matricies[column, :, :] / db3_cnf_matricies[column, :, :].sum(axis=1))

    #################
    "Print F1 Scores"
    #################

    templatetitle = '{0:10s} {1:5s} {2:1s} {3:5s}'
    template = '{0:10s} {1:5.2f} {2:1s} {3:5.2f} {4:5.2f}'
    for N in range(len(numZColumns)):
        print(templatetitle.format('N='+str(numZColumns[N]), 'DWT', ' ', 'LR'))
        for f in range(len(finger_names)):
            dwtf1 = np.median(db3_fwe_f1[N, :, f]) * 100
            lrf1 = np.median(alt_f1[N, :, f]) * 100
            print(template.format(finger_names[f], dwtf1, '>' if dwtf1 > lrf1 else '<', lrf1, dwtf1-lrf1))


    #######################
    "Plot Confusion Matrix"
    #######################

    method_cnf_matricies = alt_cnf_matricies
    resolutions = [0, 3, 5, 7, -1]
    cnf_matricies = []
    for N in resolutions:
        fig, ax = plt.subplots()
        plot_confusion_matrix(method_cnf_matricies[N, :, :], finger_names, normalize=False)
        plt.clim(0, 1800)
        ax.set_title('All subjects N = ' + str(N+1) + ' columns')
        cnf_matricies.append(method_cnf_matricies[N, :, :])

    for cnf_matrix in cnf_matricies:
        f1 = np.zeros([5], np.double)
        for f in range(5):
            p = cnf_matrix[f][f] / np.sum(cnf_matrix, 0, np.uint32)[f]
            r = cnf_matrix[f][f] / np.sum(cnf_matrix, 1, np.uint32)[f]
            f1[f] = 2 * p * r / (p + r)
        print(f1)


    ###############
    "Plot Box Plot"
    ###############
    figLRbp, axLRbp = plt.subplots()
    linfit_data = []
    linfit_pos = []
    wid = []
    i = 0
    print('LR CA')
    print(alt_ca)
    for z in numZColumns:
        if z in [1, 2, 3, 4, 6, 8, 12, 16, 25, 60, 127]:
            print('LR median N =', z, np.median(alt_ca[i, :] * 100))
            linfit_data.append(alt_ca[i, :] * 100)
            linfit_pos.append(z)
            wid.append(z/8)
        i += 1
    box = axLRbp.boxplot(linfit_data , positions=linfit_pos, widths=wid)
    axLRbp.set_xscale('log', basex=2)
    axLRbp.set_ylim(ymin=30, ymax=100)
    axLRbp.set_xticklabels(['1', '2', '3', '4', '6', '8', '12', '16', '25', '60', '127'])
    axLRbp.set_xticks([1, 2, 3, 4, 6, 8, 12, 16, 25, 60, 127])
    axLRbp.set_xlim(xmin=0.7, xmax=145)
    # axLRbp.set_title('ENV-LR Method')
    # axLRbp.set_ylabel('Classification Accuracy (%)')
    # axLRbp.set_xlabel('Number of reconstructed RF signals (N)')
    plt.grid()

    figDWTbp, axDWTbp = plt.subplots()
    db3_data = []
    db3_pos = []
    wid = []
    i = 0
    for z in numZColumns:
        if z in [1, 2, 3, 4, 6, 8, 12, 16, 25, 40, 60, 127]:
            print('DWT median N =', z, np.median(db3_fwe_ca[i, :] * 100))
            db3_data.append(db3_fwe_ca[i, :] * 100)
            db3_pos.append(z)
            wid.append(z/8)
        i += 1
    box = axDWTbp.boxplot(db3_data, positions=db3_pos, widths=wid)
    axDWTbp.set_xscale('log', basex=2)
    axDWTbp.set_ylim(ymin=30, ymax=100)
    axDWTbp.set_xticklabels(['1', '2', '3', '4', '6', '8', '12', '16', '25', '40', '60', '127'])
    axDWTbp.set_xticks([1, 2, 3, 4, 6, 8, 12, 16, 25, 40, 60, 127])
    axDWTbp.set_xlim(xmin=0.7, xmax=145)
    axDWTbp.set_title('DWT-MAV Method')
    axDWTbp.set_ylabel('Classification Accuracy (%)')
    axDWTbp.set_xlabel('Number of reconstructed RF signals (N)')

    plt.show()

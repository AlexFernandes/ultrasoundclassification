"""Basic script for plotting data relating to extracting wavelet coefficients from data

"""

from python.src.zrfManager import ZRFData
from python.src.zrfPlot import plot_confusion_matrix
import python.src.signalProcessor as sigproc
import numpy as np
from matplotlib import pyplot as plt
import matplotlib
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import confusion_matrix, accuracy_score
from pywt._dwt import dwt
from scipy.signal import hilbert
import scipy.fftpack
from scipy.signal import freqz
from skimage import exposure
import matplotlib.ticker as mtick


def MmodePlotWaveletDiff(dataDir, fingername1, fingername2, trial, zcols, waveletname, zcol):
    W = 2  # window size
    zrf1 = ZRFData(dataDir + fingername1 + str(trial) + '.zrf')
    sig = sigproc.average_into_zcols(zrf1.rfsig, zcols)
    sig1_1 = np.zeros([760, sig.shape[1], sig.shape[2]])
    sig1_2 = np.zeros([382, sig.shape[1], sig.shape[2]])
    sig1_3 = np.zeros([193, sig.shape[1], sig.shape[2]])
    sig1_4 = np.zeros([99, sig.shape[1], sig.shape[2]])
    sig1_5 = np.zeros([52, sig.shape[1], sig.shape[2]])
    meanabs1_1 = np.zeros([380, sig.shape[1], sig.shape[2]])
    meanabs1_2 = np.zeros([191, sig.shape[1], sig.shape[2]])
    meanabs1_3 = np.zeros([96, sig.shape[1], sig.shape[2]])
    meanabs1_4 = np.zeros([49, sig.shape[1], sig.shape[2]])
    meanabs1_5 = np.zeros([26, sig.shape[1], sig.shape[2]])

    for z in range(zcols):
        for frame in range(sig.shape[2]):
            (a1, sig1_1[:, z, frame]) = dwt(sig[:, z, frame], waveletname, 'zero')  # level 1
            (a2, sig1_2[:, z, frame]) = dwt(a1, waveletname, 'zero')  # level 2
            (a3, sig1_3[:, z, frame]) = dwt(a2, waveletname, 'zero')  # level 3
            (a4, sig1_4[:, z, frame]) = dwt(a3, waveletname, 'zero')  # level 4
            (a5, sig1_5[:, z, frame]) = dwt(a4, waveletname, 'zero')  # level 4
            meanabs1_1[:, z, frame] = np.convolve(np.abs(sig1_1[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs1_2[:, z, frame] = np.convolve(np.abs(sig1_2[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs1_3[:, z, frame] = np.convolve(np.abs(sig1_3[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs1_4[:, z, frame] = np.convolve(np.abs(sig1_4[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs1_5[:, z, frame] = np.convolve(np.abs(sig1_5[:, z, frame]), np.ones(W) / W, mode='valid')[::W]

    zrf2 = ZRFData(dataDir + fingername2 + str(trial) + '.zrf')
    sig = sigproc.average_into_zcols(zrf2.rfsig, zcols)
    sig2_1 = np.zeros([760, sig.shape[1], sig.shape[2]])
    sig2_2 = np.zeros([382, sig.shape[1], sig.shape[2]])
    sig2_3 = np.zeros([193, sig.shape[1], sig.shape[2]])
    sig2_4 = np.zeros([99, sig.shape[1], sig.shape[2]])
    sig2_5 = np.zeros([52, sig.shape[1], sig.shape[2]])
    meanabs2_1 = np.zeros([380, sig.shape[1], sig.shape[2]])
    meanabs2_2 = np.zeros([191, sig.shape[1], sig.shape[2]])
    meanabs2_3 = np.zeros([96, sig.shape[1], sig.shape[2]])
    meanabs2_4 = np.zeros([49, sig.shape[1], sig.shape[2]])
    meanabs2_5 = np.zeros([26, sig.shape[1], sig.shape[2]])
    fig, ax = plt.subplots(ncols=4)


    for z in range(zcols):
        for frame in range(sig.shape[2]):
            (a1, sig2_1[:, z, frame]) = dwt(sig[:, z, frame], waveletname, 'zero')  # level 1
            (a2, sig2_2[:, z, frame]) = dwt(a1, waveletname, 'zero')  # level 2
            (a3, sig2_3[:, z, frame]) = dwt(a2, waveletname, 'zero')  # level 3
            (a4, sig2_4[:, z, frame]) = dwt(a3, waveletname, 'zero')  # level 4
            (a5, sig2_5[:, z, frame]) = dwt(a4, waveletname, 'zero')  # level 5
            meanabs2_1[:, z, frame] = np.convolve(np.abs(sig2_1[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs2_2[:, z, frame] = np.convolve(np.abs(sig2_2[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs2_3[:, z, frame] = np.convolve(np.abs(sig2_3[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs2_4[:, z, frame] = np.convolve(np.abs(sig2_4[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs2_5[:, z, frame] = np.convolve(np.abs(sig2_5[:, z, frame]), np.ones(W) / W, mode='valid')[::W]

    meanabs2 = meanabs2_2 - meanabs1_2
    meanabs3 = meanabs2_3 - meanabs1_3
    meanabs4 = meanabs2_4 - meanabs1_4
    meanabs5 = meanabs2_5 - meanabs1_5

    im = ax[0].imshow(meanabs2[:, zcol, :], aspect='auto')
    ax[0].set_xticks([0, 90, 180])
    ax[0].set_xticklabels(['0', '3', '6'])
    ax[0].set_xlabel('Time (seconds)')
    ax[0].set_ylabel('Axial direction (i-axis in millimeters)')
    ax[0].set_yticks([0, meanabs2.shape[0]])
    ax[0].set_yticklabels(['0', '35'])
    ax[0].set_title('MAV2')
    plt.colorbar(im, ax=ax[0])
    im = ax[1].imshow(meanabs3[:, zcol, :], aspect='auto')
    ax[1].set_xticks([0, 90, 180])
    ax[1].set_xticklabels(['0', '3', '6'])
    ax[1].set_xlabel('Time (seconds)')
    ax[1].set_title('MAV3')
    ax[1].get_yaxis().set_visible(False)
    plt.colorbar(im, ax=ax[1])
    im = ax[2].imshow(meanabs4[:, zcol, :], aspect='auto')
    ax[2].set_xticks([0, 90, 180])
    ax[2].set_xticklabels(['0', '3', '6'])
    ax[2].set_xlabel('Time (seconds)')
    ax[2].set_title('MAV4')
    ax[2].get_yaxis().set_visible(False)
    plt.colorbar(im, ax=ax[2])
    im = ax[3].imshow(meanabs5[:, zcol, :], aspect='auto')
    ax[3].set_xticks([0, 90, 180])
    ax[3].set_xticklabels(['0', '3', '6'])
    ax[3].set_xlabel('Time (seconds)')
    ax[3].set_title('MAV5')
    ax[3].get_yaxis().set_visible(False)
    plt.colorbar(im, ax=ax[3])


def MmodePlotWavelet(dataDir, fingername, trial, zcols, waveletname, zcol=None, lvl=None):
    W = 2  # window size
    zrf = ZRFData(dataDir + fingername + str(trial) + '.zrf')
    sig = sigproc.average_into_zcols(zrf.rfsig, zcols)
    sig1 = np.zeros([760, sig.shape[1], sig.shape[2]])
    sig2 = np.zeros([382, sig.shape[1], sig.shape[2]])
    sig3 = np.zeros([193, sig.shape[1], sig.shape[2]])
    sig4 = np.zeros([99, sig.shape[1], sig.shape[2]])
    sig5 = np.zeros([52, sig.shape[1], sig.shape[2]])
    meanabs1 = np.zeros([380, sig.shape[1], sig.shape[2]])
    meanabs2 = np.zeros([191, sig.shape[1], sig.shape[2]])
    meanabs3 = np.zeros([96, sig.shape[1], sig.shape[2]])
    meanabs4 = np.zeros([49, sig.shape[1], sig.shape[2]])
    meanabs5 = np.zeros([26, sig.shape[1], sig.shape[2]])

    for z in range(zcols):
        for frame in range(sig.shape[2]):
            (a1, sig1[:, z, frame]) = dwt(sig[:, z, frame], waveletname, 'zero')  # level 1
            (a2, sig2[:, z, frame]) = dwt(a1, waveletname, 'zero')  # level 2
            (a3, sig3[:, z, frame]) = dwt(a2, waveletname, 'zero')  # level 3
            (a4, sig4[:, z, frame]) = dwt(a3, waveletname, 'zero')  # level 4
            (a5, sig5[:, z, frame]) = dwt(a4, waveletname, 'zero')  # level 4
            meanabs1[:, z, frame] = np.convolve(np.abs(sig1[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs2[:, z, frame] = np.convolve(np.abs(sig2[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs3[:, z, frame] = np.convolve(np.abs(sig3[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs4[:, z, frame] = np.convolve(np.abs(sig4[:, z, frame]), np.ones(W) / W, mode='valid')[::W]
            meanabs5[:, z, frame] = np.convolve(np.abs(sig5[:, z, frame]), np.ones(W) / W, mode='valid')[::W]

    if zcol is None:
        fig, ax = plt.subplots(ncols=5, nrows=zcols)
        for z in range(zcols):
            ax[z, 0].imshow(meanabs1[:, z, :], aspect='auto')
            ax[z, 0].set_xticks([0, 180])
            ax[z, 0].set_yticks([0, 0])
            ax[z, 0].get_xaxis().set_visible(False)
            ax[z, 1].imshow(meanabs2[:, z, :], aspect='auto')
            ax[z, 1].set_xticks([0, 180])
            ax[z, 1].get_yaxis().set_visible(False)
            ax[z, 1].get_xaxis().set_visible(False)
            ax[z, 2].imshow(meanabs3[:, z, :], aspect='auto')
            ax[z, 2].set_xticks([0, 180])
            ax[z, 2].get_yaxis().set_visible(False)
            ax[z, 2].get_xaxis().set_visible(False)
            ax[z, 3].imshow(meanabs4[:, z, :], aspect='auto')
            ax[z, 3].set_xticks([0, 180])
            ax[z, 3].get_yaxis().set_visible(False)
            ax[z, 3].get_xaxis().set_visible(False)
            ax[z, 4].imshow(meanabs5[:, z, :], aspect='auto')
            ax[z, 4].set_xticks([0, 180])
            ax[z, 4].get_yaxis().set_visible(False)
            ax[z, 4].get_xaxis().set_visible(False)
            ax[z, 0].set_ylabel('Column ' + str(z + 1))
        ax[0, 0].set_title('Wavelet 1')
        ax[0, 1].set_title('Wavelet 2')
        ax[0, 2].set_title('Wavelet 3')
        ax[0, 3].set_title('Wavelet 4')
        ax[0, 4].set_title('Wavelet 5')
        ax[-1, 0].set_xlabel('Frame #')
        ax[-1, 1].set_xlabel('Frame #')
        ax[-1, 2].set_xlabel('Frame #')
        ax[-1, 3].set_xlabel('Frame #')
        ax[-1, 4].set_xlabel('Frame #')
        ax[-1, 0].get_xaxis().set_visible(True)
        ax[-1, 1].get_xaxis().set_visible(True)
        ax[-1, 2].get_xaxis().set_visible(True)
        ax[-1, 3].get_xaxis().set_visible(True)
        ax[-1, 4].get_xaxis().set_visible(True)
    else:
        if lvl is None:
            fig, ax = plt.subplots(ncols=4)
            im = ax[0].imshow(meanabs2[:, zcol, :], aspect='auto')
            ax[0].set_xticks([0, 90, 180])
            ax[0].set_xticklabels(['0', '3', '6'])
            ax[0].set_xlabel('Time (seconds)')
            ax[0].set_ylabel('Axial direction (i-axis in millimeters)')
            ax[0].set_yticks([0, meanabs2.shape[0]])
            ax[0].set_yticklabels(['0', '35'])
            ax[0].set_title('MAV2')
            plt.colorbar(im, ax=ax[0])
            im = ax[1].imshow(meanabs3[:, zcol, :], aspect='auto')
            ax[1].set_xticks([0, 90, 180])
            ax[1].set_xticklabels(['0', '3', '6'])
            ax[1].set_xlabel('Time (seconds)')
            ax[1].set_title('MAV3')
            ax[1].get_yaxis().set_visible(False)
            plt.colorbar(im, ax=ax[1])
            im = ax[2].imshow(meanabs4[:, zcol, :], aspect='auto')
            ax[2].set_xticks([0, 90, 180])
            ax[2].set_xticklabels(['0', '3', '6'])
            ax[2].set_xlabel('Time (seconds)')
            ax[2].set_title('MAV4')
            ax[2].get_yaxis().set_visible(False)
            plt.colorbar(im, ax=ax[2])
            im = ax[3].imshow(meanabs5[:, zcol, :], aspect='auto')
            ax[3].set_xticks([0, 90, 180])
            ax[3].set_xticklabels(['0', '3', '6'])
            ax[3].set_xlabel('Time (seconds)')
            ax[3].set_title('MAV5')
            ax[3].get_yaxis().set_visible(False)
            plt.colorbar(im, ax=ax[3])
        else:
            fig, ax = plt.subplots()
            if lvl == 1:
                ax.imshow(meanabs1[:, zcol, :], aspect='auto')
            elif lvl == 2:
                ax.imshow(meanabs2[:, zcol, :], aspect='auto')
            elif lvl == 3:
                ax.imshow(meanabs3[:, zcol, :], aspect='auto')
            elif lvl == 4:
                ax.imshow(meanabs4[:, zcol, :], aspect='auto')
            elif lvl == 5:
                ax.imshow(meanabs5[:, zcol, :], aspect='auto')
            ax.set_xticks([0, 180])
            ax.get_yaxis().set_visible(False)
            ax.set_xlabel('Frame')
            ax.set_title('Wavelet ' + str(lvl))


def upsample(x, I):
    xnew = [0] * (I * len(x))
    xnew[::I] = x
    return xnew


if __name__ == "__main__":
    # dataDir = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/zrfFiles/" \
    dataDir = "D:/Code/zrfFiles/" \
              "AlexRightFingerMovement/RightFingerMotions/"
              # "AlexFingerMovement/FingerMovement/"
              # "fm_sub3_l/FingerMotions/"

    """Single-frame plot wavelet coefficients of first 4 levels"""
    zcols = 6  # columns to average to down from 127
    zcol = 5  # column to look at (start at 0)
    frame = 50  # 180 total
    waveletname = 'db3'  # wavelet name following PyWavelets convention
    maxlvl = 4  # wavelet level
    finger = 'Pinky'
    trial = 1  # 3 total
    maxdepth = 35  # mm

    """load ultrasound data"""

    font = {'family': 'normal',
            'weight': 'normal',
            'size': 16}
    matplotlib.rc('font', **font)

    zrf = ZRFData(dataDir + finger + str(trial) + '.zrf')

    """Plot original data"""
    f = 0
    fig, ax_bmode = plt.subplots()
    image = np.abs(hilbert(zrf.rfsig[:, 126:0:-1, f], axis=0))
    im = ax_bmode.imshow(image/np.max(image), aspect='auto', cmap='viridis')
    # ax_bmode.set_title("Hilbert transform envelope of RF ultrasound data")
    ax_bmode.set_xlabel("Lateral Direction (j-axis) (mm)")
    ax_bmode.set_ylabel("Axial Direction (i-axis) (mm)")
    ax_bmode.set_yticklabels(['0', '5', '10', '15', '20', '25', '30', '35'])
    ax_bmode.set_yticks([0, 216, 432, 649, 865, 1082, 1298, 1515])
    ax_bmode.set_xticklabels(['0', '5', '10', '15', '20', '25', '30', '35', '40'])
    ax_bmode.set_xticks([0, 15, 31, 47, 63, 78, 94, 110, 126])
    # fig.colorbar(im, ax=ax_bmode)
    # fig, ax_bmode0 = plt.subplots()
    # image = np.abs(hilbert(zrf.rfsig[:, :, f], axis=0))
    # image = exposure.rescale_intensity(image)
    # image = exposure.equalize_adapthist(image, clip_limit=0.01)
    # im = ax_bmode0.imshow(image, aspect='auto')
    # fig, ax_bmode1 = plt.subplots()
    # image = np.abs(hilbert(zrf.rfsig[:, :, f], axis=0))
    # image = exposure.rescale_intensity(image)
    # image = exposure.equalize_adapthist(image, clip_limit=0.015)
    # im = ax_bmode1.imshow(image, aspect='auto')
    # fig, ax_bmode2 = plt.subplots()
    # image = np.abs(hilbert(zrf.rfsig[:, :, f], axis=0))
    # image = exposure.rescale_intensity(image)
    # image = exposure.equalize_adapthist(image, clip_limit=0.02)
    # im = ax_bmode2.imshow(image, aspect='auto')
    #
    # fig, ax_bmode3 = plt.subplots()
    # image = zrf.zsig[:, :, f]
    # ax_bmode3.imshow(image, aspect='auto')
    # plt.axis('off')

    """wavelet operations"""

    sig = sigproc.average_into_zcols_by_frame(zrf.rfsig[:, :, frame], zcols)
    sig -= np.mean(sig)
    # maxdepth = 1515
    d = np.arange(start=0, stop=maxdepth, step=maxdepth / sig.shape[0])
    (y0, y1) = dwt(sig[:, zcol], waveletname, 'zero')  # level 1
    x1 = np.arange(start=0, stop=maxdepth, step=maxdepth / y1.shape[0])
    (y00, y01) = dwt(y0, waveletname, 'zero')  # level 2
    x01 = np.arange(start=0, stop=maxdepth, step=maxdepth / y01.shape[0])
    (y000, y001) = dwt(y00, waveletname, 'zero')  # level 3
    x001 = np.arange(start=0, stop=maxdepth, step=maxdepth / y001.shape[0])
    (y0000, y0001) = dwt(y000, waveletname, 'zero')  # level 4
    x0001 = np.arange(start=0, stop=maxdepth, step=maxdepth / y0001.shape[0])
    (y00000, y00001) = dwt(y0000, waveletname, 'zero')  # level 5
    x00001 = np.arange(start=0, stop=maxdepth, step=maxdepth / y00001.shape[0])

    print(y1.shape)
    print(y01.shape)
    print(y001.shape)
    print(y0001.shape)
    print(y00001.shape)

    print(y1[::2].shape)
    print(y01[::2].shape)
    print(y001[::2].shape)
    print(y0001[::2].shape)
    print(y00001[::2].shape)

    # show wavelet signal processing on same axis
    fig1, ax1 = plt.subplots(nrows=4)
    ax1[0].plot(sig[:, zcol])
    ax1[0].set_ylabel('u(i)')
    ax1[1].plot(y1, 'k')
    ax1[1].set_ylabel('D1(i)')
    ax1[2].plot(y01, 'c')
    ax1[2].set_ylabel('D2(i)')
    ax1[3].plot(y001, 'r')
    ax1[3].set_ylabel('D3(i)')
    # ax1[4].plot(y0001, 'm')
    # ax1[4].set_ylabel('D4(i)')
    # ax1[5].plot(y00001, 'b')
    # ax1[5].set_ylabel('D5(i)')
    #plt.title("Subject 3 left arm, " + finger + " finger trial " + str(trial) + ".\nAveraged into " + str(zcol + 1) + " columns")
    plt.xlabel("Axial direction (i-axis)")
    for i in range(len(ax1)):
        ax1[i].set_ylim([-10000, 10000])
        ax1[i].yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))
    # plt.legend(bbox_to_anchor=(1, 1))
    # ax1.set_yticklabels([])
    # convert to frequency
    N = sig.shape[0]
    T = 30 / 10 ** 9
    Fs = 1 / T
    yfreq = scipy.fftpack.fft(sig[:, zcol])
    y1freq = scipy.fftpack.fft(y1)
    xfreq = np.linspace(0.0, 1.0 / (2.0 * T), N / 2)
    for l in range(7):
        print('Fs /', 2**l, '=', Fs/(1000000 * 2**l), 'MHz')

    # wavelet coefficients
    haar_g = [0.7071067811865476,
              0.7071067811865476]
    haar_h = [-0.7071067811865476,
              0.7071067811865476]
    db2_g = [-0.12940952255092145,
             0.22414386804185735,
             0.836516303737469,
             0.48296291314469025]
    db2_h = [-0.48296291314469025,
             0.836516303737469,
             -0.22414386804185735,
             -0.12940952255092145]
    db3_g = [0.035226291882100656,
             -0.08544127388224149,
             -0.13501102001039084,
             0.4598775021193313,
             0.8068915093133388,
             0.3326705529509569]
    db3_h = [-0.3326705529509569,
             0.8068915093133388,
             -0.4598775021193313,
             -0.13501102001039084,
             0.08544127388224149,
             0.035226291882100656]
    db4_g = [-0.010597401784997278,
             0.032883011666982945,
             0.030841381835986965,
             -0.18703481171888114,
             -0.02798376941698385,
             0.6308807679295904,
             0.7148465705525415,
             0.23037781330885523]
    db4_h = [-0.23037781330885523,
             0.7148465705525415,
             -0.6308807679295904,
             -0.02798376941698385,
             0.18703481171888114,
             0.030841381835986965,
             -0.032883011666982945,
             -0.010597401784997278]
    db5_g = [0.003335725285001549,
             -0.012580751999015526,
             -0.006241490213011705,
             0.07757149384006515,
             -0.03224486958502952,
             -0.24229488706619015,
             0.13842814590110342,
             0.7243085284385744,
             0.6038292697974729,
             0.160102397974125]
    db5_h = [-0.160102397974125,
             0.6038292697974729,
             -0.7243085284385744,
             0.13842814590110342,
             0.24229488706619015,
             -0.03224486958502952,
             -0.07757149384006515,
             -0.006241490213011705,
             0.012580751999015526,
             0.003335725285001549]

    scal1 = np.divide(db3_g, np.sqrt(2))
    wav1 = np.divide(db3_h, np.sqrt(2))
    wav2 = np.divide(np.convolve(upsample(wav1, 2), scal1), np.sqrt(2))
    scal2 = np.divide(np.convolve(upsample(scal1, 2), scal1), np.sqrt(2))
    wav3 = np.divide(np.convolve(upsample(wav2, 2), scal2), np.sqrt(2))
    scal3 = np.divide(np.convolve(upsample(scal2, 2), scal2), np.sqrt(2))
    wav4 = np.divide(np.convolve(upsample(wav3, 2), scal3), np.sqrt(2))
    scal4 = np.divide(np.convolve(upsample(scal3, 2), scal3), np.sqrt(2))
    wav5 = np.divide(np.convolve(upsample(wav4, 2), scal4), np.sqrt(2))
    scal5 = np.divide(np.convolve(upsample(scal4, 2), scal4), np.sqrt(2))
    wav6 = np.divide(np.convolve(upsample(wav5, 2), scal5), np.sqrt(2))
    scal6 = np.divide(np.convolve(upsample(scal5, 2), scal5), np.sqrt(2))

    w1, h1 = freqz(wav1)
    w2, h2 = freqz(wav2)
    w3, h3 = freqz(wav3)
    w4, h4 = freqz(wav4)
    w5, h5 = freqz(wav5)
    w6, h6 = freqz(wav6)

    # show frequency band
    lw = 3
    fig2, ax2 = plt.subplots()
    ax2.plot(xfreq[1:] / 10 ** 6, 2.0 / N * np.abs(yfreq[1:N // 2]) / np.max(2.0 / N * np.abs(yfreq[1:N // 2])),
             label='FFT{u(i)}')
    ax2.plot(w1 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(h1) / np.max(np.abs(h1)), 'k', linewidth=lw, label='level 1')
    ax2.plot(w2 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(h2) / np.max(np.abs(h2)), 'c', linewidth=lw, label='level 2')
    ax2.plot(w3 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(h3) / np.max(np.abs(h3)), 'r', linewidth=lw, label='level 3')
    ax2.plot(w4 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(h4) / np.max(np.abs(h4)), 'm--', linewidth=lw, label='level 4')
    ax2.plot(w5 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(h5) / np.max(np.abs(h5)), 'b--', linewidth=lw, label='level 5')
    ax2.plot(w6 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(h6) / np.max(np.abs(h6)), 'y--', linewidth=lw, label='level 6')
    plt.legend(loc='upper left', bbox_to_anchor=(1, 1))
    plt.xlabel('Frequency (MHz)')
    # plt.xscale('log', basex=10)
    plt.xlim([0.1, 20])
    plt.ylabel('Normalized magnitude')
    #
    # # convert every 5 mm to frequency
    # N = 216
    # T = 30 / 10 ** 9
    # Fs = 1 / T
    # y5freq = scipy.fftpack.fft(sig[0:N, zcol])
    # x5freq = np.linspace(0.0, 1.0 / (2.0 * T), int(N / 2))
    # y10freq = scipy.fftpack.fft(sig[N:N * 2, zcol])
    # y15freq = scipy.fftpack.fft(sig[N * 2:N * 3, zcol])
    # y20freq = scipy.fftpack.fft(sig[N * 3:N * 4, zcol])
    # y25freq = scipy.fftpack.fft(sig[N * 4:N * 5, zcol])
    # y30freq = scipy.fftpack.fft(sig[N * 5:N * 6, zcol])
    # y35freq = scipy.fftpack.fft(sig[N * 6:N * 7, zcol])
    #
    # fig, ax5mm = plt.subplots(nrows=7, sharex=True)
    # a5, = ax5mm[0].plot(x5freq[1:] / 10 ** 6,
    #                     2.0 / N * np.abs(y5freq[1:N // 2]) / np.max(2.0 / N * np.abs(y5freq[1:N // 2])))
    # w1, = ax5mm[0].plot(db3_w1 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h1), label='db3 wavelet level 1')
    # w2, = ax5mm[0].plot(db3_w2 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h2), label='db3 wavelet level 2')
    # w3, = ax5mm[0].plot(db3_w3 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h3), label='db3 wavelet level 3')
    # w4, = ax5mm[0].plot(db3_w4 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h4), label='db3 wavelet level 4')
    # w5, = ax5mm[0].plot(db3_w5 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h5), label='db3 wavelet level 5')
    # a10, = ax5mm[1].plot(x5freq[1:] / 10 ** 6,
    #                      2.0 / N * np.abs(y10freq[1:N // 2]) / np.max(2.0 / N * np.abs(y10freq[1:N // 2])))
    # ax5mm[1].plot(db3_w1 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h1), label='db3 wavelet level 1')
    # ax5mm[1].plot(db3_w2 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h2), label='db3 wavelet level 2')
    # ax5mm[1].plot(db3_w3 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h3), label='db3 wavelet level 3')
    # ax5mm[1].plot(db3_w4 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h4), label='db3 wavelet level 4')
    # ax5mm[1].plot(db3_w5 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h5), label='db3 wavelet level 5')
    # a15, = ax5mm[2].plot(x5freq[1:] / 10 ** 6,
    #                      2.0 / N * np.abs(y15freq[1:N // 2]) / np.max(2.0 / N * np.abs(y15freq[1:N // 2])))
    # ax5mm[2].plot(db3_w1 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h1), label='db3 wavelet level 1')
    # ax5mm[2].plot(db3_w2 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h2), label='db3 wavelet level 2')
    # ax5mm[2].plot(db3_w3 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h3), label='db3 wavelet level 3')
    # ax5mm[2].plot(db3_w4 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h4), label='db3 wavelet level 4')
    # ax5mm[2].plot(db3_w5 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h5), label='db3 wavelet level 5')
    # a20, = ax5mm[3].plot(x5freq[1:] / 10 ** 6,
    #                      2.0 / N * np.abs(y20freq[1:N // 2]) / np.max(2.0 / N * np.abs(y20freq[1:N // 2])))
    # ax5mm[3].plot(db3_w1 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h1), label='db3 wavelet level 1')
    # ax5mm[3].plot(db3_w2 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h2), label='db3 wavelet level 2')
    # ax5mm[3].plot(db3_w3 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h3), label='db3 wavelet level 3')
    # ax5mm[3].plot(db3_w4 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h4), label='db3 wavelet level 4')
    # ax5mm[3].plot(db3_w5 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h5), label='db3 wavelet level 5')
    # a25, = ax5mm[4].plot(x5freq[1:] / 10 ** 6,
    #                      2.0 / N * np.abs(y25freq[1:N // 2]) / np.max(2.0 / N * np.abs(y25freq[1:N // 2])))
    # ax5mm[4].plot(db3_w1 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h1), label='db3 wavelet level 1')
    # ax5mm[4].plot(db3_w2 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h2), label='db3 wavelet level 2')
    # ax5mm[4].plot(db3_w3 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h3), label='db3 wavelet level 3')
    # ax5mm[4].plot(db3_w4 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h4), label='db3 wavelet level 4')
    # ax5mm[4].plot(db3_w5 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h5), label='db3 wavelet level 5')
    # a30, = ax5mm[5].plot(x5freq[1:] / 10 ** 6,
    #                      2.0 / N * np.abs(y30freq[1:N // 2]) / np.max(2.0 / N * np.abs(y30freq[1:N // 2])))
    # ax5mm[5].plot(db3_w1 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h1), label='db3 wavelet level 1')
    # ax5mm[5].plot(db3_w2 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h2), label='db3 wavelet level 2')
    # ax5mm[5].plot(db3_w3 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h3), label='db3 wavelet level 3')
    # ax5mm[5].plot(db3_w4 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h4), label='db3 wavelet level 4')
    # ax5mm[5].plot(db3_w5 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h5), label='db3 wavelet level 5')
    # a35, = ax5mm[6].plot(x5freq[1:] / 10 ** 6,
    #                      2.0 / N * np.abs(y35freq[1:N // 2]) / np.max(2.0 / N * np.abs(y35freq[1:N // 2])))
    # ax5mm[6].plot(db3_w1 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h1), label='db3 wavelet level 1')
    # ax5mm[6].plot(db3_w2 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h2), label='db3 wavelet level 2')
    # ax5mm[6].plot(db3_w3 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h3), label='db3 wavelet level 3')
    # ax5mm[6].plot(db3_w4 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h4), label='db3 wavelet level 4')
    # ax5mm[6].plot(db3_w5 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_h5), label='db3 wavelet level 5')
    # plt.subplots_adjust(right=0.7)
    # ax5mm[0].legend([w1, w2, w3, w4, w5],
    #                 ['db3 wavelet level 1',
    #                  'db3 wavelet level 2',
    #                  'db3 wavelet level 3',
    #                  'db3 wavelet level 4',
    #                  'db3 wavelet level 5'], bbox_to_anchor=(1.05, 1), loc=2)
    #
    # plt.xlabel('Frequency (MHz)')
    # ax5mm[0].set_title('FFT every 5 mm\ntop: 0mm to 5mm, bottom: 30mm to 35mm')

    # # mean(abs(wavelet every 2 vals))
    # detail = y1
    # if detail.shape[0] % 2 == 0:
    #     shortdetail = np.mean(np.abs(detail).reshape(-1, 2), axis=1)
    # else:
    #     shortdetail = np.mean(np.abs(detail[0:detail.shape[0] - 1]).reshape(-1, 2), axis=1)
    # y1 = shortdetail
    # x1 = np.arange(start=0, stop=maxdepth, step=maxdepth/y1.shape[0])
    #
    # detail = y01
    # if detail.shape[0] % 2 == 0:
    #     shortdetail = np.mean(np.abs(detail).reshape(-1, 2), axis=1)
    # else:
    #     shortdetail = np.mean(np.abs(detail[0:detail.shape[0] - 1]).reshape(-1, 2), axis=1)
    # y01 = shortdetail
    # x01 = np.arange(start=0, stop=maxdepth, step=maxdepth/y01.shape[0])
    #
    # detail = y001
    # if detail.shape[0] % 2 == 0:
    #     shortdetail = np.mean(np.abs(detail).reshape(-1, 2), axis=1)
    # else:
    #     shortdetail = np.mean(np.abs(detail[0:detail.shape[0] - 1]).reshape(-1, 2), axis=1)
    # y001 = shortdetail
    # x001 = np.arange(start=0, stop=maxdepth, step=maxdepth/y001.shape[0])
    #
    # detail = y0001
    # if detail.shape[0] % 2 == 0:
    #     shortdetail = np.mean(np.abs(detail).reshape(-1, 2), axis=1)
    # else:
    #     shortdetail = np.mean(np.abs(detail[0:detail.shape[0] - 1]).reshape(-1, 2), axis=1)
    # y0001 = shortdetail
    # x0001 = np.arange(start=0, stop=maxdepth, step=maxdepth/y0001.shape[0])
    #
    # detail = y00001
    # if detail.shape[0] % 2 == 0:
    #     shortdetail = np.mean(np.abs(detail).reshape(-1, 2), axis=1)
    # else:
    #     shortdetail = np.mean(np.abs(detail[0:detail.shape[0] - 1]).reshape(-1, 2), axis=1)
    # y00001 = shortdetail
    # x00001 = np.arange(start=0, stop=maxdepth, step=maxdepth/y00001.shape[0])
    #
    # # show wavelet signal processing on same axis
    # fig3, ax3 = plt.subplots()
    # ax3.plot(d, sig[:, zcol], label='averaged signal')
    # ax3.plot(x1, y1 - 40000, label='db3 wavelet level 1')
    # ax3.plot(x01, y01 - 80000, label='db3 wavelet level 2')
    # ax3.plot(x001, y001 - 120000, label='db3 wavelet level 3')
    # ax3.plot(x0001, y0001 - 160000, label='db3 wavelet level 4')
    # ax3.plot(x00001, y00001 - 200000, label='db3 wavelet level 5')
    # plt.title("Arm3, finger: " + finger + str(trial) + ".\nmean(abs(every 2 values))")
    # plt.xlabel("depth (mm)")
    # plt.legend(bbox_to_anchor=(1, 1))
    # ax3.set_yticklabels([])

    """Show M-mode for wavelet scales"""

    # MmodePlotWavelet(dataDir, fingername='Pinky', trial=2, zcols=6, waveletname=waveletname, zcol=5)
    # MmodePlotWavelet(dataDir, fingername='Index', trial=2, zcols=6, waveletname=waveletname, zcol=5)
    # MmodePlotWaveletDiff(dataDir, fingername1='Index', fingername2='Pinky', trial=2, zcols=6, waveletname=waveletname, zcol=5)
    # for l in range(5):
    #     MmodePlotWavelet(dataDir, fingername='Pinky', trial=2, zcols=6, waveletname=waveletname, zcol=5, lvl=l+1)
    # for l in range(5):
    #     MmodePlotWavelet(dataDir, fingername='Index', trial=2, zcols=6, waveletname=waveletname, zcol=5, lvl=l+1)

    """Show as image as updating frame by frame plot"""

    # plt.ion()
    # fingername = "Pinky"
    # trial = 1
    # zrfFile = dataDir + fingername + str(trial) + ".zrf"
    #
    # fig, ax = plt.subplots()
    # im = ax.imshow(np.zeros((382, 127)), aspect='auto', animated=True)
    # ax.set_xlabel('z-scan')
    # ax.set_ylabel('depth')
    #
    # frame = 0
    # ax.cla()
    # zrf = ZRFData(zrfFile)
    #
    # zrf = ZRFData(dataDir + fingername + str(trial) + '.zrf')
    # sig = sigproc.average_into_zcols(zrf.rfsig, zcols)
    # sig1 = np.zeros([760, sig.shape[1], sig.shape[2]])
    # sig2 = np.zeros([382, sig.shape[1], sig.shape[2]])
    # sig3 = np.zeros([193, sig.shape[1], sig.shape[2]])
    # sig4 = np.zeros([99, sig.shape[1], sig.shape[2]])
    #
    # for z in range(zcols):
    #     for frame in range(sig.shape[2]):
    #         (a1, sig1[:, z, frame]) = dwt(sig[:, z, frame], waveletname, 'zero')  # level 1
    #         (a2, sig2[:, z, frame]) = dwt(a1, waveletname, 'zero')  # level 2
    #         (a3, sig3[:, z, frame]) = dwt(a2, waveletname, 'zero')  # level 3
    #         (a4, sig4[:, z, frame]) = dwt(a3, waveletname, 'zero')  # level 4
    #
    # im = ax.imshow(sig2[:, :, frame], aspect='auto', animated=True)
    # ax.set_title(zrfFile.split('/')[-1])
    #
    # while True:
    #     plt.sca(ax)
    #     im.set_data(sig2[:, :, frame])
    #     plt.draw()
    #     plt.pause(1/3000)
    #     frame += 1
    #     if frame == sig2.shape[2]:
    #         frame = 0

    """Plot summed differences between frames of full recording"""

    # finger = 'Index'
    # trial = 1
    # zrf = ZRFData(dataDir + finger + str(trial) + '.zrf')
    # sig = np.zeros([zrf.rfsig.shape[0], zrf.rfsig.shape[1]])
    # for d in range(sig.shape[0]):
    #     for z in range(sig.shape[1]):
    #         for f in range(0, 180):
    #             sig[d, z] += np.abs(zrf.rfsig[d, z, f] - zrf.rfsig[d, z, f - 1])
    #
    # plt.figure(1)
    # plt.imshow(sig, aspect='auto')
    # plt.title(finger + str(trial))
    # plt.colorbar()
    #
    # finger = 'Pinky'
    # trial = 1
    # zrf = ZRFData(dataDir + finger + str(trial) + '.zrf')
    # sig = np.zeros([zrf.rfsig.shape[0], zrf.rfsig.shape[1]])
    # for d in range(sig.shape[0]):
    #     for z in range(sig.shape[1]):
    #         for f in range(0, 180):
    #             sig[d, z] += np.abs(zrf.rfsig[d, z, f] - zrf.rfsig[d, z, f - 1])
    #
    # plt.figure(2)
    # plt.imshow(sig, aspect='auto')
    # plt.title(finger + str(trial))
    # plt.colorbar()

    plt.show()

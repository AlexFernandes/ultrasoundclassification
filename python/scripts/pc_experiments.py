"""Perform classification for specific resolution
"""

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from python.src.zrfManager import ZRFData
import python.src.patternClassification as pc
import numpy as np
import time
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.decomposition import PCA
from sklearn.metrics import confusion_matrix, accuracy_score
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import SelectPercentile, SelectKBest, SelectFpr, SelectFdr, SelectFwe, mutual_info_classif, f_classif


def print_elapsed_time(startTime):
    elapsed_time = time.time()
    hours, rem = divmod(elapsed_time - startTime, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Elapsed Time: {:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))


if __name__ == "__main__":
    # Time Start
    startTime = time.time()

    # Parameters
    numTrials = 3
    frameStart = 0
    frameEnd = 180
    frameGroupSize = 3
    numFrames = frameEnd - frameStart
    waveletname = 'db3'
    levels = [2, 3, 4, 5]
    windowSize = 2  # dsize as in window size along the depth axis
    numSegments = 100

    # Resolution
    for N in range(1, 7):

        fingerNames = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
        fingerLabel = {'Thumb': 0, 'Index': 1, 'Middle': 2, 'Ring': 3, 'Pinky': 4}

        zrfDir = "D:/Code/zrfFiles/"

        # subjectPath = "fm_sub2_l/FingerMotions/"
        subjectPath = "AlexFingerMovement/FingerMovement/"

        print('Resolution N =', N)


        filePath = zrfDir + subjectPath

        print(subjectPath)
        samples = pc.DataContainer()  # container to put extracted features and samples to then classify


        for fingerID in range(len(fingerNames)):
            for trial in range(1, numTrials + 1):
                # print(fingerNames[fingerID], trial, end=' ')
                # print_elapsed_time(startTime)

                # load ultrasound .zrf file
                zrf = ZRFData(filePath + fingerNames[fingerID] + str(trial) + '.zrf')

                ########################################
                """Preprocessing & Feature Extraction"""
                ########################################

                # extract wavelet coefficient features for each frame of the rf data
                # list of a list: [numFrames][zcols*N]; N is based on the level of wavelet scales
                exctractedFeatures = []
                for frame in range(frameStart, frameEnd):  # frames are representative as samples
                    feature_per_sample = []

                    # # linear fit features
                    # for feature in pc.exctract_linear_fit_segments(zrf.rfsig[:, :, frame],  # rf signal
                    #                                                N,  # num z columns to average to
                    #                                                numSegments):  # num depth segments
                    #     feature_per_sample.append(feature)

                    # wavelet features
                    for feature in pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
                                                                   N,  # num columns to average to
                                                                   waveletname,  # wavelet
                                                                   levels,  # levels
                                                                   windowSize):  # window size for mean(abs())
                        feature_per_sample.append(feature)

                    exctractedFeatures.append(feature_per_sample)  # num depth segments
                # obtain difference between frames as features
                differenceFeatures = []  # same as linearFitFeatures
                for frame in range(numFrames):
                    frameFeatures = []
                    for feature in range(len(exctractedFeatures[frame])):
                        frameFeatures.append(
                            exctractedFeatures[frame][feature] - exctractedFeatures[frame - 1][feature])
                    differenceFeatures.append(frameFeatures)

                # group frames into group size and obtain min/max values and add the samples to data set
                # sample size reduces by frameGroupSize amount i.e. 540 samples / 3 frameGroupSize = 180 samples
                for frame in range(0, numFrames, frameGroupSize):
                    frameFeatures = []
                    for feature in range(len(exctractedFeatures[frame])):
                        frameFeatures.append(
                            np.min([exctractedFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                        frameFeatures.append(
                            np.max([exctractedFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                        frameFeatures.append(
                            np.min([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                        frameFeatures.append(
                            np.max([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))

                    # collection of frames are represented as a sample
                    samples.add_sample(np.asarray(frameFeatures), fingerID, trial)


        ############################
        """Pattern Classification"""
        ############################

        # Normalize Data
        samples.X = StandardScaler().fit_transform(samples.X)

        # Feature Selection
        # selector = SelectPercentile(mutual_info_classif, percentile=1)
        # selector = SelectKBest(mutual_info_classif, k=900)
        selector = SelectFwe(f_classif)

        # Classifier
        lda = LinearDiscriminantAnalysis()

        # Split into test / training data
        cnf_matrix = None
        y_pred = []
        y_test = []
        crossvalacc = []

        # classify with test samples represented as the current trial number
        for trial in range(1, numTrials + 1):
            samples.test_train_split_by_trial(trial)

            # Remove features
            X_train_new = selector.fit_transform(samples.X_train, samples.y_train)
            X_test_new = selector.transform(samples.X_test)

            # Predict
            pred = lda.fit(X_train_new, samples.y_train).predict(X_test_new)

            for s in range(len(pred)):
                y_pred.append(pred[s])
                y_test.append(samples.y_test[s])

            print('Trial', trial)
            print('Features:', len(samples.X_train[0]), '->', len(X_train_new[0]))
            trial_cnf = confusion_matrix(samples.y_test, pred)
            crossvalacc.append(accuracy_score(samples.y_test, pred))
            print(trial_cnf)
            print(accuracy_score(samples.y_test, pred))

            # # Plot feature selector value
            # fig, axmi = plt.subplots()
            # X_indices = np.arange(len(samples.X_train[0]))
            # values = sorted(selector.pvalues_)
            # axmi.bar(X_indices, values)
            # axmi.set_xlabel('Sorted features')
            # axmi.set_ylabel('Value')

        # # Plot PCA 2D
        # fig, axpca2d = plt.subplots()
        # colors = ['b', 'g', 'r', 'c', 'k']
        # X = selector.fit_transform(samples.X, samples.y)
        # pca2d_plot = PCA(n_components=2, whiten=True, svd_solver='arpack')
        # X_pca = pd.DataFrame(pca2d_plot.fit(X).transform(X))
        # pos_len = int(len(samples.y) / 5)
        # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
        #     axpca2d.scatter(X_pca[i * pos_len:i * pos_len + pos_len][0],
        #                   X_pca[i * pos_len:i * pos_len + pos_len][1],
        #                   alpha=0.8, color=color, label=fingerName)
        # axpca2d.legend(loc='best', shadow=False, scatterpoints=1)
        # axpca2d.set_xlabel('First Principal Component')
        # axpca2d.set_ylabel('Second Principal Component')
        # axpca2d.set_title('PCA plot')

        # # Plot PCA 3D
        # fig = plt.figure()
        # axpca3d = fig.add_subplot(111, projection='3d')
        # colors = ['b', 'g', 'r', 'c', 'k']
        # X = selector.fit_transform(samples.X, samples.y)
        # pca3d_plot = PCA(n_components=3, whiten=True, svd_solver='arpack')
        # X_pca = pd.DataFrame(pca3d_plot.fit(X).transform(X))
        # pos_len = int(len(samples.y) / 5)
        # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
        #     axpca3d.scatter(X_pca[i * pos_len:i * pos_len + pos_len][0],
        #                   X_pca[i * pos_len:i * pos_len + pos_len][1],
        #                   X_pca[i * pos_len:i * pos_len + pos_len][2],
        #                   alpha=0.8, color=color, label=fingerName)
        # axpca3d.legend(loc='best', shadow=False, scatterpoints=1)
        # axpca3d.set_xlabel('First Principal Component')
        # axpca3d.set_ylabel('Second Principal Component')
        # axpca3d.set_zlabel('Third Principal Component')
        # axpca3d.set_title('PCA plot')

        print('Complete Confusion matrix')
        print('Total samples:', len(samples.X))
        cnf_matrix = confusion_matrix(y_test, y_pred)
        print(cnf_matrix)
        print('Accuracy: {} +/- {} %'.format(np.mean(crossvalacc) * 100, np.std(crossvalacc) * 100))
        print_elapsed_time(startTime)

    plt.show()


import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import matplotlib

if __name__ == '__main__':

    font = {'family': 'normal',
            'weight': 'normal',
            'size': 16}
    matplotlib.rc('font', **font)

    ####################################################################################
    """Computation time of Spatial Features vs Number of reconstructed RF signals (N)"""
    ####################################################################################
    N = np.asarray([1, 2, 3, 4, 6, 8, 12, 16, 25, 40, 60, 127])
    linfit25 = np.asarray(
        [[3.50, 0.19], [6.68, 0.31], [9.89, 0.42], [13.10, 0.53], [19.57, 0.72], [26.01, 0.82], [38.81, 0.95],
         [51.79, 1.02], [80.65, 1.19], [128.92, 1.47], [193.41, 1.63], [408.54, 2.64]])
    linfit50 = np.asarray(
        [[5.69, 0.25], [11.19, 0.40], [16.70, 0.47], [22.17, 0.49], [33.16, 0.48], [44.10, 0.51], [65.90, 0.68],
         [88.00, 0.79], [137.25, 1.19], [219.46, 1.87], [329.37, 2.21], [695.99, 4.05]])
    linfit100 = np.asarray(
        [[10.36, 0.36], [20.54, 0.35], [30.70, 0.40], [40.82, 0.48], [61.15, 0.69], [81.37, 0.88], [121.62, 1.04],
         [162.45, 1.33], [253.43, 1.90], [405.31, 2.67], [608.62, 3.35], [1286.05, 6.45]])
    # db3 wavelets used during experiment
    lvl1 = np.asarray(
        [[0.25, 0.02], [0.35, 0.02], [0.44, 0.02], [0.52, 0.02], [0.70, 0.03], [0.88, 0.03], [1.23, 0.04], [1.58, 0.05],
         [2.35, 0.06], [3.65, 0.08], [5.37, 0.10], [11.04, 0.20]])
    lvl2 = np.asarray(
        [[0.30, 0.01], [0.44, 0.02], [0.57, 0.02], [0.71, 0.02], [0.99, 0.03], [1.26, 0.04], [1.82, 0.04], [2.37, 0.06],
         [3.61, 0.08], [5.68, 0.15], [8.42, 0.20], [17.56, 0.36]])
    lvl3 = np.asarray(
        [[0.33, 0.01], [0.51, 0.02], [0.69, 0.02], [0.86, 0.02], [1.21, 0.03], [1.56, 0.04], [2.26, 0.05], [2.97, 0.08],
         [4.55, 0.12], [7.18, 0.20], [10.68, 0.25], [22.40, 0.47]])
    lvl4 = np.asarray(
        [[0.37, 0.02], [0.57, 0.02], [0.78, 0.03], [0.99, 0.03], [1.40, 0.03], [1.81, 0.04], [2.64, 0.06], [3.47, 0.09],
         [5.33, 0.16], [8.41, 0.21], [12.52, 0.26], [26.33, 0.53]])
    lvl5 = np.asarray(
        [[0.39, 0.02], [0.63, 0.02], [0.86, 0.02], [1.10, 0.03], [1.56, 0.04], [2.03, 0.04], [2.96, 0.07], [3.90, 0.09],
         [6.00, 0.17], [9.47, 0.20], [14.13, 0.30], [29.74, 0.54]])
    print('slope (ms/N)')
    template = '{0:10s} {1:6.3f}'
    for name, data in zip(['linfit25',
                           'linfit50',
                           'linfit100',
                           'lvl1',
                           'lvl2',
                           'lvl3',
                           'lvl4',
                           'lvl5'],
                          [linfit25[:, 0],
                           linfit50[:, 0],
                           linfit100[:, 0],
                           lvl1[:, 0],
                           lvl2[:, 0],
                           lvl3[:, 0],
                           lvl4[:, 0],
                           lvl5[:, 0]]):
        print(template.format(name, np.polyfit(N, data, 1)[0]))

    fig, axN = plt.subplots(nrows=2, sharex=True)
    axN[0].plot(N, linfit25[:, 0], label='25 segments')
    axN[0].plot(N, linfit50[:, 0], label='50 segments')
    axN[0].plot(N, linfit100[:, 0], label='100 segments')
    axN[0].set_ylabel('Computation time (ms)')
    axN[0].legend()
    axN[1].plot(N, lvl1[:, 0], label='lvl 1')
    axN[1].plot(N, lvl2[:, 0], label='lvls 1, 2')
    axN[1].plot(N, lvl3[:, 0], label='lvls 1, 2, 3')
    axN[1].plot(N, lvl4[:, 0], label='lvls 1, 2, 3, 4')
    axN[1].plot(N, lvl5[:, 0], label='lvls 1, 2, 3, 4, 5')
    axN[1].set_ylabel('Computation time (ms)')
    axN[1].set_xlabel('Number of reconstructed RF signals (N)')
    axN[1].legend()

    fig, axRatio = plt.subplots()
    axRatio.plot(N, linfit25[:, 0] / lvl3[:, 0], label='DWT Db3 vs 25 segments')
    axRatio.plot(N, linfit50[:, 0] / lvl3[:, 0], label='DWT Db3 vs 50 segments')
    axRatio.plot(N, linfit100[:, 0] / lvl3[:, 0], label='DWT Db3 vs 100 segments')
    axRatio.set_ylabel('Computational efficiency')
    axRatio.set_xscale('log', basex=2)
    axRatio.set_ylim([10, 70])
    axRatio.set_xlabel('Number of reconstructed RF signals (N)')
    axRatio.legend(loc='upper left')
    axRatio.grid()

    ####################################################################################
    """Computation time of Pipeline vs Number of reconstructed RF signals (N)"""
    ####################################################################################
    N = np.asarray([1, 2, 3, 4, 6, 8, 12, 16, 25, 40, 60, 127])
    LR10_spatial = np.asarray([[6.38, 0.68],
                               [11.58, 1.60],
                               [16.95, 1.09],
                               [22.38, 1.24],
                               [33.23, 1.40],
                               [44.19, 1.40],
                               [65.89, 1.46],
                               [88.02, 1.51],
                               [137.02, 1.68],
                               [218.64, 1.99],
                               [327.24, 2.28],
                               [692.83, 4.28]])
    LR10_temporal = np.asarray([[0.02, 0.00],
                                [0.03, 0.00],
                                [0.04, 0.00],
                                [0.06, 0.00],
                                [0.08, 0.00],
                                [0.11, 0.01],
                                [0.16, 0.01],
                                [0.22, 0.01],
                                [0.35, 0.02],
                                [0.56, 0.02],
                                [0.83, 0.02],
                                [1.75, 0.06]])
    LR10_classify = np.asarray([[0.08, 0.00],
                                [0.09, 0.03],
                                [0.08, 0.01],
                                [0.10, 0.40],
                                [0.09, 0.41],
                                [0.10, 0.49],
                                [0.10, 0.51],
                                [0.10, 0.43],
                                [0.11, 0.47],
                                [0.13, 0.31],
                                [0.13, 0.27],
                                [0.13, 0.15]])
    LR10_total = np.asarray([[6.48, 0.68],
                             [11.70, 1.06],
                             [17.07, 1.09],
                             [22.53, 1.31],
                             [33.41, 1.46],
                             [44.40, 1.49],
                             [66.16, 1.54],
                             [88.34, 1.57],
                             [137.48, 1.74],
                             [219.33, 2.01],
                             [328.20, 2.30],
                             [694.72, 4.29]])

    db3lvl123_spatial = np.asarray([[1.52, 0.12],
                                    [1.65, 0.21],
                                    [2.02, 0.21],
                                    [2.43, 0.20],
                                    [3.29, 0.18],
                                    [4.16, 0.15],
                                    [5.90, 0.18],
                                    [7.66, 0.21],
                                    [11.48, 0.27],
                                    [18.08, 0.37],
                                    [26.79, 0.61],
                                    [56.34, 4.06]])
    db3lvl123_temporal = np.asarray([[0.49, 0.04],
                                     [0.93, 0.05],
                                     [1.38, 0.06],
                                     [1.83, 0.07],
                                     [2.72, 0.10],
                                     [3.62, 0.14],
                                     [5.41, 0.19],
                                     [7.19, 0.24],
                                     [11.16, 0.34],
                                     [17.80, 0.46],
                                     [26.71, 0.69],
                                     [56.61, 1.36]])
    db3lvl123_classify = np.asarray([[0.10, 0.39],
                                     [0.11, 0.22],
                                     [0.13, 0.05],
                                     [0.15, 0.07],
                                     [0.20, 0.05],
                                     [0.24, 0.08],
                                     [0.28, 0.12],
                                     [0.33, 0.17],
                                     [0.56, 0.25],
                                     [1.06, 0.33],
                                     [1.98, 0.48],
                                     [4.77, 0.88]])
    db3lvl123_total = np.asarray([[2.11, 0.42],
                                  [2.69, 0.33],
                                  [3.52, 0.25],
                                  [4.42, 0.25],
                                  [6.22, 0.26],
                                  [8.01, 0.28],
                                  [11.59, 0.37],
                                  [15.17, 0.45],
                                  [23.20, 0.62],
                                  [36.94, 0.85],
                                  [55.47, 1.26],
                                  [117.71, 4.79]])

    print('slope (ms/N)')
    template = '{0:10s} {1:6.3f}'
    for name, data in zip(['LR10 spatial', 'LR10 temporal', 'LR10 classify', 'LR10 total',
                           'db3 spatial', 'db3 temporal', 'db3 classify', 'db3 total'],
                          [LR10_spatial, LR10_temporal, LR10_classify, LR10_total,
                           db3lvl123_spatial, db3lvl123_temporal, db3lvl123_classify, db3lvl123_total]):
        print(template.format(name, np.polyfit(N, data[:, 0], 1)[0]))

    fig, axN = plt.subplots(nrows=2, sharex=True)
    for name, data in zip(['LR10_spatial', 'LR10_temporal', 'LR10_classify', 'LR10_total'],
                          [LR10_spatial, LR10_temporal, LR10_classify, LR10_total]):
        axN[0].plot(N, data[:, 0], label=name)
        axN[0].set_ylabel('Computation time (ms)')
    axN[0].legend()
    for name, data in zip(['db3 spatial', 'db3 temporal', 'db3 classify', 'db3 total'],
                          [db3lvl123_spatial, db3lvl123_temporal, db3lvl123_classify, db3lvl123_total]):
        axN[1].plot(N, data[:, 0], label=name)
        axN[1].set_ylabel('Computation time (ms)')
    axN[1].set_xlabel('Number of reconstructed RF signals (N)')
    axN[1].legend()

    fig, axRatio = plt.subplots()
    axRatio.plot(N, LR10_spatial[:, 0] / db3lvl123_spatial[:, 0], label='spatial')
    axRatio.plot(N, LR10_temporal[:, 0] / db3lvl123_temporal[:, 0], label='temporal')
    axRatio.plot(N, LR10_classify[:, 0] / db3lvl123_classify[:, 0], label='classify')
    axRatio.plot(N, LR10_total[:, 0] / db3lvl123_total[:, 0], label='total')
    axRatio.set_ylabel('Computational efficiency')
    axRatio.set_xscale('log', basex=2)
    axRatio.set_xlabel('Number of reconstructed RF signals (N)')
    axRatio.legend(loc='upper left')
    axRatio.grid()

    ########################################
    """Plot Computation time vs parameter"""
    ########################################
    lvl = np.asarray(range(1, 21))
    maxlvl = np.asarray([[0.76, 0.12],
                         [1.08, 0.72],
                         [1.30, 0.19],
                         [1.49, 0.20],
                         [1.66, 0.21],
                         [1.82, 0.23],
                         [1.97, 0.37],
                         [2.12, 0.25],
                         [2.27, 0.25],
                         [2.41, 0.26],
                         [2.55, 0.27],
                         [2.69, 0.27],
                         [2.84, 0.28],
                         [2.98, 0.29],
                         [3.13, 0.30],
                         [3.26, 0.30],
                         [3.41, 0.31],
                         [3.55, 0.32],
                         [3.70, 0.33],
                         [3.84, 0.33]])
    segs = np.asarray(range(5, 101, 5))
    linfit = np.asarray([[8.26, 0.48],
                         [11.07, 0.56],
                         [13.75, 0.62],
                         [16.47, 0.67],
                         [19.12, 0.72],
                         [21.82, 0.77],
                         [24.60, 0.82],
                         [27.32, 0.89],
                         [30.02, 0.88],
                         [32.66, 0.89],
                         [35.38, 0.95],
                         [38.11, 0.93],
                         [40.87, 0.95],
                         [43.69, 0.95],
                         [46.47, 0.99],
                         [48.99, 0.98],
                         [51.66, 1.03],
                         [54.48, 1.03],
                         [57.04, 1.03],
                         [59.93, 1.16]])

    fig, axparam = plt.subplots(nrows=2)
    axparam[0].plot(segs, linfit[:, 0], label='Envelope Linear Regression')
    axparam[0].set_ylabel('Computation time (ms)')
    axparam[0].set_xlabel('Number of segments')
    axparam[0].legend()
    axparam[1].plot(lvl, maxlvl[:, 0], label='Wavelet MAV')
    axparam[1].set_ylabel('Computation time (ms)')
    axparam[1].set_xlabel('Max level')
    axparam[1].legend()

    plt.show()

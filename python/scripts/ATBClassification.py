"""Classification using ATB file data
"""

import matplotlib.pyplot as plt
from python.src.readATB import read_atb
import python.src.patternClassification as pc
import numpy as np
import time
from pywt._dwt import dwt
import pandas as pd
from sklearn.manifold import MDS, TSNE
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, accuracy_score, f1_score

import warnings
warnings.filterwarnings("ignore")


def print_elapsed_time(startTime):
    elapsed_time = time.time()
    hours, rem = divmod(elapsed_time - startTime, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Elapsed Time: {:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))


def printATBInfo(capture_file_header, channel_file_header_array):
    template = "{:<23} | {}"
    print(template.format('Capture file header', 'Value'))
    for k, v in sorted(vars(capture_file_header).items()):
        if k[0:2] == 'm_':
            print(template.format(k, v))
    print()

    for channel in range(capture_file_header.m_nChannels):
        print(template.format('Channel %d' % channel, 'Value'))
        for k, v in sorted(vars(channel_file_header_array[0]).items()):
            if k[0:2] == 'm_':
                print(template.format(k, v))
        print()


if __name__ == '__main__':
    # Time Start
    startTime = time.time()

    # ATB Files
    atbDir = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/AlazarTech/"
    # atbDir = "D:/Code/atbFiles/"
    subjectPath = "1kPRF_FingerFlexions_134channel/"
    PRF = 1000  # list of Pulse Repetition Frequency or Frame Rate (Hz) corresponding to subjectPaths
    fingerNames = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']  # class numbered by this ordering
    Trials = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    # Trials = [1, 3, 5, 7, 8]
    numTrials = len(Trials)
    # channels = [0, 2, 3]  # Channels to use

    # Pre-processing & Feature Extraction Parameters
    waveletname = 'db3'
    depthRange = [320, 1520]
    levels = [3, 4, 5, 6]  # make in ascending order
    frameAvg = 10  # number of frames to average for purposes of reducing noise
    timeGroupSize = 10  # number of time samples to perform max voting
    windowSize = 2  # size for applying the Mean Absolute Value function

    # for (frameAvg, timeGroupSize) in zip([   9,   9,    8,   8,    7,   7,    6,    6,    5,   5,   4,   4,   3,   3],
    #                                      [  11,   5,   12,   6,   14,   7,   15,   10,   20,  10,  25,  20,  30,  20]):
    for channels in [[0], [2], [3], [0, 2], [0, 3], [2, 3], [0, 2, 3]]:
        print('######################################################################')

        # Classify Subject Data
        samples = pc.DataContainer()  # container to put extracted features and samples of complete data set

        # extract features by loading one atb file at a time
        for fingerID in range(len(fingerNames)):
            # Print
            # print(fingerNames[fingerID], end=' ')
            # print_elapsed_time(startTime)

            for trial in range(numTrials):
                capture_file_header, channel_file_header_array, atb_sample_data, frame_time_stamps = read_atb(
                    atbDir + subjectPath + fingerNames[fingerID] + str(Trials[trial]) + '.atb')

                # number of time samples obtained from the atb file recording
                numTimeSamples = atb_sample_data.shape[2] // frameAvg

                # Preprocess acquired data
                sample_data = np.zeros(atb_sample_data.shape, dtype=np.dtype('float64'))
                for c in channels:
                    for f in range(sample_data.shape[2]):
                        # Format to volts
                        dSampleZeroValue = (1 << (capture_file_header.m_nBitsPerSample - 1)) - 0.5
                        dSampleMaxValue = (1 << (capture_file_header.m_nBitsPerSample - 1)) - 0.5
                        nSampleBitShift = (capture_file_header.m_nBytesPerSample * 8) - capture_file_header.m_nBitsPerSample
                        sample_data[:, c, f] = np.asarray(atb_sample_data[:, c, f] >> nSampleBitShift,
                                                          dtype=np.dtype('float64'))
                        dMaxVolts = channel_file_header_array[c].m_dInputRange_volts * channel_file_header_array[
                            c].m_dProbeAttenuation
                        sample_data[:, c, f] = dMaxVolts * (sample_data[:, c, f] - dSampleZeroValue) / dSampleMaxValue
                        # DC removal
                        sample_data[:, c, f] -= np.mean(sample_data[:, c, f])

                # Spatial Features
                spatialFeatures = []  # [time samples][spatial features]
                for t in range(numTimeSamples):
                    mav_features = []
                    for channel in channels:
                        # Reduce variance of noise by averaging into the time samples
                        approx = np.zeros(depthRange[1] - depthRange[0], dtype=np.dtype('float64'))  # 1-D RF signal
                        for d in range(depthRange[0], depthRange[1]):
                            approx[d - depthRange[0]] = np.mean(
                                sample_data[d, channel, t * frameAvg:(t + 1) * frameAvg])
                        # DC removal
                        approx -= np.mean(approx)
                        # Discrete Wavelet Transform applied along depth axis
                        for lvl in range(levels[-1]):
                            # recursively obtain wavelet 'detail' coefficients using previous approx coefficients
                            (approx, detail) = dwt(approx, waveletname, 'zero')
                            # if current lvl is listed in levels, use detail coefficients as features
                            if lvl in levels:
                                # Spatial Feature Extraction
                                meanabs = np.convolve(np.abs(detail), np.ones(windowSize) / windowSize, mode='valid')
                                for feature in meanabs[::windowSize]:  # down sample by W
                                    mav_features.append(feature)
                                    spatialFeatures.append(mav_features)

                # Time Features
                timeFeatures = []  # [time samples][difference features]
                for t in range(numTimeSamples):
                    frameFeatures = []
                    for feature in range(len(spatialFeatures[t])):
                        frameFeatures.append(spatialFeatures[t][feature] - spatialFeatures[t - 1][feature])
                        timeFeatures.append(frameFeatures)

                # # Collect Features into sample data with min/max values
                # for t in range(0, numTimeSamples, timeGroupSize):
                #     timeSampleFeatures = []
                #     for feature in range(len(spatialFeatures[0])):
                #         timeSampleFeatures.append(
                #             np.min([spatialFeatures[t + f][feature] for f in range(timeGroupSize)]))
                #         timeSampleFeatures.append(
                #             np.max([spatialFeatures[t + f][feature] for f in range(timeGroupSize)]))
                #     for feature in range(len(timeFeatures[0])):
                #         timeSampleFeatures.append(
                #             np.min([timeFeatures[t + f][feature] for f in range(timeGroupSize)]))
                #         timeSampleFeatures.append(
                #             np.max([timeFeatures[t + f][feature] for f in range(timeGroupSize)]))
                #     samples.add_sample(np.asarray(timeSampleFeatures), fingerID, trial)

                # Add samples directly
                for t in range(0, numTimeSamples):
                    timeSampleFeatures = []
                    for feature in range(len(spatialFeatures[0])):
                        timeSampleFeatures.append(spatialFeatures[t][feature])
                    for feature in range(len(timeFeatures[0])):
                        timeSampleFeatures.append(timeFeatures[t][feature])
                    samples.add_sample(np.asarray(timeSampleFeatures), fingerID, trial)

                # Print
                # printATBInfo(capture_file_header, channel_file_header_array)
                # print('sample_data', sample_data.shape, '(depth, channel, frame)')
                # print('Number of time samples from atb file:', len(samples.X))
                # print(fingerNames[fingerID], trial, end=' ')
                # print_elapsed_time(startTime)

        ############################
        """Pattern Classification"""
        ############################

        # Feature Selection
        # a = 0.01
        # selector = SelectFwe(f_classif, alpha=a)
        # print("Feature Selector: Fwe alpha =", a)

        # Classifier
        lda = LinearDiscriminantAnalysis()
        cnf_matrix = None
        lda_y_pred = []
        y_test = []
        lda_crossvalacc = []
        lda_f1scoremacro = []
        lda_thumbf1score = []
        lda_indexf1score = []
        lda_middlef1score = []
        lda_ringf1score = []
        lda_pinkyf1score = []

        # classify with test samples represented as the current trial number
        for trial in range(numTrials):
            samples.test_train_split_by_trial(trial)

            X_train_new = samples.X_train
            X_test_new = samples.X_test

            # Normalize Data
            scalar = StandardScaler()
            scalar.fit(X_train_new)
            X_train_new = scalar.transform(X_train_new)
            X_test_new = scalar.transform(X_test_new)

            lda_pred_prob = lda.fit(X_train_new, samples.y_train).predict_proba(X_test_new)

            lda_pred = []
            test = []
            # Max voting by deciding most common prediction value over the time period
            for t in range(0, lda_pred_prob.shape[0], timeGroupSize):
                lda_p = np.zeros(5)
                l = np.zeros(5)
                # softmax
                for i in range(timeGroupSize):
                    if t + i < lda_pred_prob.shape[0]:
                        lda_p += lda_pred_prob[t + i, :]
                        l[samples.y_test[t + i]] += 1
                lda_p /= timeGroupSize
                lda_c = lda_p.argmax()
                lda_pred.append(lda_c)
                test.append(l.argmax())

            for s in range(len(lda_pred)):
                lda_y_pred.append(lda_pred[s])
                y_test.append(test[s])

            # # Plot LDA
            # fig, axlda = plt.subplots()
            # colors = ['b', 'g', 'r', 'c', 'k']
            # samples.test_train_split_by_trial(trial)
            # # Remove features
            # lda_plot = LinearDiscriminantAnalysis(n_components=2)
            # X_train_lda = pd.DataFrame(lda_plot.fit(X_train_new, samples.y_train).transform(X_train_new))
            # X_test_lda = pd.DataFrame(lda_plot.fit(X_train_new, samples.y_train).transform(X_test_new))
            # pos_len_train = len(samples.y_train) // 5
            # pos_len_test = len(samples.y_test) // 5
            # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
            #     axlda.scatter(X_train_lda[i * pos_len_train:i * pos_len_train + pos_len_train][0],
            #                   X_train_lda[i * pos_len_train:i * pos_len_train + pos_len_train][1],
            #                   alpha=0.8, color=color, label=fingerName + ' train', marker='o')
            # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
            #     axlda.scatter(X_test_lda[i * pos_len_test:i * pos_len_test + pos_len_test][0],
            #                   X_test_lda[i * pos_len_test:i * pos_len_test + pos_len_test][1],
            #                   alpha=0.8, color=color, label=fingerName + ' test', marker='x')
            # axlda.legend(loc='best', shadow=False)
            # axlda.set_xlabel('First Discriminant')
            # axlda.set_ylabel('Second Discriminant')
            # axlda.set_title('LDA plot for trial ' + str(trial) + ' as test data')

            trial_cnf = confusion_matrix(test, lda_pred)
            lda_crossvalacc.append(accuracy_score(test, lda_pred))
            lda_f1 = f1_score(y_test, lda_y_pred, average=None)
            lda_f1scoremacro.append(f1_score(y_test, lda_y_pred, average='macro'))
            lda_thumbf1score.append(lda_f1[0])
            lda_indexf1score.append(lda_f1[1])
            lda_middlef1score.append(lda_f1[2])
            lda_ringf1score.append(lda_f1[3])
            lda_pinkyf1score.append(lda_f1[4])
            print('\nTrial', trial)
            print('Train samples:', len(X_train_new) // timeGroupSize, 'Test samples:', len(X_test_new) // timeGroupSize)
            print('Features:', len(samples.X_train[0]), '->', len(X_train_new[0]))
            print(trial_cnf)
            print(accuracy_score(test, lda_pred))

        print('Classifying:', subjectPath, 'PRF:', PRF, 'frameAvg:', frameAvg, 'timeGroupSize:', timeGroupSize)
        print('Channels', channels)
        print('levels:', levels)
        print('Trials:', Trials)
        print('Depth range:', depthRange[0], '~', depthRange[1])
        print('Complete Confusion matrix')
        print('# samples:', len(samples.X) // timeGroupSize)
        print('# features:', len(samples.X[0]))

        print('LDA')
        cnf_matrix = confusion_matrix(y_test, lda_y_pred)
        print(cnf_matrix)
        print('Class Acc: {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_crossvalacc) * 100, np.std(lda_crossvalacc) * 100))
        print('F1 Macro:  {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_f1scoremacro) * 100, np.std(lda_f1scoremacro) * 100))
        print('Thumb  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_thumbf1score) * 100, np.std(lda_thumbf1score) * 100))
        print('Index  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_indexf1score) * 100, np.std(lda_indexf1score) * 100))
        print('Middle f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_middlef1score) * 100, np.std(lda_middlef1score) * 100))
        print('Ring   f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_ringf1score) * 100, np.std(lda_ringf1score) * 100))
        print('Pinky  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_pinkyf1score) * 100, np.std(lda_pinkyf1score) * 100))

        ############################
        """Feature Evaluation"""
        ############################

        # # Plot 2D projection of features
        # fig, axpca = plt.subplots()
        # colors = ['b', 'g', 'r', 'c', 'k']
        # pca_plot = PCA(n_components=2, whiten=True, svd_solver='arpack')
        # X_pca = pd.DataFrame(pca_plot.fit(samples.X).transform(samples.X))
        # pos_len = X_pca.shape[0] // 5
        # print(X_pca.shape)
        # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
        #     axpca.scatter(X_pca[i * pos_len:i * pos_len + pos_len][0],
        #                   X_pca[i * pos_len:i * pos_len + pos_len][1],
        #                   alpha=0.8, color=color, label=fingerName)
        # axpca.legend(loc='best', shadow=False, scatterpoints=1)
        # axpca.set_xlabel('First Component')
        # axpca.set_ylabel('Second Component')
        # axpca.set_title('Features projected onto two components with PCA')

        # # Plot 2D projection of features
        # fig = plt.figure()
        # axtsne = fig.add_subplot()
        # colors = ['b', 'g', 'r', 'c', 'k']
        # tsne_plot = TSNE(n_components=2, init='pca')
        # X_tsne = pd.DataFrame(tsne_plot.fit_transform(samples.X))
        # pos_len = int(len(samples.y) / 5)
        # print(X_tsne.shape)
        # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
        #     axtsne.scatter(X_tsne[i * pos_len:i * pos_len + pos_len][0],
        #                    X_tsne[i * pos_len:i * pos_len + pos_len][1],
        #                    alpha=0.8, color=color, label=fingerName)
        # axtsne.legend(loc='best', shadow=False, scatterpoints=1)
        # axtsne.set_xlabel('First Component')
        # axtsne.set_ylabel('Second Component')
        # axtsne.set_title('Features projected onto two components with t-SNE')
        #
        # # Plot 2D projection of features
        # fig = plt.figure()
        # axmds = fig.add_subplot()
        # colors = ['b', 'g', 'r', 'c', 'k']
        # mds_plot = MDS(n_components=2)
        # X_mds = pd.DataFrame(mds_plot.fit_transform(samples.X))
        # pos_len = int(len(samples.y) / 5)
        # print(X_mds.shape)
        # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
        #     axmds.scatter(X_mds[i * pos_len:i * pos_len + pos_len][0],
        #                    X_mds[i * pos_len:i * pos_len + pos_len][1],
        #                    alpha=0.8, color=color, label=fingerName)
        # axmds.legend(loc='best', shadow=False, scatterpoints=1)
        # axmds.set_xlabel('First Component')
        # axmds.set_ylabel('Second Component')
        # axmds.set_title('Features projected onto two components with MDS')


        # plt.show()

"""Test zrfManager, and file format etc.
"""
from python.src.zrfManager import RFContainer
import python.src.signalProcessor as sigProc
import python.src.patternClassification as pc


if __name__ == "__main__":
    dataFolder = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/zrfFiles" \
                 "/AlexFingerMovement/FingerMovement/"

    rfcontainer = RFContainer()
    rfcontainer.load_zrf("Thumb1", dataFolder + "Thumb1.zrf")
    rfcontainer.load_zrf("Thumb2", dataFolder + "Thumb2.zrf")
    rfcontainer.load_zrf("Thumb3", dataFolder + "Thumb3.zrf")

    print("label='Thumb1', size:", rfcontainer.get_rfsig("Thumb1").shape)
    print("label='Thumb2', size:", rfcontainer.get_rfsig("Thumb2").shape)
    print("label='Thumb3', size:", rfcontainer.get_rfsig("Thumb3").shape)

    rfsig = rfcontainer.get_rfsig("Thumb1")
    print("Reduce by average int into 2 z columns:",
          sigProc.average_into_zcols(rfsig, 2).shape)

    print("Reduce to 2 z columns on frame 50:",
          sigProc.average_into_zcols_by_frame(rfsig[:,:,50], 2).shape)

    print("Extract features from frame 50: num features =",
          len(pc.exctract_linear_fit_segments(rfsig[:,:,50], 2, 100)))

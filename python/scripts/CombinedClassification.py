"""Combination of linear fit and wavelet feature extraction methods
"""

"""Classify finger motions using wavelet coefficients as features

"""
import xlsxwriter

from python.src.zrfManager import ZRFData
import python.src.patternClassification as pc
import numpy as np
import time
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import confusion_matrix, accuracy_score


def print_elapsed_time(startTime):
    elapsed_time = time.time()
    hours, rem = divmod(elapsed_time - startTime, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Elapsed Time: {:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))


def write_cnf_worksheet(worksheet, cnf_matrix, zcols, acc, row, col):
    worksheet.write(row, col, 'zcols:')
    worksheet.write(row, col + 1, zcols)
    worksheet.write(row, col + 2, 'acc:')
    worksheet.write(row, col + 3, acc)
    for thumb, index, middle, ring, pinky in cnf_matrix:
        worksheet.write(row + 1, col, thumb)
        worksheet.write(row + 1, col + 1, index)
        worksheet.write(row + 1, col + 2, middle)
        worksheet.write(row + 1, col + 3, ring)
        worksheet.write(row + 1, col + 4, pinky)
        row += 1


if __name__ == "__main__":
    # Time Start
    startTime = time.time()

    numTrials = 3
    frameStart = 0
    frameEnd = 180
    frameGroupSize = 3
    numFrames = frameEnd - frameStart
    waveletname = 'db3'
    levels = [2, 3, 4, 5]
    windowSize = 2  # dsize as in window size along the depth axis
    numSegments = 100

    dataDir = '/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/' \
              'ultrasoundclassification/data/'
    workbook = xlsxwriter.Workbook(dataDir + 'accuracy/combined-linfit-db3_test1.xlsx')

    fingerNames = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    fingerLabel = {'Thumb': 0, 'Index': 1, 'Middle': 2, 'Ring': 3, 'Pinky': 4}

    # numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 40, 50, 60, 70, 80, 90,
    #                100, 110, 120, 127]
    numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

    zrfDir = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/zrfFiles/"

    subjectPaths = ["AlexFingerMovement/FingerMovement/",
                    "AlexRightFingerMovement/RightFingerMotions/",
                    "fm_sub1_l/FingerMotions/",
                    "fm_sub1_r/FingerMotions/",
                    "fm_sub2_l/FingerMotions/",
                    "fm_sub2_r/FingerMotions/",
                    "fm_sub3_l/FingerMotions/",
                    "fm_sub3_r/FingerMotions/",
                    "fm_sub4_l/FingerMotions/",
                    "fm_sub4_r/FingerMotions/"]

    for subjectPath in subjectPaths:
        filePath = zrfDir + subjectPath

        print(subjectPath)
        print('numZcolumns', numZColumns)
        print('numTrials', numTrials)
        print('numFrames', numFrames)
        print('frameGroupSize', frameGroupSize)

        worksheet = workbook.add_worksheet(subjectPath.split('/')[0])
        for zcols in numZColumns:

            samples = pc.DataContainer()  # container to put extracted features and samples to then classify

            for fingerID in range(len(fingerNames)):
                for trial in range(1, numTrials + 1):
                    # print(fingerNames[fingerID], trial, end=' ')
                    # print_elapsed_time(startTime)
                    # load ultrasound .zrf file
                    zrf = ZRFData(filePath + fingerNames[fingerID] + str(trial) + '.zrf')

                    # extract wavelet coefficient features for each frame of the rf data
                    # list of a list: [numFrames][zcols*N]; N is based on the level of wavelet scales
                    exctractedFeatures = []
                    for frame in range(frameStart, frameEnd):  # frames are representative as samples
                        feature_per_sample = []
                        for feature in pc.exctract_linear_fit_segments(zrf.rfsig[:, :, frame],  # rf signal
                                                                       zcols,  # num z columns to average to
                                                                       numSegments):  # num depth segments
                            feature_per_sample.append(feature)
                        for feature in pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
                                                                       zcols,  # num z columns to average to
                                                                       waveletname,  # wavelet
                                                                       levels,  # levels
                                                                       windowSize):  # window size for mean(abs())
                            feature_per_sample.append(feature)
                        exctractedFeatures.append(feature_per_sample)  # num depth segments
                    # obtain difference between frames as features
                    differenceFeatures = []  # same as linearFitFeatures
                    for frame in range(numFrames):
                        frameFeatures = []
                        for feature in range(len(exctractedFeatures[frame])):
                            frameFeatures.append(
                                exctractedFeatures[frame][feature] - exctractedFeatures[frame - 1][feature])
                        differenceFeatures.append(frameFeatures)

                    # group frames into group size and obtain min/max values and add the samples to data set
                    # sample size reduces by frameGroupSize amount i.e. 540 samples / 3 frameGroupSize = 180 samples
                    for frame in range(0, numFrames, frameGroupSize):
                        frameFeatures = []
                        for feature in range(len(exctractedFeatures[frame])):
                            frameFeatures.append(
                                np.min([exctractedFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.max([exctractedFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.min([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.max([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))

                        # each frame is represented as a sample
                        samples.add_sample(np.asarray(frameFeatures), fingerID, trial)

            # Split into test / training data
            lda = LinearDiscriminantAnalysis()
            cnf_matrix = None
            y_pred = []
            y_test = []

            # classify with test samples represented as the current trial number
            for trial in range(1, numTrials + 1):
                samples.test_train_split_by_trial(trial)

                pred = lda.fit(samples.X_train, samples.y_train).predict(samples.X_test)

                for s in range(len(pred)):
                    y_pred.append(pred[s])
                    y_test.append(samples.y_test[s])

            print('# zcols:', zcols)
            print('# samples:', len(samples.X))
            print('# features:', len(samples.X[0]))
            cnf_matrix = confusion_matrix(y_test, y_pred)
            print(cnf_matrix)
            print('Accuracy:', accuracy_score(y_test, y_pred))
            print_elapsed_time(startTime)

            write_cnf_worksheet(worksheet, cnf_matrix, zcols, accuracy_score(y_test, y_pred), (zcols - 1) * 6, 0)
    workbook.close()

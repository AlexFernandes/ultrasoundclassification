"""Perform classification for specific resolution
"""
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from python.src.zrfManager import ZRFData
import python.src.patternClassification as pc
import numpy as np
import time
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.decomposition import PCA
from sklearn.metrics import confusion_matrix, accuracy_score
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import SelectFwe, f_classif
from mlxtend.plotting import plot_decision_regions


def print_elapsed_time(startTime):
    elapsed_time = time.time()
    hours, rem = divmod(elapsed_time - startTime, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Elapsed Time: {:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))


if __name__ == "__main__":
    # Time Start
    startTime = time.time()

    # Parameters
    numTrials = 3
    frameStart = 0
    frameEnd = 180
    frameGroupSize = 3
    numFrames = frameEnd - frameStart
    waveletname = 'db3'
    levels = [1, 2, 3]
    windowSize = 2  # dsize as in window size along the depth axis
    numSegments = 100

    # Resolution
    zcols = 127

    fingerNames = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    fingerLabel = {'Thumb': 0, 'Index': 1, 'Middle': 2, 'Ring': 3, 'Pinky': 4}

    # numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 40, 50, 60, 70, 80, 90,
    #                100, 110, 120, 127]
    # numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    numZColumns = [4, 6, 8]

    zrfDir = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/zrfFiles/"
    # zrfDir = "D:/Code/zrfFiles/"

    # subjectPaths = ["AlexFingerMovement/FingerMovement/",
    #                 "AlexRightFingerMovement/RightFingerMotions/",
    #                 "fm_sub1_l/FingerMotions/",
    #                 "fm_sub1_r/FingerMotions/",
    #                 "fm_sub2_l/FingerMotions/",
    #                 "fm_sub2_r/FingerMotions/",
    #                 "fm_sub3_l/FingerMotions/",
    #                 "fm_sub3_r/FingerMotions/",
    #                 "fm_sub4_l/FingerMotions/",
    #                 "fm_sub4_r/FingerMotions/"]
    # subjectPaths = ["fm_sub4_l/FingerMotions/"]
    subjectPaths = ["fm_sub2_l/FingerMotions/"]

    print('Resolution: zcols =', zcols)

    for subjectPath in subjectPaths:

        filePath = zrfDir + subjectPath

        print(subjectPath)
        samples = pc.DataContainer()  # container to put extracted features and samples to then classify

        for fingerID in range(len(fingerNames)):
            for trial in range(1, numTrials + 1):
                print(fingerNames[fingerID], trial, end=' ')
                print_elapsed_time(startTime)
                # load ultrasound .zrf file
                zrf = ZRFData(filePath + fingerNames[fingerID] + str(trial) + '.zrf')

                # extract wavelet coefficient features for each frame of the rf data
                # list of a list: [numFrames][zcols*N]; N is based on the level of wavelet scales
                exctractedFeatures = []
                for frame in range(frameStart, frameEnd):  # frames are representative as samples
                    feature_per_sample = []

                    # # linear fit features
                    # for feature in pc.exctract_linear_fit_segments(zrf.rfsig[:, :, frame],  # rf signal
                    #                                                zcols,  # num z columns to average to
                    #                                                numSegments):  # num depth segments
                    #     feature_per_sample.append(feature)

                    # wavelet features
                    for feature in pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
                                                                   zcols,  # num z columns to average to
                                                                   waveletname,  # wavelet
                                                                   levels,  # levels
                                                                   windowSize):  # window size for mean(abs())
                        feature_per_sample.append(feature)

                    exctractedFeatures.append(feature_per_sample)  # num depth segments
                # obtain difference between frames as features
                differenceFeatures = []  # same as linearFitFeatures
                for frame in range(numFrames):
                    frameFeatures = []
                    for feature in range(len(exctractedFeatures[frame])):
                        frameFeatures.append(
                            exctractedFeatures[frame][feature] - exctractedFeatures[frame - 1][feature])
                    differenceFeatures.append(frameFeatures)

                # group frames into group size and obtain min/max values and add the samples to data set
                # sample size reduces by frameGroupSize amount i.e. 540 samples / 3 frameGroupSize = 180 samples
                for frame in range(0, numFrames, frameGroupSize):
                    frameFeatures = []
                    for feature in range(len(exctractedFeatures[frame])):
                        frameFeatures.append(
                            np.min([exctractedFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                        frameFeatures.append(
                            np.max([exctractedFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                        frameFeatures.append(
                            np.min([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                        frameFeatures.append(
                            np.max([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))

                    # collection of frames are represented as a sample
                    samples.add_sample(np.asarray(frameFeatures), fingerID, trial)

                # # add samples per frame from features directly
                # for frame in range(0, numFrames):
                #     frameFeatures = []
                #     for feature in range(len(exctractedFeatures[frame])):
                #         frameFeatures.append(exctractedFeatures[frame][feature])
                #     for feature in range(len(differenceFeatures[frame])):
                #         frameFeatures.append(differenceFeatures[frame][feature])
                #     samples.add_sample(np.asarray(frameFeatures), fingerID, trial)

        ############################
        """Pattern Classification"""
        ############################

        font = {'family': 'normal',
                'weight': 'normal',
                'size': 16}
        matplotlib.rc('font', **font)

        # Normalize Data
        samples.X = StandardScaler().fit_transform(samples.X)

        # Feature Selection
        a = 0.05
        selector = SelectFwe(f_classif, alpha=a)
        print("Feature Selector: Fwe alpha =", a)

        # Classifier
        lda = LinearDiscriminantAnalysis()
        cnf_matrix = None
        y_pred = []
        y_test = []
        crossvalacc = []

        # fig, axlda = plt.subplots(ncols=3)
        # fig, axlda = plt.subplots()

        # classify with test samples represented as the current trial number
        for trial in range(1, numTrials + 1):
            samples.test_train_split_by_trial(trial)

            # Remove features
            X_train_new = selector.fit_transform(samples.X_train, samples.y_train)
            X_test_new = selector.transform(samples.X_test)

            pred = lda.fit(X_train_new, samples.y_train).predict(X_test_new)
            pred_prob = lda.predict_log_proba(X_test_new)

            for s in range(len(pred)):
                y_pred.append(pred[s])
                y_test.append(samples.y_test[s])

            print('Trial', trial)
            trial_cnf = confusion_matrix(samples.y_test, pred)
            crossvalacc.append(accuracy_score(samples.y_test, pred))
            print(trial_cnf)
            print(accuracy_score(samples.y_test, pred))

            # # if subjectPath == subjectPaths[0] and trial == 1:
            #     # Plot Time Predictions
            #     fig, ax = plt.subplots()
            #     for c in range(5):
            #         ax.plot(np.arange(0, pred_prob.shape[0])/10, pred_prob[:, c])
            #     ax.legend(fingerNames)
            #     ax.set_xticks([0, 6, 12, 18, 24, 30])
            #     ax.set_xlabel('Time (sec)')
            #     ax.set_title('Subject 3 left arm\nPredicting 100 ms intervals of trial 1 finger flexions')
            #     ax.set_ylabel("Log probability of finger flexion")
            #     plt.grid()

            # Plot LDA trial specific
            # if trial == 2:
            #     colors = ['b', 'g', 'r', 'c', 'k']
            #     lda_plot = LinearDiscriminantAnalysis(n_components=2)
            #     X_train_lda = pd.DataFrame(lda_plot.fit(X_train_new, samples.y_train).transform(X_train_new))
            #     X_test_lda = pd.DataFrame(lda_plot.fit(X_train_new, samples.y_train).transform(X_test_new))
            #     pos_len_train = len(samples.y_train) // 5
            #     pos_len_test = len(samples.y_test) // 5
            #     for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
            #         axlda.scatter(X_train_lda[i * pos_len_train:i * pos_len_train + pos_len_train][0],
            #                                  X_train_lda[i * pos_len_train:i * pos_len_train + pos_len_train][1],
            #                                  alpha=0.8, color=color, label=fingerName + ' train', marker='.')
            #     for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
            #         axlda.scatter(X_test_lda[i * pos_len_test:i * pos_len_test + pos_len_test][0],
            #                                  X_test_lda[i * pos_len_test:i * pos_len_test + pos_len_test][1],
            #                                  alpha=0.8, color=color, label=fingerName + ' test', marker='x')
            #     axlda.set_xlabel('First Discriminant')
            #     axlda.set_ylabel('Second Discriminant')
            #     # axlda.set_title('Trial ' + str(trial) + ' as test data')
            #     axlda.legend(loc='upper left', shadow=False, bbox_to_anchor=(1.05, 1))

            # Plot LDA with decision region
            if trial == 1:
                colors = ['b', 'g', 'r', 'c', 'k']
                lda_reduce = LinearDiscriminantAnalysis(n_components=2)
                X_train_reduce = lda_reduce.fit(X_train_new, samples.y_train).transform(X_train_new)
                X_test_reduce = lda_reduce.transform(X_test_new)
                lda_plot = LinearDiscriminantAnalysis()
                lda_plot.fit(X_train_reduce, samples.y_train)
                ax = plot_decision_regions(X=X_test_reduce, y=np.asarray(samples.y_test), clf=lda_plot, legend=0)
                plt.xlabel('First Discriminant')
                plt.ylabel('Second Discriminant')
                ax.set_title(subjectPath + '\nN=' + str(zcols) + ' test data trial ' + str(trial))
                handles, labels = ax.get_legend_handles_labels()
                ax.legend(handles, fingerNames, loc='center left', framealpha=0.3, scatterpoints=1, bbox_to_anchor=(1, 0.5))



            # # Plot LDA all trial
            # colors = ['b', 'g', 'r', 'c', 'k']
            # lda_plot = LinearDiscriminantAnalysis(n_components=2)
            # X_train_lda = pd.DataFrame(lda_plot.fit(X_train_new, samples.y_train).transform(X_train_new))
            # X_test_lda = pd.DataFrame(lda_plot.fit(X_train_new, samples.y_train).transform(X_test_new))
            # pos_len_train = len(samples.y_train) // 5
            # pos_len_test = len(samples.y_test) // 5
            # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
            #     axlda[trial-1].scatter(X_train_lda[i * pos_len_train:i * pos_len_train + pos_len_train][0],
            #                   X_train_lda[i * pos_len_train:i * pos_len_train + pos_len_train][1],
            #                   alpha=0.8, color=color, label=fingerName+' train', marker='.')
            # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
            #     axlda[trial-1].scatter(X_test_lda[i * pos_len_test:i * pos_len_test + pos_len_test][0],
            #                   X_test_lda[i * pos_len_test:i * pos_len_test + pos_len_test][1],
            #                   alpha=0.8, color=color, label=fingerName+' test', marker='x')
            # axlda[trial-1].set_xlabel('First Discriminant')
            # axlda[trial-1].set_ylabel('Second Discriminant')
            # axlda[trial-1].set_title('Trial ' + str(trial) + ' as test data')
            # axlda[2].legend(loc='upper left', shadow=False, bbox_to_anchor=(1.05, 1))

        # # Plot PCA
        # fig = plt.figure()
        # axpca = fig.add_subplot(1, 1, 1)
        # colors = ['b', 'g', 'r', 'c', 'k']
        # pca_plot = PCA(n_components=2, whiten=True, svd_solver='arpack')
        # X_pca = pd.DataFrame(pca_plot.fit(samples.X).transform(samples.X))
        # pos_len = int(len(samples.y) / 5)
        # print(X_pca.shape)
        # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
        #     axpca.scatter(X_pca[i * pos_len:i * pos_len + pos_len][0],
        #                   X_pca[i * pos_len:i * pos_len + pos_len][1],
        #                   alpha=0.8, color=color, label=fingerName)
        # axpca.legend(loc='best', shadow=False, scatterpoints=1)
        # axpca.set_xlabel('First Principal Component')
        # axpca.set_ylabel('Second Principal Component')\


        # # Plot PCA
        # fig = plt.figure()
        # axpca = fig.add_subplot(111, projection='3d')
        # colors = ['b', 'g', 'r', 'c', 'k']
        # pca_plot = PCA(n_components=3, whiten=True, svd_solver='arpack')
        # X_pca = pd.DataFrame(pca_plot.fit(samples.X).transform(samples.X))
        # pos_len = int(len(samples.y) / 5)
        # print(X_pca.shape)
        # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
        #     axpca.scatter(X_pca[i * pos_len:i * pos_len + pos_len][0],
        #                   X_pca[i * pos_len:i * pos_len + pos_len][1],
        #                   X_pca[i * pos_len:i * pos_len + pos_len][2],
        #                   alpha=0.8, color=color, label=fingerName)
        # axpca.legend(loc='best', shadow=False, scatterpoints=1)
        # axpca.set_xlabel('First Principal Component')
        # axpca.set_ylabel('Second Principal Component')
        # axpca.set_zlabel('Third Principal Component')\

        print('Complete Confusion matrix')
        print('# samples:', len(samples.X))
        print('# features:', len(samples.X[0]))
        cnf_matrix = confusion_matrix(y_test, y_pred)
        print(cnf_matrix)
        print('Accuracy: {} +/- {} %'.format(np.mean(crossvalacc) * 100, np.std(crossvalacc) * 100))
        print_elapsed_time(startTime)

        # print('pca explained variance ratio of discriminant functions:')
        # print(pca_plot.explained_variance_ratio_)
    plt.show()

from python.src.readATB import read_atb
import matplotlib.pyplot as plt

if __name__ == '__main__':
    atbFileDir = 'D:/Code/ultrasoundclassification/data/alazarTest/'
    atbFile = 'deform_test_withupdate.atb'
    # atbFile = 'deform_test_noupdate.atb'
    # atbFile = 'deform_test2_withupdate.atb'
    # atbFile = 'deform_test2_noupdate.atb'

    capture_file_header, \
    channel_file_header_array, \
    sample_data, \
    frame_time_stamps = read_atb(atbFileDir + atbFile)

    # The capture_file_header is instantiated with the following variables:
    # capture_file_header.m_dwType                  str (4 characters or less)
    # capture_file_header.m_dwSize                  numpy.uint32
    # capture_file_header.m_nBitsPerSample          numpy.int32
    # capture_file_header.m_nBytesPerSample         numpy.int32
    # capture_file_header.m_uSamplesPerRecord       numpy.uint32
    # capture_file_header.m_dSamplesPerSecond       numpy.double
    # capture_file_header.m_dSecondsPerRecord       numpy.double
    # capture_file_header.m_nTrigDelay_samples      numpy.int32
    # capture_file_header.m_uPreTrig_samples        numpy.uint32
    # capture_file_header.m_nRecordsPerCapture      numpy.int32
    # capture_file_header.m_nCapturesPerFile        numpy.int32
    # capture_file_header.m_nChannels               numpy.int32
    # capture_file_header.m_cComment                str (32 characters or less)
    # capture_file_header.m_dwFlags                 numpy.uint32
    # capture_file_header.m_dwDataOffset_bytes      numpy.uint32
    # capture_file_header.m_dwStreamFlags           numpy.uint32
    # capture_file_header.m_nTimestampMultiplier    numpy.uint32

    # Eg. print capture file header
    template = "{:<23} | {}"
    print(template.format('Capture file header', 'Value'))
    for k, v in sorted(vars(capture_file_header).items()):
        if k[0:2] == 'm_':
            print(template.format(k, v))
    print('\n')


    # The channel_file_header_array is instantiated with the following variables:
    # channel_file_header_array[channel].m_bDisabled            bool
    # channel_file_header_array[channel].m_dInputRange_volts    np.double
    # channel_file_header_array[channel].m_dProbeAttenuation    np.double

    # Eg. print each header in channel file header array
    for channel in range(capture_file_header.m_nChannels):
        print(template.format('Channel %d' % channel, 'Value'))
        for k, v in sorted(vars(channel_file_header_array[0]).items()):
            if k[0:2] == 'm_':
                print(template.format(k, v))
        print()


    frameRate = 1 / capture_file_header.m_dSecondsPerRecord
    print('Calculated frame rate:', frameRate, 'frames / second') # I found that you can't rely on the frame rate

    # Eg. plot all 4 channels from a given frame
    frame = 3000
    depth = capture_file_header.m_uSamplesPerRecord
    totalFrames = capture_file_header.m_nCapturesPerFile * capture_file_header.m_nRecordsPerCapture
    plt.figure(0)
    for channel in range(capture_file_header.m_nChannels):
        plt.plot(sample_data[0 : depth - 1, channel, frame], label='channel %d' % channel)
    plt.title('%s\nframe %d/%d' % (atbFile, frame,
                                   totalFrames - 1))
    plt.legend()
    plt.xlabel('depth')
    plt.ylabel('amplitude')


    # Eg. plot channel 0 in M-mod
    channel = 0

    plt.figure(1)
    plt.imshow(sample_data[:, channel, :], aspect='auto')
    plt.title('%s\nchannel %d' % (atbFile, channel))
    plt.xlabel('frame')
    plt.ylabel('depth')

    plt.show()
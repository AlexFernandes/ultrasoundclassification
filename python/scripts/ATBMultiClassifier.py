"""Classification using ATB file data
"""

import matplotlib.pyplot as plt
from python.src.readATB import read_atb
import python.src.patternClassification as pc
import numpy as np
import time
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.metrics import confusion_matrix, accuracy_score, f1_score
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import SelectFwe, f_classif
from pywt._dwt import dwt
import pandas as pd

import warnings
warnings.filterwarnings("ignore")



def print_elapsed_time(startTime):
    elapsed_time = time.time()
    hours, rem = divmod(elapsed_time - startTime, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Elapsed Time: {:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))


def printATBInfo(capture_file_header, channel_file_header_array):
    template = "{:<23} | {}"
    print(template.format('Capture file header', 'Value'))
    for k, v in sorted(vars(capture_file_header).items()):
        if k[0:2] == 'm_':
            print(template.format(k, v))
    print()

    for channel in range(capture_file_header.m_nChannels):
        print(template.format('Channel %d' % channel, 'Value'))
        for k, v in sorted(vars(channel_file_header_array[0]).items()):
            if k[0:2] == 'm_':
                print(template.format(k, v))
        print()


if __name__ == '__main__':
    # Time Start
    startTime = time.time()

    # ATB Files
    # atbDir = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/AlazarTech/"
    atbDir = "D:/Code/atbFiles/"
    subjectPath = "1kPRF_FingerFlexions_3channel/"
    PRF = 1000  # list of Pulse Repetition Frequency or Frame Rate (Hz) corresponding to subjectPaths
    fingerNames = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']  # class numbered by this ordering
    # Trials = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    # Trials = [1, 3, 5, 7, 8]
    Trials = [5, 6, 8]
    numTrials = len(Trials)
    print(numTrials)
    channels = [0, 2, 3]  # Channels to use

    # Pre-processing & Feature Extraction Parameters
    waveletname = 'db3'
    depthRange = [320, 1520]
    levels = [4, 5, 6]  # make in ascending order
    frameAvg = 1  # number of frames to average for purposes of reducing noise
    timeGroupSize = 100  # number of time samples to perform max voting
    windowSize = 2  # size for applying the Mean Absolute Value function

    # for (frameAvg, timeGroupSize) in zip([20, 10,  5,  4],
    #                                      [ 5, 10, 20, 25]):
    # for (frameAvg, timeGroupSize) in zip([1, 2],
    #                                      [100, 50]):
    for (frameAvg, timeGroupSize) in zip([4],
                                         [25]):
    # for (timeGroupSize, frameAvg) in zip([1, 2, 4, 5, 10, 20, 25, 50, 100],
    #                                      [100, 50, 25, 20, 10, 5, 4, 2, 1]):
        print('######################################################################')

        # Classify Subject Data
        samples = pc.DataContainer()  # container to put extracted features and samples of complete data set

        # extract features by loading one atb file at a time
        for fingerID in range(len(fingerNames)):
            # Print
            # print(fingerNames[fingerID], end=' ')
            # print_elapsed_time(startTime)

            for trial in range(numTrials):
                capture_file_header, channel_file_header_array, atb_sample_data, frame_time_stamps = read_atb(
                    atbDir + subjectPath + fingerNames[fingerID] + str(Trials[trial]) + '.atb')

                # number of time samples obtained from the atb file recording
                numTimeSamples = atb_sample_data.shape[2] // frameAvg

                # Preprocess acquired data
                # sample_data = np.zeros(atb_sample_data.shape, dtype=np.dtype('float64'))
                sample_data = np.zeros(atb_sample_data.shape, dtype=np.dtype('float32'))
                for c in channels:
                    for f in range(sample_data.shape[2]):
                        # Format to volts
                        dSampleZeroValue = (1 << (capture_file_header.m_nBitsPerSample - 1)) - 0.5
                        dSampleMaxValue = (1 << (capture_file_header.m_nBitsPerSample - 1)) - 0.5
                        nSampleBitShift = (capture_file_header.m_nBytesPerSample * 8) - capture_file_header.m_nBitsPerSample
                        sample_data[:, c, f] = np.asarray(atb_sample_data[:, c, f] >> nSampleBitShift,
                                                          # dtype=np.dtype('float64'))
                                                          dtype=np.dtype('float32'))
                        dMaxVolts = channel_file_header_array[c].m_dInputRange_volts * channel_file_header_array[
                            c].m_dProbeAttenuation
                        sample_data[:, c, f] = dMaxVolts * (sample_data[:, c, f] - dSampleZeroValue) / dSampleMaxValue
                        # DC removal
                        sample_data[:, c, f] -= np.mean(sample_data[:, c, f])

                # Spatial Features
                spatialFeatures = []  # [time samples][spatial features]
                for t in range(numTimeSamples):
                    mav_features = []
                    for channel in channels:
                        # Reduce variance of noise by averaging into the time samples
                        # approx = np.zeros(depthRange[1] - depthRange[0], dtype=np.dtype('float64'))  # 1-D RF signal
                        approx = np.zeros(depthRange[1] - depthRange[0], dtype=np.dtype('float32'))  # 1-D RF signal
                        for d in range(depthRange[0], depthRange[1]):
                            approx[d - depthRange[0]] = np.mean(
                                sample_data[d, channel, t * frameAvg:(t + 1) * frameAvg])
                        # DC removal
                        approx -= np.mean(approx)
                        # Discrete Wavelet Transform applied along depth axis
                        for lvl in range(1, levels[-1]+1):
                            # recursively obtain wavelet 'detail' coefficients using previous approx coefficients
                            (approx, detail) = dwt(approx, waveletname, 'zero')
                            # if current lvl is listed in levels, use detail coefficients as features
                            if lvl in levels:
                                # Spatial Feature Extraction
                                meanabs = np.convolve(np.abs(detail), np.ones(windowSize) / windowSize, mode='valid')
                                for feature in meanabs[::windowSize]:  # down sample by W
                                    mav_features.append(feature)
                                    spatialFeatures.append(mav_features)

                # Time Features
                timeFeatures = []  # [time samples][difference features]
                for t in range(numTimeSamples):
                    frameFeatures = []
                    for feature in range(len(spatialFeatures[t])):
                        frameFeatures.append(spatialFeatures[t][feature] - spatialFeatures[t - 1][feature])
                        timeFeatures.append(frameFeatures)

                # # Collect Features into sample data with min/max values
                # for t in range(0, numTimeSamples, timeGroupSize):
                #     timeSampleFeatures = []
                #     for feature in range(len(spatialFeatures[t])):
                #         timeSampleFeatures.append(
                #             np.min([spatialFeatures[t + f][feature] for f in range(timeGroupSize)]))
                #         timeSampleFeatures.append(
                #             np.max([spatialFeatures[t + f][feature] for f in range(timeGroupSize)]))
                #     for feature in range(len(timeFeatures[t])):
                #         timeSampleFeatures.append(
                #             np.min([timeFeatures[t + f][feature] for f in range(timeGroupSize)]))
                #         timeSampleFeatures.append(
                #             np.max([timeFeatures[t + f][feature] for f in range(timeGroupSize)]))
                #     samples.add_sample(np.asarray(timeSampleFeatures), fingerID, trial)

                # Add samples directly
                for t in range(0, numTimeSamples):
                    timeSampleFeatures = []
                    for feature in range(len(spatialFeatures[0])):
                        timeSampleFeatures.append(spatialFeatures[t][feature])
                    for feature in range(len(timeFeatures[0])):
                        timeSampleFeatures.append(timeFeatures[t][feature])
                    samples.add_sample(np.asarray(timeSampleFeatures), fingerID, trial)

                # Print
                # printATBInfo(capture_file_header, channel_file_header_array)
                # print('sample_data', sample_data.shape, '(depth, channel, frame)')
                # print('Number of time samples from atb file:', len(samples.X))
                # print(fingerNames[fingerID], trial, end=' ')
                # print_elapsed_time(startTime)

        ############################
        """Pattern Classification"""
        ############################
        print('X = ', len(samples.X), len(samples.X[0]))
        # Feature Selection
        a = 0.01
        selector = SelectFwe(f_classif, alpha=a)
        print("Feature Selector: Fwe alpha =", a)

        # Classifier
        # lda = LinearDiscriminantAnalysis()
        # mlp = MLPClassifier(hidden_layer_sizes=(15))
        knn = KNeighborsClassifier(n_neighbors=3)
        print('4 components\n1 layer, 15 nodes\nk=3')
        # eclf = VotingClassifier(estimators=[('lda', lda), ('mlp', mlp), ('knn', knn)], voting='soft')
        cnf_matrix = None
        # lda_y_pred = []
        # lda_y_test = []
        # mlp_y_pred = []
        # mlp_y_test = []
        knn_y_pred = []
        knn_y_test = []
        y_pred = []
        y_test = []
        # lda_crossvalacc = []
        # lda_thumbf1score = []
        # lda_indexf1score = []
        # lda_middlef1score = []
        # lda_ringf1score = []
        # lda_pinkyf1score = []
        # mlp_crossvalacc = []
        # mlp_thumbf1score = []
        # mlp_indexf1score = []
        # mlp_middlef1score = []
        # mlp_ringf1score = []
        # mlp_pinkyf1score = []
        knn_crossvalacc = []
        knn_thumbf1score = []
        knn_indexf1score = []
        knn_middlef1score = []
        knn_ringf1score = []
        knn_pinkyf1score = []
        crossvalacc = []
        thumbf1score = []
        indexf1score = []
        middlef1score = []
        ringf1score = []
        pinkyf1score = []

        # classify with test samples represented as the current trial number
        for trial in range(numTrials):
            samples.test_train_split_by_trial(trial)

            # Remove features
            X_train_new = selector.fit_transform(samples.X_train, samples.y_train)
            X_test_new = selector.transform(samples.X_test)
            # X_train_new = samples.X_train
            # X_test_new = samples.X_test

            # Normalize Data
            scalar = StandardScaler()
            scalar.fit(X_train_new)
            X_train_new = scalar.transform(X_train_new)
            X_test_new = scalar.transform(X_test_new)

            # lda_pred_prob = lda.fit(X_train_new, samples.y_train).predict_proba(X_test_new)
            # mlp_pred_prob = mlp.fit(X_train_new, samples.y_train).predict_proba(X_test_new)
            knn_pred_prob = knn.fit(X_train_new, samples.y_train).predict_proba(X_test_new)
            # pred_prob = eclf.fit(X_train_new, samples.y_train).predict_proba(X_test_new)

            # lda_pred = []
            # mlp_pred = []
            knn_pred = []
            # pred = []
            test = []
            # Max voting by deciding most common prediction value over the time period
            # for t in range(0, lda_pred_prob.shape[0], 1):
            for t in range(0, knn_pred_prob.shape[0], timeGroupSize):
                # lda_p = np.zeros(5)
                # mlp_p = np.zeros(5)
                knn_p = np.zeros(5)
                # p = np.zeros(5)
                l = np.zeros(5)
                # softmax
                # for i in range(1):
                for i in range(timeGroupSize):
                    if t + i < knn_pred_prob.shape[0]:
                        # lda_p += lda_pred_prob[t + i, :]
                        # mlp_p += mlp_pred_prob[t + i, :]
                        knn_p += knn_pred_prob[t + i, :]
                        # p += pred_prob[t + i, :]
                        l[samples.y_test[t + i]] += 1
                # lda_p /= timeGroupSize
                # mlp_p /= timeGroupSize
                knn_p /= timeGroupSize
                # p /= timeGroupSize
                # lda_c = lda_p.argmax()
                # mlp_c = mlp_p.argmax()
                knn_c = knn_p.argmax()
                # c = p.argmax()
                np.set_printoptions(formatter={'float': lambda x: "{0:0.4f}".format(x)})
                # if c != l.argmax():
                #     print((t * frameAvg * timeGroupSize / PRF) % 6, l.argmax(), 'pred:', c, lda_p, mlp_p, knn_p)
                # else:
                #     print((t * frameAvg * timeGroupSize / PRF) % 6, l.argmax(), 'pred:', c, p)
                # lda_pred.append(lda_c)
                # mlp_pred.append(mlp_c)
                knn_pred.append(knn_c)
                # pred.append(c)
                test.append(l.argmax())

            for s in range(len(knn_pred)):
                # lda_y_pred.append(lda_pred[s])
                # mlp_y_pred.append(mlp_pred[s])
                knn_y_pred.append(knn_pred[s])
                # y_pred.append(pred[s])
                y_test.append(test[s])

            # # Plot LDA
            # fig, axlda = plt.subplots()
            # colors = ['b', 'g', 'r', 'c', 'k']
            # samples.test_train_split_by_trial(trial)
            # # Remove features
            # X_train_new = selector.fit_transform(samples.X_train, samples.y_train)
            # X_test_new = selector.transform(samples.X_test)
            # lda_plot = LinearDiscriminantAnalysis(n_components=2)
            # X_train_lda = pd.DataFrame(lda_plot.fit(X_train_new, samples.y_train).transform(X_train_new))
            # X_test_lda = pd.DataFrame(lda_plot.fit(X_train_new, samples.y_train).transform(X_test_new))
            # pos_len_train = len(samples.y_train) // 5
            # pos_len_test = len(samples.y_test) // 5
            # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
            #     axlda.scatter(X_train_lda[i * pos_len_train:i * pos_len_train + pos_len_train][0],
            #                   X_train_lda[i * pos_len_train:i * pos_len_train + pos_len_train][1],
            #                   alpha=0.8, color=color, label=fingerName + ' train', marker='o')
            # for color, i, fingerName in zip(colors, [0, 1, 2, 3, 4], fingerNames):
            #     axlda.scatter(X_test_lda[i * pos_len_test:i * pos_len_test + pos_len_test][0],
            #                   X_test_lda[i * pos_len_test:i * pos_len_test + pos_len_test][1],
            #                   alpha=0.8, color=color, label=fingerName + ' test', marker='x')
            # axlda.legend(loc='best', shadow=False)
            # axlda.set_xlabel('First Discriminant')
            # axlda.set_ylabel('Second Discriminant')
            # axlda.set_title('LDA plot for trial ' + str(trial) + ' as test data')

            trial_cnf = confusion_matrix(test, knn_pred)
            # lda_crossvalacc.append(accuracy_score(test, lda_pred))
            # mlp_crossvalacc.append(accuracy_score(test, mlp_pred))
            knn_crossvalacc.append(accuracy_score(test, knn_pred))
            # crossvalacc.append(accuracy_score(test, pred))
            # lda_f1 = f1_score(y_test, lda_y_pred, average=None)
            # mlp_f1 = f1_score(y_test, mlp_y_pred, average=None)
            knn_f1 = f1_score(y_test, knn_y_pred, average=None)
            # f1 = f1_score(y_test, y_pred, average=None)
            # lda_thumbf1score.append(lda_f1[0])
            # lda_indexf1score.append(lda_f1[1])
            # lda_middlef1score.append(lda_f1[2])
            # lda_ringf1score.append(lda_f1[3])
            # lda_pinkyf1score.append(lda_f1[4])
            # mlp_thumbf1score.append(mlp_f1[0])
            # mlp_indexf1score.append(mlp_f1[1])
            # mlp_middlef1score.append(mlp_f1[2])
            # mlp_ringf1score.append(mlp_f1[3])
            # mlp_pinkyf1score.append(mlp_f1[4])
            knn_thumbf1score.append(knn_f1[0])
            knn_indexf1score.append(knn_f1[1])
            knn_middlef1score.append(knn_f1[2])
            knn_ringf1score.append(knn_f1[3])
            knn_pinkyf1score.append(knn_f1[4])
            # thumbf1score.append(f1[0])
            # indexf1score.append(f1[1])
            # middlef1score.append(f1[2])
            # ringf1score.append(f1[3])
            # pinkyf1score.append(f1[4])
            # print('\nTrial', trial)
            # print('Train samples:', len(X_train_new) // timeGroupSize, 'Test samples:', len(X_test_new) // timeGroupSize)
            # print('Features:', len(samples.X_train[0]), '->', len(X_train_new[0]))
            # print(trial_cnf)
            # print(accuracy_score(test, pred))

        print('Classifying:', subjectPath, 'PRF:', PRF, 'frameAvg:', frameAvg, 'timeGroupSize:', timeGroupSize)
        print('levels:', levels)
        print('Trials:', Trials)
        print('Depth range:', depthRange[0], '~', depthRange[1])
        print('Complete Confusion matrix')
        print('# samples:', len(samples.X) // timeGroupSize)
        print('# features:', len(samples.X[0]))

        # print('LDA')
        # cnf_matrix = confusion_matrix(y_test, lda_y_pred)
        # print(cnf_matrix)
        # print('Accuracy:  {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_crossvalacc) * 100, np.std(lda_crossvalacc) * 100))
        # print('Thumb  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_thumbf1score) * 100, np.std(lda_thumbf1score) * 100))
        # print('Index  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_indexf1score) * 100, np.std(lda_indexf1score) * 100))
        # print('Middle f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_middlef1score) * 100, np.std(lda_middlef1score) * 100))
        # print('Ring   f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_ringf1score) * 100, np.std(lda_ringf1score) * 100))
        # print('Pinky  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(lda_pinkyf1score) * 100, np.std(lda_pinkyf1score) * 100))

        # print('MLP')
        # cnf_matrix = confusion_matrix(y_test, mlp_y_pred)
        # print(cnf_matrix)
        # print('Accuracy:  {0:.2f} +/- {1:.2f} %'.format(np.mean(mlp_crossvalacc) * 100, np.std(mlp_crossvalacc) * 100))
        # print('Thumb  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(mlp_thumbf1score) * 100, np.std(mlp_thumbf1score) * 100))
        # print('Index  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(mlp_indexf1score) * 100, np.std(mlp_indexf1score) * 100))
        # print('Middle f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(mlp_middlef1score) * 100, np.std(mlp_middlef1score) * 100))
        # print('Ring   f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(mlp_ringf1score) * 100, np.std(mlp_ringf1score) * 100))
        # print('Pinky  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(mlp_pinkyf1score) * 100, np.std(mlp_pinkyf1score) * 100))

        print('KNN')
        cnf_matrix = confusion_matrix(y_test, knn_y_pred)
        print(cnf_matrix)
        print('Accuracy:  {0:.2f} +/- {1:.2f} %'.format(np.mean(knn_crossvalacc) * 100, np.std(knn_crossvalacc) * 100))
        print('Thumb  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(knn_thumbf1score) * 100, np.std(knn_thumbf1score) * 100))
        print('Index  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(knn_indexf1score) * 100, np.std(knn_indexf1score) * 100))
        print('Middle f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(knn_middlef1score) * 100, np.std(knn_middlef1score) * 100))
        print('Ring   f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(knn_ringf1score) * 100, np.std(knn_ringf1score) * 100))
        print('Pinky  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(knn_pinkyf1score) * 100, np.std(knn_pinkyf1score) * 100))

        # print('Soft Voting')
        # cnf_matrix = confusion_matrix(y_test, y_pred)
        # print(cnf_matrix)
        # print('Accuracy:  {0:.2f} +/- {1:.2f} %'.format(np.mean(crossvalacc) * 100, np.std(crossvalacc) * 100))
        # print('Thumb  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(thumbf1score) * 100, np.std(thumbf1score) * 100))
        # print('Index  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(indexf1score) * 100, np.std(indexf1score) * 100))
        # print('Middle f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(middlef1score) * 100, np.std(middlef1score) * 100))
        # print('Ring   f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(ringf1score) * 100, np.std(ringf1score) * 100))
        # print('Pinky  f1: {0:.2f} +/- {1:.2f} %'.format(np.mean(pinkyf1score) * 100, np.std(pinkyf1score) * 100))

        print_elapsed_time(startTime)

        # plt.show()

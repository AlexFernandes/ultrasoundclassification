from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.feature_selection import SelectFwe, f_classif
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.preprocessing import StandardScaler

from python.src.zrfManager import ZRFData
import python.src.patternClassification as pc
import numpy as np
import time
import warnings
import xlsxwriter


def write_linfit_worksheet(worksheet, times, segs):
    for t in range(len(times)):
        worksheet.write(segs, t * 2, times[t][0])
        worksheet.write(segs, t * 2 + 1, times[t][1])


def write_dwtmav_worksheet(worksheet, times, mavlength):
    for t in range(len(times)):
        worksheet.write(mavlength, t * 2, times[t][0])
        worksheet.write(mavlength, t * 2 + 1, times[t][1])


def print_elapsed_time(startTime):
    elapsed_time = time.time()
    hours, rem = divmod(elapsed_time - startTime, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Elapsed Time: {:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))


if __name__ == "__main__":
    warnings.filterwarnings("ignore")

    # Time Start
    startTime = time.time()

    dataDir = '/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/' \
              'ultrasoundclassification/data/'
    # dataDir = "D:/Code/ultrasoundclassification/data/"

    numTrials = 3
    frameStart = 0
    frameEnd = 180
    frameGroupSize = 3
    numFrames = frameEnd - frameStart
    levels = [1, 2, 3]
    wavlet = 'db3'
    windowSize = 2  # dsize as in window size along the depth axis

    fingerNames = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    fingerLabel = {'Thumb': 0, 'Index': 1, 'Middle': 2, 'Ring': 3, 'Pinky': 4}

    # numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 40, 50, 60, 70, 80, 90,
    #                100, 110, 120, 127]
    # numZColumns = [4, 6, 8]
    numZColumns = [1, 1, 2, 3, 4, 6, 8, 12, 16, 25, 40, 60, 127]
    # numZColumns = [6]

    zrfDir = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/zrfFiles/"
    # zrfDir = "D:/Code/zrfFiles/"

    subjectPaths = ["AlexFingerMovement/FingerMovement/",
                    "AlexRightFingerMovement/RightFingerMotions/",
                    "fm_sub1_l/FingerMotions/",
                    "fm_sub1_r/FingerMotions/",
                    "fm_sub2_l/FingerMotions/",
                    "fm_sub2_r/FingerMotions/",
                    "fm_sub3_l/FingerMotions/",
                    "fm_sub3_r/FingerMotions/",
                    "fm_sub4_l/FingerMotions/",
                    "fm_sub4_r/FingerMotions/"]
    # subjectPaths = ["AlexFingerMovement/FingerMovement/"]

    #########################################################################
    """Computation time Spatial Features (ms) vs reconstructed columns (N)"""
    #########################################################################
    # for zcols in numZColumns:
    #     print('# cols', zcols)
    #     linfit25_times = []
    #     linfit50_times = []
    #     linfit100_times = []
    #     wav1_times = []
    #     wav2_times = []
    #     wav3_times = []
    #     wav4_times = []
    #     wav5_times = []
    #
    #     for subjectPath in subjectPaths:
    #         filePath = zrfDir + subjectPath
    #         # print(subjectPath)
    #
    #         for fingerID in range(len(fingerNames)):
    #             for trial in range(1, numTrials + 1):
    #                 # print(fingerNames[fingerID], trial, end=' ')
    #                 # print_elapsed_time(startTime)
    #                 # load ultrasound .zrf file
    #                 zrf = ZRFData(filePath + fingerNames[fingerID] + str(trial) + '.zrf')
    #
    #                 # extract spatial features
    #                 for frame in range(frameStart, frameEnd):
    #                     # Linear Fit
    #                     linfitStartTime = time.time()
    #                     pc.exctract_linear_fit_segments(zrf.rfsig[:, :, frame],  # rf signal
    #                                                     zcols,  # num z columns to average to
    #                                                     25)
    #                     elapsed_time = time.time()
    #                     linfit25_times.append(elapsed_time-linfitStartTime)
    #                     linfitStartTime = time.time()
    #                     pc.exctract_linear_fit_segments(zrf.rfsig[:, :, frame],  # rf signal
    #                                                     zcols,  # num z columns to average to
    #                                                     50)
    #                     elapsed_time = time.time()
    #                     linfit50_times.append(elapsed_time-linfitStartTime)
    #                     linfitStartTime = time.time()
    #                     pc.exctract_linear_fit_segments(zrf.rfsig[:, :, frame],  # rf signal
    #                                                     zcols,  # num z columns to average to
    #                                                     100)
    #                     elapsed_time = time.time()
    #                     linfit100_times.append(elapsed_time-linfitStartTime)
    #
    #                     # Wavelet
    #                     wavStartTime = time.time()
    #                     pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
    #                                                     zcols,  # num z columns to average to
    #                                                     wavlet,  # wavelet type
    #                                                     [1],  # levels
    #                                                     windowSize)
    #                     elapsed_time = time.time()
    #                     wav1_times.append(elapsed_time-wavStartTime)
    #
    #                     wavStartTime = time.time()
    #                     pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
    #                                                     zcols,  # num z columns to average to
    #                                                     wavlet,  # wavelet type
    #                                                     [1, 2],  # levels
    #                                                     windowSize)
    #                     elapsed_time = time.time()
    #                     wav2_times.append(elapsed_time-wavStartTime)
    #
    #                     wavStartTime = time.time()
    #                     pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
    #                                                     zcols,  # num z columns to average to
    #                                                     wavlet,  # wavelet type
    #                                                     [1, 2, 3],  # levels
    #                                                     windowSize)
    #                     elapsed_time = time.time()
    #                     wav3_times.append(elapsed_time-wavStartTime)
    #
    #                     wavStartTime = time.time()
    #                     pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
    #                                                     zcols,  # num z columns to average to
    #                                                     wavlet,  # wavelet type
    #                                                     [1, 2, 3, 4],  # levels
    #                                                     windowSize)
    #                     elapsed_time = time.time()
    #                     wav4_times.append(elapsed_time-wavStartTime)
    #
    #                     wavStartTime = time.time()
    #                     pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
    #                                                     zcols,  # num z columns to average to
    #                                                     wavlet,  # wavelet type
    #                                                     [1, 2, 3, 4, 5],  # levels
    #                                                     windowSize)
    #                     elapsed_time = time.time()
    #                     wav5_times.append(elapsed_time-wavStartTime)
    #
    #
    #     linfit25_times_mean = int(round(np.mean(linfit25_times) * 100000)) / 100
    #     linfit25_times_std = int(round(np.mean(np.std(linfit25_times)) * 100000)) / 100
    #     linfit50_times_mean = int(round(np.mean(np.mean(linfit50_times)) * 100000)) / 100
    #     linfit50_times_std = int(round(np.mean(np.std(linfit50_times)) * 100000)) / 100
    #     linfit100_times_mean = int(round(np.mean(np.mean(linfit100_times)) * 100000)) / 100
    #     linfit100_times_std = int(round(np.mean(np.std(linfit100_times)) * 100000)) / 100
    #     wav1_times_mean = int(round(np.mean(np.mean(wav1_times)) * 100000)) / 100
    #     wav1_times_std = int(round(np.mean(np.std(wav1_times)) * 100000)) / 100
    #     wav2_times_mean = int(round(np.mean(np.mean(wav2_times)) * 100000)) / 100
    #     wav2_times_std = int(round(np.mean(np.std(wav2_times)) * 100000)) / 100
    #     wav3_times_mean = int(round(np.mean(np.mean(wav3_times)) * 100000)) / 100
    #     wav3_times_std = int(round(np.mean(np.std(wav3_times)) * 100000)) / 100
    #     wav4_times_mean = int(round(np.mean(np.mean(wav4_times)) * 100000)) / 100
    #     wav4_times_std = int(round(np.mean(np.std(wav4_times)) * 100000)) / 100
    #     wav5_times_mean = int(round(np.mean(np.mean(wav5_times)) * 100000)) / 100
    #     wav5_times_std = int(round(np.mean(np.std(wav5_times)) * 100000)) / 100
    #     template = '{0:10s} {1:5.2f} +/- {2:5.2f}'
    #     print(template.format('linfit25', linfit25_times_mean, linfit25_times_std))
    #     print(template.format('linfit50', linfit50_times_mean, linfit50_times_std))
    #     print(template.format('linfit100', linfit100_times_mean, linfit100_times_std))
    #     print(template.format('lvl1', wav1_times_mean, wav1_times_std))
    #     print(template.format('lvl2', wav2_times_mean, wav2_times_std))
    #     print(template.format('lvl3', wav3_times_mean, wav3_times_std))
    #     print(template.format('lvl4', wav4_times_mean, wav4_times_std))
    #     print(template.format('lvl5', wav5_times_mean, wav5_times_std))
    #     print_elapsed_time(startTime)

    #########################################################################
    """Computation time full pipeline (ms) vs reconstructed columns (N)"""
    #########################################################################
    workbook_name = 'dwtmav_len_16_computation_time.xlsx'
    workbook = xlsxwriter.Workbook(dataDir + 'accuracy/' + workbook_name)

    # segments = [50]
    # for s in range(50, 51, 10):
    #     segments.append(s)
    mavlength = [16]
    # for s in range(50, 2, -1):
    #     mavlength.append(s)

    for zcols in numZColumns:
        print('##################################################################################################')
        # print('segments:', segments)
        print('mavlength:', mavlength)
        try:
            worksheet = workbook.add_worksheet('zcols' + str(zcols))
        except xlsxwriter.exceptions.DuplicateWorksheetName:
            pass
        for l in mavlength:
        # for segs in segments:
        #     print('Linear Regression: segments', segs)
            print('DWT-MAV mav length:', l)
            print('# cols', zcols)
            # every frameGroupSize frames
            spatial_times = []
            temporal_times = []
            classify_times = []

            for subjectPath in subjectPaths:
                filePath = zrfDir + subjectPath
                # print(subjectPath)

                try:
                    samples = pc.DataContainer()  # container to put extracted features and samples to then classify
                    for fingerID in range(len(fingerNames)):
                        for trial in range(1, numTrials + 1):
                            # print(fingerNames[fingerID], trial, end=' ')
                            # print_elapsed_time(startTime)
                            # load ultrasound .zrf file
                            zrf = ZRFData(filePath + fingerNames[fingerID] + str(trial) + '.zrf')

                            # extract spatial features
                            spatialFeatures = []
                            f = 0
                            for frame in range(numFrames):
                                if f == 0:
                                    spatialStartTime = time.time()
                                # # Linear Regression Spatial Features
                                # spatialFeatures.append(
                                #     pc.exctract_linear_fit_segments(zrf.rfsig[:, :, frame],  # rf signal
                                #                                     zcols,  # num z columns to average to
                                #                                     segs))  # number of depth segments
                                # Wavelet Spatial Features
                                spatialFeatures.append(pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
                                                                                       zcols,  # num z columns to average to
                                                                                       wavlet,  # wavelet type
                                                                                       [1, 2, 3],  # levels
                                                                                       l))  # MAV length
                                if f < frameGroupSize - 1:
                                    f += 1
                                else:
                                    f = 0
                                    elapsed_time = time.time()
                                    spatial_times.append(elapsed_time - spatialStartTime)

                            # obtain difference between frames as features
                            temporalFeatures = []  # same as linearFitFeatures
                            f = 0
                            for frame in range(numFrames):
                                frameFeatures = []
                                if f == 0:
                                    temporalStartTime = time.time()
                                for feature in range(len(spatialFeatures[frame])):
                                    frameFeatures.append(
                                        spatialFeatures[frame][feature] - spatialFeatures[frame - 1][feature])
                                if f < frameGroupSize - 1:
                                    f += 1
                                else:
                                    f = 0
                                    elapsed_time = time.time()
                                    temporal_times.append(elapsed_time - temporalStartTime)
                                temporalFeatures.append(frameFeatures)

                            # group frames into group size and obtain min/max values and add the samples to data set
                            for frame in range(0, numFrames, frameGroupSize):
                                frameGroupFeatures = []
                                for feature in range(len(spatialFeatures[frame])):
                                    frameGroupFeatures.append(
                                        np.min([spatialFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                                    frameGroupFeatures.append(
                                        np.max([spatialFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                                    frameGroupFeatures.append(
                                        np.min([temporalFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                                    frameGroupFeatures.append(
                                        np.max([temporalFeatures[frame + f][feature] for f in range(frameGroupSize)]))

                                # each frame group is represented as a sample
                                samples.add_sample(np.asarray(frameGroupFeatures), fingerID, trial)

                    'Pattern Classification'
                    # Normalize Data
                    samples.X = StandardScaler().fit_transform(samples.X)
                    if subjectPath == subjectPaths[0]:
                        print('samples.X.shape', samples.X.shape)

                    # Feature Selection
                    a = 0.05
                    selector = SelectFwe(f_classif, alpha=a)

                    # Classifier
                    lda = LinearDiscriminantAnalysis()

                    cnf_matrix = None
                    y_pred = []
                    y_test = []
                    crossvalacc = []

                    # classify with test samples represented as the current trial number
                    for trial in range(1, numTrials + 1):
                        samples.test_train_split_by_trial(trial)

                        # Remove features
                        X_train_new = selector.fit_transform(samples.X_train, samples.y_train)
                        X_test_new = selector.transform(samples.X_test)

                        lda.fit(X_train_new, samples.y_train)
                        for s in range(X_test_new.shape[0]):
                            test = X_test_new[s, :].reshape(1, -1)
                            classifyStartTime = time.time()
                            lda.predict(test)
                            elapsed_time = time.time()
                            classify_times.append(elapsed_time - classifyStartTime)
                except:
                    print('###############################################################')
                    # print(subjectPath, 'Segment', segs, 'failed')
                    print(subjectPath, 'MAV length', l, 'failed')
                    print('###############################################################')

            total_times = []
            for t in range(len(spatial_times)):
                total_times.append(spatial_times[t] + temporal_times[t] + classify_times[t])

            write_times = []
            for times, names in zip([spatial_times, temporal_times, classify_times, total_times],
                                    ['spatial', 'temporal', 'classify', 'total']):
                times_mean = int(round(np.mean(times) * 100000)) / 100
                times_std = int(round(np.mean(np.std(times)) * 100000)) / 100
                write_times.append((times_mean, times_std))
                template = '{0:10s} {1:5.2f} +/- {2:5.2f}'
                print(template.format(names, times_mean, times_std))
            write_linfit_worksheet(worksheet,
                                   [('spatial', '+/-'), ('temporal', '+/-'), ('classify', '+/-'), ('total', '+/-')], 0)
            # write_linfit_worksheet(worksheet, write_times, segs)
            write_dwtmav_worksheet(worksheet, write_times, l)
            print_elapsed_time(startTime)
    workbook.close()

    # for zcols in numZColumns:
    # zcols = 6
    ########################################################
    """Computation time (ms) vs DWT parameters"""
    ########################################################
    # print('# cols', zcols)
    # for windowSize in range(1, 101):
    #
    #     lvl_times = []
    #     for subjectPath in subjectPaths:
    #         filePath = zrfDir + subjectPath
    #         # print(subjectPath)
    #
    #         for fingerID in range(len(fingerNames)):
    #             for trial in range(1, numTrials + 1):
    #                 # print(fingerNames[fingerID], trial, end=' ')
    #                 # print_elapsed_time(startTime)
    #                 # load ultrasound .zrf file
    #                 zrf = ZRFData(filePath + fingerNames[fingerID] + str(trial) + '.zrf')
    #
    #                 # extract spatial features
    #                 for frame in range(frameStart, frameEnd):
    #                     lvlStartTime = time.time()
    #                     pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
    #                                                     zcols,  # num z columns to average to
    #                                                     wavlet,  # wavelet type
    #                                                     levels,  # levels
    #                                                     windowSize)
    #                     elapsed_time = time.time()
    #                     lvl_times.append(elapsed_time - lvlStartTime)
    #
    #     lvl_times_mean = int(round(np.mean(lvl_times) * 100000)) / 100
    #     lvl_times_std = int(round(np.mean(np.std(lvl_times)) * 100000)) / 100
    #     template = 'MAV size {0:3.0f} {1:5.2f} +/- {2:5.2f}'
    #     print(template.format(windowSize, lvl_times_mean, lvl_times_std))

    ########################################################
    """Computation time (ms) vs LR parameters"""
    ########################################################
    # print('# cols', zcols)
    # for segs in [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100]:
    #     linfit_times = []
    #     for subjectPath in subjectPaths:
    #         filePath = zrfDir + subjectPath
    #         # print(subjectPath)
    #
    #         for fingerID in range(len(fingerNames)):
    #             for trial in range(1, numTrials + 1):
    #                 # print(fingerNames[fingerID], trial, end=' ')
    #                 # print_elapsed_time(startTime)
    #                 # load ultrasound .zrf file
    #                 zrf = ZRFData(filePath + fingerNames[fingerID] + str(trial) + '.zrf')
    #
    #                 # extract spatial features
    #                 for frame in range(frameStart, frameEnd):
    #                     linfitStartTime = time.time()
    #                     pc.exctract_linear_fit_segments(zrf.rfsig[:, :, frame],  # rf signal
    #                                                     zcols,  # num z columns to average to
    #                                                     segs)
    #                     elapsed_time = time.time()
    #                     linfit_times.append(elapsed_time - linfitStartTime)
    #
    #     linfit_times_mean = int(round(np.mean(linfit_times) * 100000)) / 100
    #     linfit_times_std = int(round(np.mean(np.std(linfit_times)) * 100000)) / 100
    #     template = 'segs {0:3.0f} {1:5.2f} +/- {2:5.2f}'
    #     print(template.format(segs, linfit_times_mean, linfit_times_std))
    # print_elapsed_time(startTime)

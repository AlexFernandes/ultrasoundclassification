import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats


if __name__ == '__main__':

    linfit_data = np.asarray([0.6633333333333333, 0.78, 0.8133333333333334,
                              0.6266666666666667, 0.79, 0.52,
                              0.65, 0.8433333333333334, 0.6733333333333333,
                              0.6066666666666667, 0.9133333333333333, 0.8966666666666666,
                              0.9666666666666667, 0.97, 0.8866666666666667,
                              0.7733333333333333, 0.9966666666666667, 0.8766666666666667,
                              0.5833333333333334, 0.9466666666666667, 0.7933333333333333,
                              0.83, 0.87, 0.9466666666666667,
                              0.7533333333333333, 0.92, 0.82,
                              0.6433333333333333, 0.9533333333333334, 0.64])
    wavdb3_data = np.asarray([0.7466666666666667, 0.7666666666666667, 0.8133333333333334,
                              0.7133333333333334, 0.8533333333333334, 0.5933333333333334,
                              0.75, 0.8766666666666667, 0.7166666666666667,
                              0.8433333333333334, 0.9733333333333334, 0.8933333333333333,
                              0.9966666666666667, 0.9833333333333333, 0.8,
                              0.8866666666666667, 1.0, 0.74,
                              0.8333333333333334, 0.9966666666666667, 0.8333333333333334,
                              0.9266666666666666, 0.98, 0.88,
                              0.8466666666666667, 0.99, 0.8566666666666667,
                              0.8866666666666667, 1.0, 0.5933333333333334])

    # N = 6
    linfit_fwe_data = np.asarray([[0.6933333333333334, 0.77, 0.8333333333333334],
                                  [0.6633333333333333, 0.7466666666666667, 0.52],
                                  [0.6433333333333333, 0.9, 0.6133333333333333],
                                  [0.7333333333333333, 0.9633333333333334, 0.8733333333333333],
                                  [0.9666666666666667, 0.9933333333333333, 0.9133333333333333],
                                  [0.8433333333333334, 1.0, 0.91],
                                  [0.6166666666666667, 0.9533333333333334, 0.8266666666666667],
                                  [0.8366666666666667, 0.91, 0.97],
                                  [0.89, 0.9366666666666666, 0.8466666666666667],
                                  [0.7433333333333333, 0.94, 0.7066666666666667]])
    wavdb3_fwe_data = np.asarray([[0.76, 0.84, 0.81],
                                  [0.7, 0.8133333333333334, 0.5233333333333333],
                                  [0.7366666666666667, 0.8966666666666666, 0.7033333333333334],
                                  [0.8133333333333334, 0.9766666666666667, 0.8933333333333333],
                                  [0.9966666666666667, 0.9833333333333333, 0.7766666666666666],
                                  [0.87, 1.0, 0.8233333333333334],
                                  [0.8633333333333333, 0.9866666666666667, 0.8266666666666667],
                                  [0.89, 0.9533333333333334, 0.86],
                                  [0.8566666666666667, 0.9833333333333333, 0.91],
                                  [0.91, 0.99, 0.6233333333333333]])

    nowav_fwe_data = np.asarray([[0.9533333333333334, 0.72, 0.7233333333333334],
                                 [0.6066666666666667, 0.7, 0.46],
                                 [0.5833333333333334, 0.76, 0.6233333333333333],
                                 [0.6733333333333333, 0.8766666666666667, 0.81],
                                 [0.9533333333333334, 0.9833333333333333, 0.79],
                                 [0.6766666666666666, 0.9366666666666666, 0.7766666666666666],
                                 [0.7666666666666667, 0.8366666666666667, 0.63],
                                 [0.84, 0.94, 0.8866666666666667],
                                 [0.7233333333333334, 0.83, 0.87],
                                 [0.5633333333333334, 0.9066666666666666, 0.54]])

    f, p = stats.f_oneway(linfit_data, wavdb3_data)
    f_fwe, p_fwe = stats.f_oneway([linfit_fwe_data[i, j] for i in range(10) for j in range(3)],
                                  [wavdb3_fwe_data[i, j] for i in range(10) for j in range(3)])
    f_wav, p_wav = stats.f_oneway([nowav_fwe_data[i, j] for i in range(10) for j in range(3)],
                                  [wavdb3_fwe_data[i, j] for i in range(10) for j in range(3)])
    print('One way ANOVA p =', p)
    print('fwe: One way ANOVA p =', p_fwe)
    print('wav: One way ANOVA p =', p_wav)

    # Linear (slope, intercept) fit
    linfit_mean = np.asarray([75.22222222222223,
                              64.55555555555556,
                              72.22222222222223,
                              80.55555555555554,
                              94.11111111111111,
                              88.22222222222221,
                              77.44444444444444,
                              88.22222222222221,
                              83.11111111111111,
                              74.55555555555556])
    linfit_std = np.asarray([6.431020501550126,
                             11.103330609203887,
                             8.616664875707244,
                             14.08001823792309,
                             3.852207960746601,
                             9.126004088169367,
                             14.893034248795725,
                             4.840671313509326,
                             6.849348892187755,
                             14.692737771085296])

    linfit_fwe_mean = np.mean(linfit_fwe_data, axis=1)
    linfit_fwe_std = np.std(linfit_fwe_data, axis=1)

    # Daubechies3 wavelet into mean(abs())
    wavdb3_mean = np.asarray([77.55555555555556,
                              72.00000000000001,
                              78.11111111111111,
                              90.33333333333333,
                              92.66666666666667,
                              87.55555555555557,
                              88.77777777777779,
                              92.88888888888889,
                              89.77777777777777,
                              82.66666666666667])
    wavdb3_std = np.asarray([2.793290019994785,
                             10.624918300339486,
                             6.892472186342589,
                             5.354126134736337,
                             8.973211159808873,
                             10.64349334651382,
                             7.699607172920183,
                             4.0855058468556065,
                             6.533862412439624,
                             17.135624576584004])

    wavdb3_fwe_mean = np.mean(wavdb3_fwe_data, axis=1)
    wavdb3_fwe_std = np.std(wavdb3_fwe_data, axis=1)

    nowav_fwe_mean = np.mean(nowav_fwe_data, axis=1)
    nowav_fwe_std = np.std(nowav_fwe_data, axis=1)

    # Create lists for the plot
    subjects = ['sub1 l arm',
                'sub1 r arm',
                'sub2 l arm',
                'sub2 r arm',
                'sub3 l arm',
                'sub3 r arm',
                'sub4 l arm',
                'sub4 r arm',
                'sub5 l arm',
                'sub5 r arm']
    x_pos = np.arange(len(subjects)) * 4
    x_pos_all = np.arange(len(subjects)) * 2
    linfit_mean = linfit_mean / 100
    linfit_fwe_mean = linfit_fwe_mean
    wavdb3_mean = wavdb3_mean / 100
    wavdb3_fwe_mean = wavdb3_fwe_mean
    # For only upper error bar
    # linfit_std = np.asarray([np.zeros(linfit_std.shape[0]), linfit_std / 100])
    # wavdb3_std = np.asarray([np.zeros(wavdb3_std.shape[0]), wavdb3_std / 100])
    # For both error bars
    linfit_std /= 100
    wavdb3_std /= 100

    # Build the plot
    fig, ax = plt.subplots()
    # ax.bar(x_pos-1, linfit_mean, yerr=linfit_std, align='center', alpha=1, ecolor='black', capsize=10)
    # ax.bar(x_pos-0.4, linfit_fwe_mean, yerr=linfit_fwe_std, align='center', alpha=1, ecolor='black', capsize=10)
    # ax.bar(x_pos+0.4, wavdb3_mean, yerr=wavdb3_std, align='center', alpha=1, ecolor='black', capsize=10)
    ax.bar(x_pos - 0.4, nowav_fwe_mean, yerr=nowav_fwe_std, align='center', alpha=1, ecolor='black', capsize=10)
    ax.bar(x_pos + 0.4, wavdb3_fwe_mean, yerr=wavdb3_fwe_std, align='center', alpha=1, ecolor='black', capsize=10)

    ax.set_ylabel('3-Fold Cross Validation Accuracy')
    ax.set_xticks(x_pos)
    ax.set_xticklabels(subjects)
    ax.set_title('3-Fold Cross Validation Classification Accuracy of each subject\nFor N = 6 columns resolution')
    plt.ylim([0, 1])
    ax.yaxis.grid(True)
    ax.legend(['No Wavelet', 'Daubechies 3 Wavelet'])

    plt.show()

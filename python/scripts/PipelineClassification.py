"""Classify finger motions using extraction methods and implementing meta learning methods

"""
import xlsxwriter
from python.src.zrfManager import ZRFData
import python.src.patternClassification as pc
import numpy as np
import time
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import confusion_matrix, accuracy_score


def print_elapsed_time(startTime):
    elapsed_time = time.time()
    hours, rem = divmod(elapsed_time - startTime, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Elapsed Time: {:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))


def write_cnf_worksheet(worksheet, cnf_matrix, zcols, acc, row, col):
    worksheet.write(row, col, 'zcols:')
    worksheet.write(row, col + 1, zcols)
    worksheet.write(row, col + 2, 'acc:')
    worksheet.write(row, col + 3, acc)
    for thumb, index, middle, ring, pinky in cnf_matrix:
        worksheet.write(row + 1, col, thumb)
        worksheet.write(row + 1, col + 1, index)
        worksheet.write(row + 1, col + 2, middle)
        worksheet.write(row + 1, col + 3, ring)
        worksheet.write(row + 1, col + 4, pinky)
        row += 1


if __name__ == "__main__":
    # Time Start
    startTime = time.time()

    numTrials = 3
    frameStart = 0
    frameEnd = 180
    frameGroupSize = 3
    numFrames = frameEnd - frameStart
    waveletname = 'db3'
    levels = [2, 3, 4]
    numsegs = 50  # segments per wavelet

    dataDir = '/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/' \
              'ultrasoundclassification/data/'
    workbook = xlsxwriter.Workbook(dataDir + 'accuracy/wav-db3_lvl2to4_groupsize-3.xlsx')

    fingerNames = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    fingerLabel = {'Thumb': 0, 'Index': 1, 'Middle': 2, 'Ring': 3, 'Pinky': 4}

    numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    zrfDir = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/zrfFiles/"

    subjectPaths = ["AlexFingerMovement/FingerMovement/",
                    "AlexRightFingerMovement/RightFingerMotions/",
                    "fm_sub1_l/FingerMotions/",
                    "fm_sub1_r/FingerMotions/",
                    "fm_sub2_l/FingerMotions/",
                    "fm_sub2_r/FingerMotions/",
                    "fm_sub3_l/FingerMotions/",
                    "fm_sub3_r/FingerMotions/",
                    "fm_sub4_l/FingerMotions/",
                    "fm_sub4_r/FingerMotions/"]

    for subjectPath in subjectPaths:
        filePath = zrfDir + subjectPath

        print(subjectPath)
        print('numZcolumns', numZColumns)
        print('numTrials', numTrials)
        print('numFrames', numFrames)
        print('frameGroupSize', frameGroupSize)

        worksheet = workbook.add_worksheet(subjectPath.split('/')[0])
        for zcols in numZColumns:

            samples = pc.DataContainer()  # container to put extracted features and samples to then classify

            for fingerID in range(len(fingerNames)):
                for trial in range(1, numTrials + 1):
                    # print(fingerNames[fingerID], trial, end=' ')
                    # print_elapsed_time(startTime)
                    # load ultrasound .zrf file
                    zrf = ZRFData(filePath + fingerNames[fingerID] + str(trial) + '.zrf')

                    # extract wavelet coefficient features for each frame of the rf data
                    extractedFeatures = []  # list of a list: [numFrames][zcols*N]; N is based on the level of
                    # wavelet scales
                    for frame in range(frameStart, frameEnd):
                        # extract_linear_fit_segments returns a list of features extracted for the given frame
                        extractedFeatures.append(pc.extract_wavelet_coefficients(zrf.rfsig[:, :, frame],  # rf signal
                                                                                 zcols,  # num z columns to average to
                                                                                 waveletname,  # wavelet
                                                                                 levels))  # wavelet levels

                    # obtain difference between frames as features
                    differenceFeatures = []  # same as linearFitFeatures
                    for frame in range(numFrames):
                        frameFeatures = []
                        for feature in range(len(extractedFeatures[frame])):
                            frameFeatures.append(
                                extractedFeatures[frame][feature] - extractedFeatures[frame - 1][feature])
                        differenceFeatures.append(frameFeatures)

                    # group frames into group size and obtain min/max values and add the samples to data set
                    for frame in range(0, numFrames, frameGroupSize):
                        frameFeatures = []
                        for feature in range(len(extractedFeatures[frame])):
                            frameFeatures.append(
                                np.min([extractedFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.max([extractedFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.min([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.max([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))

                        # each frame is represented as a sample
                        samples.add_sample(np.asarray(frameFeatures), fingerID, trial)

            # Split into test / training data
            lda = LinearDiscriminantAnalysis()
            cnf_matrix = None
            y_pred = []
            y_test = []

            # classify with test samples represented as the current trial number
            for trial in range(1, numTrials + 1):
                samples.test_train_split_by_trial(trial)

                pred = lda.fit(samples.X_train, samples.y_train).predict(samples.X_test)

                for s in range(len(pred)):
                    y_pred.append(pred[s])
                    y_test.append(samples.y_test[s])

            print('# zcols:', zcols)
            print('# samples:', len(samples.X))
            print('# features:', len(samples.X[0]))
            cnf_matrix = confusion_matrix(y_test, y_pred)
            print(cnf_matrix)
            print('Accuracy:', accuracy_score(y_test, y_pred))
            print_elapsed_time(startTime)

            write_cnf_worksheet(worksheet, cnf_matrix, zcols, accuracy_score(y_test, y_pred), (zcols - 1) * 6, 0)
    workbook.close()

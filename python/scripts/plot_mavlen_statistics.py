from xlrd import open_workbook
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy import stats
from python.src.zrfPlot import plot_confusion_matrix

def loadScores(excelFile, mavlengths):
    wb = open_workbook(excelFile)
    f1scores = np.zeros([len(mavlengths), wb.nsheets - 1, 5])
    classacc = np.zeros([len(mavlengths), wb.nsheets - 1])
    s = -1
    for sheet in wb.sheets():
        if sheet.name != 'Statistics':
            s = s + 1
            z = -1
            for mavlen in mavlengths:
                z += 1
                cnf_matrix = np.zeros([5, 5], np.uint32)
                for row in range(5):
                    for col in range(5):
                        cnf_matrix[row][col] = sheet.cell((mavlen - 1) * 6 + row + 1, col).value
                acc = 0
                for i in range(5):
                    for j in range(5):
                        if i == j:
                            acc = acc + cnf_matrix[i][j]
                acc = acc / np.sum(cnf_matrix)
                f1 = np.zeros([5], np.double)
                for f in range(5):
                    p = cnf_matrix[f][f] / np.sum(cnf_matrix, 0, np.uint32)[f]
                    r = cnf_matrix[f][f] / np.sum(cnf_matrix, 1, np.uint32)[f]
                    f1[f] = 2 * p * r / (p + r)
                    f1scores[z, s, f] = f1[f]
                classacc[z, s] = acc
    return f1scores, classacc


def loadSumCnf(excelFile, mavlengths):
    wb = open_workbook(excelFile)
    cnf_matricies = np.zeros([len(mavlengths), 5, 5], np.uint32)
    for sheet in wb.sheets():
        if sheet.name != 'Statistics':
            z = -1
            for mavlen in mavlengths:
                z += 1
                for row in range(5):
                    for col in range(5):
                        cnf_matricies[z, row, col] += sheet.cell((mavlen - 1) * 6 + row + 1, col).value
    return cnf_matricies


if __name__ == "__main__":
    ###########
    """Setup"""
    ###########

    # dataDir = 'D:/Code/ultrasoundclassification/data/accuracy/'
    dataDir = '/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/ultrasoundclassification/' \
              'data/accuracy/'

    mavlengths = []
    for s in range(50, 2, -1):
        mavlengths.append(s)
    mavlengths = np.asarray(mavlengths)


    zcol1_file = 'dwtmav_lvl1to3_N1.xlsx'
    zcol1_f1, zcol1_ca = loadScores(dataDir + zcol1_file, mavlengths)
    zcol1_cnf_matricies = loadSumCnf(dataDir + zcol1_file, mavlengths)

    zcol2_file = 'dwtmav_lvl1to3_N2.xlsx'
    zcol2_f1, zcol2_ca = loadScores(dataDir + zcol2_file, mavlengths)
    zcol2_cnf_matricies = loadSumCnf(dataDir + zcol2_file, mavlengths)

    zcol3_file = 'dwtmav_lvl1to3_N3.xlsx'
    zcol3_f1, zcol3_ca = loadScores(dataDir + zcol3_file, mavlengths)
    zcol3_cnf_matricies = loadSumCnf(dataDir + zcol3_file, mavlengths)

    zcol4_file = 'dwtmav_lvl1to3_N4.xlsx'
    zcol4_f1, zcol4_ca = loadScores(dataDir + zcol4_file, mavlengths)
    zcol4_cnf_matricies = loadSumCnf(dataDir + zcol4_file, mavlengths)

    zcol6_file = 'dwtmav_lvl1to3_N6.xlsx'
    zcol6_f1, zcol6_ca = loadScores(dataDir + zcol6_file, mavlengths)
    zcol6_cnf_matricies = loadSumCnf(dataDir + zcol6_file, mavlengths)

    zcol8_file = 'dwtmav_lvl1to3_N8.xlsx'
    zcol8_f1, zcol8_ca = loadScores(dataDir + zcol8_file, mavlengths)
    zcol8_cnf_matricies = loadSumCnf(dataDir + zcol8_file, mavlengths)

    zcol12_file = 'dwtmav_lvl1to3_N12.xlsx'
    zcol12_f1, zcol12_ca = loadScores(dataDir + zcol12_file, mavlengths)
    zcol12_cnf_matricies = loadSumCnf(dataDir + zcol12_file, mavlengths)

    zcol16_file = 'dwtmav_lvl1to3_N16.xlsx'
    zcol16_f1, zcol16_ca = loadScores(dataDir + zcol16_file, mavlengths)
    zcol16_cnf_matricies = loadSumCnf(dataDir + zcol16_file, mavlengths)

    zcol25_file = 'dwtmav_lvl1to3_N25.xlsx'
    zcol25_f1, zcol25_ca = loadScores(dataDir + zcol25_file, mavlengths)
    zcol25_cnf_matricies = loadSumCnf(dataDir + zcol25_file, mavlengths)

    # zcol40_file = 'dwtmav_lvl1to3_N40.xlsx'
    # zcol40_f1, zcol40_ca = loadScores(dataDir + zcol40_file, mavlengths)
    # zcol40_cnf_matricies = loadSumCnf(dataDir + zcol40_file, mavlengths)

    zcol60_file = 'dwtmav_lvl1to3_N60.xlsx'
    zcol60_f1, zcol60_ca = loadScores(dataDir + zcol60_file, mavlengths)
    zcol60_cnf_matricies = loadSumCnf(dataDir + zcol60_file, mavlengths)

    zcol127_file = 'dwtmav_lvl1to3_N127.xlsx'
    zcol127_f1, zcol127_ca = loadScores(dataDir + zcol127_file, mavlengths)
    zcol127_cnf_matricies = loadSumCnf(dataDir + zcol127_file, mavlengths)

    choose_zcol_ca = zcol1_ca
    choose_zcol_f1 = zcol1_f1
    choosemavlength = 34
    print('choose mav length', mavlengths[choosemavlength])

    numsubjects = 10
    N = np.asarray([1, 2, 3, 4, 6, 8, 12, 16, 25, 60, 127])

    font = {'family': 'normal',
            'weight': 'normal',
            'size': 16}
    matplotlib.rc('font', **font)

    dashed_line_weight = 0.5

    colour_fingers = ['blue', 'red', 'green', 'purple', 'black']
    finger_names = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']

    ######################
    """Print Statistics"""
    ######################
    i = 0
    for N_ca in [zcol1_ca, zcol2_ca, zcol3_ca, zcol4_ca, zcol6_ca, zcol8_ca, zcol12_ca, zcol16_ca,
                 zcol25_ca, zcol60_ca, zcol127_ca]:
        m_ca = np.median(N_ca[:, :], axis=1) * 100
        print('N =', N[i], 'Max CA', np.amax(m_ca), 'at mavlen', mavlengths[np.where(m_ca == np.amax(m_ca))],
              np.where(m_ca == np.amax(m_ca)))
        i += 1

    i = 0
    for N_ca in [zcol1_ca, zcol2_ca, zcol3_ca, zcol4_ca, zcol6_ca, zcol8_ca, zcol12_ca, zcol16_ca,
                 zcol25_ca, zcol60_ca, zcol127_ca]:
        m_ca = np.median(N_ca[:, :], axis=1) * 100
        print('N =', N[i], 'Min CA', np.amin(m_ca), 'at mavlen', mavlengths[np.where(m_ca == np.amin(m_ca))],
              np.where(m_ca == np.amin(m_ca)))
        i += 1

    i = 0
    for N_f1 in [zcol1_f1, zcol2_f1, zcol3_f1, zcol4_f1, zcol6_f1, zcol8_f1, zcol12_f1, zcol16_f1,
                 zcol25_f1, zcol60_f1, zcol127_f1]:
        print('N =', N[i])
        for finger in range(5):
            m_f1 = np.median(N_f1[:, :, finger], axis=1) * 100
            print('Max', finger_names[finger], 'F1', np.amax(m_f1), 'at mavlen', mavlengths[np.where(m_f1 == np.amax(m_f1))],
                  np.where(m_f1 == np.amax(m_f1)))
        i += 1

    ####################
    """Compare ENV-LR"""
    ####################

    lr_file = 'linfit_seg10_fwe-a5.xlsx'
    lr_f1, alt_ca = loadScores(dataDir + lr_file, N)


    for mavlen in range(len(mavlengths)):
        sumdiff = 0
        F1 = np.zeros([N.shape[0], numsubjects, 5])
        for sub in range(numsubjects):
            F1[0, sub, :] = zcol1_f1[mavlen, sub, :]
        for sub in range(numsubjects):
            F1[1, sub, :] = zcol2_f1[mavlen, sub, :]
        for sub in range(numsubjects):
            F1[2, sub, :] = zcol3_f1[mavlen, sub, :]
        for sub in range(numsubjects):
            F1[3, sub, :] = zcol4_f1[mavlen, sub, :]
        for sub in range(numsubjects):
            F1[4, sub, :] = zcol6_f1[mavlen, sub, :]
        for sub in range(numsubjects):
            F1[5, sub, :] = zcol8_f1[mavlen, sub, :]
        for sub in range(numsubjects):
            F1[6, sub, :] = zcol12_f1[mavlen, sub, :]
        for sub in range(numsubjects):
            F1[7, sub, :] = zcol16_f1[mavlen, sub, :]
        for sub in range(numsubjects):
            F1[8, sub, :] = zcol25_f1[mavlen, sub, :]
        for sub in range(numsubjects):
            F1[9, sub, :] = zcol60_f1[mavlen, sub, :]
        for sub in range(numsubjects):
            F1[10, sub, :] = zcol127_f1[mavlen, sub, :]
        for n in [3, 4, 5, 6, 7, 8, 9, 10]:
            for f in range(len(finger_names)):
                for sub in range(numsubjects):
                    sumdiff += F1[n, sub, f]*100 - lr_f1[n, sub, f]*100
        print(mavlengths[mavlen], '[', mavlen, ']', sumdiff)

    F1 = np.zeros([N.shape[0], numsubjects, 5])
    for sub in range(numsubjects):
        F1[0, sub, :] = zcol1_f1[choosemavlength, sub, :]
    for sub in range(numsubjects):
        F1[1, sub, :] = zcol2_f1[choosemavlength, sub, :]
    for sub in range(numsubjects):
        F1[2, sub, :] = zcol3_f1[choosemavlength, sub, :]
    for sub in range(numsubjects):
        F1[3, sub, :] = zcol4_f1[choosemavlength, sub, :]
    for sub in range(numsubjects):
        F1[4, sub, :] = zcol6_f1[choosemavlength, sub, :]
    for sub in range(numsubjects):
        F1[5, sub, :] = zcol8_f1[choosemavlength, sub, :]
    for sub in range(numsubjects):
        F1[6, sub, :] = zcol12_f1[choosemavlength, sub, :]
    for sub in range(numsubjects):
        F1[7, sub, :] = zcol16_f1[choosemavlength, sub, :]
    for sub in range(numsubjects):
        F1[8, sub, :] = zcol25_f1[choosemavlength, sub, :]
    for sub in range(numsubjects):
        F1[9, sub, :] = zcol60_f1[choosemavlength, sub, :]
    for sub in range(numsubjects):
        F1[10, sub, :] = zcol127_f1[choosemavlength, sub, :]

    templatetitle = '{0:10s} {1:5s} {2:1s} {3:5s}'
    template = '{0:10s} {1:5.2f} {2:1s} {3:5.2f} {4:5.2f}'
    for n in range(len(N)):
        print(templatetitle.format('N='+str(N[n]), 'DWT', ' ', 'LR'))
        for f in range(len(finger_names)):
            dwtf1 = np.median(F1[n, :, f]) * 100
            lrf1 = np.median(lr_f1[n, :, f]) * 100
            print(template.format(finger_names[f], dwtf1, '>' if dwtf1 > lrf1 else '<', lrf1, dwtf1-lrf1))

    #####################################################
    """Plot for Q1 Q2 Q3 F1 Scores for table in thesis"""
    #####################################################

    for n in range(len(N)):
        for f in range(5):
            print('{0:.1f}'.format(np.percentile(F1[n, :, f], 25) * 100), end=' – ')
            print('{0:.1f}'.format(np.percentile(F1[n, :, f], 75) * 100), end='\t')
        print()
        for f in range(5):
            print('{0:.1f}'.format(np.percentile(lr_f1[n, :, f], 25) * 100), end=' – ')
            print('{0:.1f}'.format(np.percentile(lr_f1[n, :, f], 75) * 100), end='\t')
        print()

    # Plot F1
    Finger = 4
    fig, axf1 = plt.subplots()
    axf1.plot(N, np.percentile(F1[:, :, Finger], 75, axis=1) * 100, 'b-^', label='DWT-MAV 75th percentile')
    axf1.plot(N, np.percentile(F1[:, :, Finger], 25, axis=1) * 100, 'b-v', label='DWT-MAV 25th percentile')
    axf1.plot(N, np.percentile(lr_f1[:, :, Finger], 75, axis=1) * 100, 'r-^', label='ENV-LR 75th percentile')
    axf1.plot(N, np.percentile(lr_f1[:, :, Finger], 25, axis=1) * 100, 'r-v', label='ENV-LR 25th percentile')
    axf1.set_xscale('log', basex=2)
    axf1.set_ylim(ymin=30, ymax=100)
    axf1.set_xticklabels(['1', '2', '3', '4', '6', '8', '12', '16', '25', '60', '127'])
    axf1.set_xticks([1, 2, 3, 4, 6, 8, 12, 16, 25, 60, 127])
    axf1.set_xlim(xmin=0.7, xmax=145)
    plt.grid()
    plt.legend(loc=6, bbox_to_anchor=(1.0, 0.5))


    #############################
    """Classification Accuracy"""
    #############################
    m_ca = np.median(choose_zcol_ca[:, :], axis=1) * 100
    print('Max CA', np.amax(m_ca), 'at mavlen', mavlengths[np.where(m_ca == np.amax(m_ca))], np.where(m_ca == np.amax(m_ca)))
    q75_ca = np.percentile(choose_zcol_ca, q=75, axis=1) * 100
    q25_ca = np.percentile(choose_zcol_ca, q=25, axis=1) * 100
    fig, axCA = plt.subplots()
    axCA.scatter(mavlengths, q75_ca, marker='^', c='b')
    axCA.scatter(mavlengths, m_ca, c='r')
    axCA.scatter(mavlengths, q25_ca, marker='v', c='g')
    p2, p1, p0 = np.polyfit(mavlengths, q75_ca, 2)
    axCA.plot(mavlengths, p2*mavlengths*mavlengths + p1*mavlengths + p0, 'b')
    p2, p1, p0 = np.polyfit(mavlengths, m_ca, 2)
    axCA.plot(mavlengths, p2*mavlengths*mavlengths + p1*mavlengths + p0, 'r')
    p2, p1, p0 = np.polyfit(mavlengths, q25_ca, 2)
    axCA.plot(mavlengths, p2*mavlengths*mavlengths + p1*mavlengths + p0, 'g')
    axCA.set_ylim(ymin=50, ymax=100)
    plt.xlabel('Number of segments')
    plt.ylabel('Classification Accuracy (%)')
    # plt.title('Classification acurracy with N=6 reconstructed RF signals')
    plt.legend(['upper quartile trend line',
                'median trendline',
                'lower quartile trendline',
                'upper quartile',
                'median',
                'lower quartile'], bbox_to_anchor=(1.0, 1.0))
    plt.grid()

    ##############################################
    """Classification Accuracy across N columns"""
    ##############################################
    CA = np.zeros([N.shape[0], numsubjects])
    for sub in range(numsubjects):
        CA[0, sub] = zcol1_ca[choosemavlength, sub]
    for sub in range(numsubjects):
        CA[1, sub] = zcol2_ca[choosemavlength, sub]
    for sub in range(numsubjects):
        CA[2, sub] = zcol3_ca[choosemavlength, sub]
    for sub in range(numsubjects):
        CA[3, sub] = zcol4_ca[choosemavlength, sub]
    for sub in range(numsubjects):
        CA[4, sub] = zcol6_ca[choosemavlength, sub]
    for sub in range(numsubjects):
        CA[5, sub] = zcol8_ca[choosemavlength, sub]
    for sub in range(numsubjects):
        CA[6, sub] = zcol12_ca[choosemavlength, sub]
    for sub in range(numsubjects):
        CA[7, sub] = zcol16_ca[choosemavlength, sub]
    for sub in range(numsubjects):
        CA[8, sub] = zcol25_ca[choosemavlength, sub]
    for sub in range(numsubjects):
        CA[9, sub] = zcol60_ca[choosemavlength, sub]
    for sub in range(numsubjects):
        CA[10, sub] = zcol127_ca[choosemavlength, sub]

    figLRbp, axLRbp = plt.subplots()
    CA_data = []
    CA_pos = []
    wid = []
    i = 0
    print('CA')
    print(CA)
    for n in N:
        if n in [1, 2, 3, 4, 6, 8, 12, 16, 25, 40, 60, 127]:
            print('CA median N =', n, np.median(CA[i, :] * 100))
            CA_data.append(CA[i, :] * 100)
            CA_pos.append(n)
            wid.append(n/8)
        i += 1
    box = axLRbp.boxplot(CA_data, positions=CA_pos, widths=wid)
    axLRbp.set_xscale('log', basex=2)
    axLRbp.set_ylim(ymin=30, ymax=100)
    axLRbp.set_xticklabels(['1', '2', '3', '4', '6', '8', '12', '16', '25', '60', '127'])
    axLRbp.set_xticks([1, 2, 3, 4, 6, 8, 12, 16, 25, 60, 127])
    axLRbp.set_xlim(xmin=0.7, xmax=145)
    # axLRbp.set_title('DWT-MAV Method, MAV length ' + str(mavlengths[choosemavlength]))
    # axLRbp.set_ylabel('Classification Accuracy (%)')
    # axLRbp.set_xlabel('Number of reconstructed RF signals (N)')
    plt.grid()


    #########################################
    """Per subject Classification Accuracy"""
    #########################################
    fig, axsub = plt.subplots()
    for sub in range(10):
        # axsub.scatter(numSegs, choose_zcol_ca[:, sub])
        p2, p1, p0 = np.polyfit(mavlengths, choose_zcol_ca[:, sub], 2)
        axsub.plot(mavlengths, p2*mavlengths*mavlengths + p1*mavlengths + p0)
    # axsub.set_ylim(ymin=0, ymax=1)
    axsub.legend([
                  # 'sub1 l arm',
                  # 'sub1 r arm',
                  # 'sub2 l arm',
                  # 'sub2 r arm',
                  # 'sub3 l arm',
                  # 'sub3 r arm',
                  # 'sub4 l arm',
                  # 'sub4 r arm',
                  # 'sub5 l arm',
                  # 'sub5 r arm',
                  'sub1 l trendline',
                  'sub1 r trendline',
                  'sub2 l trendline',
                  'sub2 r trendline',
                  'sub3 l trendline',
                  'sub3 r trendline',
                  'sub4 l trendline',
                  'sub4 r trendline',
                  'sub5 l trendline',
                  'sub5 r trendline'],
                 loc='lower right')
    plt.xlabel('Number of segments')
    plt.ylabel('Overall classification accuracy')
    plt.title('Classification accuracy using\nlinear regression method')
    plt.grid()

    ###############
    "Plot Box Plot"
    ###############
    figCAbp, axCAbp = plt.subplots()
    ca_data = []
    ca_pos = []
    wid = []
    i = 0
    for z in mavlengths:
        if z in mavlengths:
            print('zcol CA median mav length =', z, np.median(choose_zcol_ca[i, :] * 100))
            ca_data.append(choose_zcol_ca[i, :] * 100)
            ca_pos.append(z)
            wid.append(1)
        i += 1
    box = axCAbp.boxplot(ca_data, positions=ca_pos, widths=wid)
    axCAbp.set_ylim(ymin=30, ymax=100)
    axCAbp.set_title('Box plot across all subject arms')
    axCAbp.set_ylabel('Classification Accuracy (%)')
    axCAbp.set_xlabel('Number of segments')

    ######################
    """Compare F1 Score"""
    ######################
    figF1bp, axF1bp = plt.subplots()
    f1_data = []
    f1_pos = []
    wid = []
    i = 0
    f1scores = np.mean(choose_zcol_f1[:, :, :], axis=2)
    for z in mavlengths:
        if z in mavlengths:
            print('zcol F1 median mav length =', z, np.median(f1scores[i, :] * 100))
            f1_data.append(f1scores[i, :] * 100)
            f1_pos.append(z)
            wid.append(1)
        i += 1
    box = axF1bp.boxplot(f1_data, positions=f1_pos, widths=wid)
    axF1bp.set_ylim(ymin=30, ymax=100)
    axF1bp.set_title('Box plot across all subject arms')
    axF1bp.set_ylabel('F1 Scores (%)')
    axF1bp.set_xlabel('Number of segments')

    ################
    """Per Finger"""
    ################
    figfinger, axfinger = plt.subplots()
    for f in range(len(finger_names)):
        axfinger.scatter(mavlengths, np.mean(choose_zcol_f1[:, :, f], axis=1), label=finger_names[f])
    axfinger.plot(mavlengths, np.mean(np.mean(choose_zcol_f1[:, :, :], axis=2), axis=1), label='Average across fingers')
    plt.xlabel('Number Segments')
    plt.ylabel('F1 Score')
    plt.legend()
    plt.grid()


    ##########
    plt.show()
    ##########

"""Load and plot atb files
"""

import pylab as plt
import numpy as np
from python.src.readATB import read_atb
import scipy.fftpack
from scipy.signal import freqz
from mpl_toolkits import mplot3d
from scipy.signal import hilbert
from pywt._dwt import dwt


def upsample(x, I):
    xnew = [0] * (I * len(x))
    xnew[::I] = x
    return xnew


if __name__ == '__main__':

    ###########
    '''Setup'''
    ###########

    font = {'family': 'normal',
            'weight': 'normal',
            'size': 20}
    plt.rc('font', **font)

    # Load data
    # atbFolder = 'D:/Code/atbFiles/'
    atbFolder = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/AlazarTech/1kPRF_FingerFlexions_134channel/"

    # atbFile = '60Fs-1RpT-360TpA-2432.atb'
    atbFile = 'Pinky9.atb'
    print('loading', atbFolder + atbFile)
    capture_file_header, channel_file_header_array, atb_sample_data, frame_time_stamps = read_atb(atbFolder + atbFile)
    Fs = capture_file_header.m_dSamplesPerSecond  # Sampling rate along depth
    numFrames = capture_file_header.m_nRecordsPerCapture * capture_file_header.m_nCapturesPerFile

    # Preprocess acquired data
    sample_data = np.zeros(atb_sample_data.shape, dtype=np.dtype('float64'))
    channel_tgc = [0.01, 0.01, 0.01, 0.01]
    for c in range(atb_sample_data.shape[1]):
        for f in range(sample_data.shape[2]):
            # Format to volts
            dSampleZeroValue = (1 << (capture_file_header.m_nBitsPerSample - 1)) - 0.5
            dSampleMaxValue = (1 << (capture_file_header.m_nBitsPerSample - 1)) - 0.5
            nSampleBitShift = (capture_file_header.m_nBytesPerSample * 8) - capture_file_header.m_nBitsPerSample
            sample_data[:, c, f] = np.asarray(atb_sample_data[:, c, f] >> nSampleBitShift, dtype=np.dtype('float64'))
            dMaxVolts = channel_file_header_array[c].m_dInputRange_volts * channel_file_header_array[
                c].m_dProbeAttenuation
            sample_data[:, c, f] = dMaxVolts * (sample_data[:, c, f] - dSampleZeroValue) / dSampleMaxValue
            # DC removal
            sample_data[:, c, f] -= np.mean(sample_data[:, c, f])
            # # Time Gain Compensation
            # for d in range(sample_data.shape[0]):
            #     sample_data[d, c, f] = sample_data[d, c, f] * d * channel_tgc[c]

    # Frame rate correction (when using an external trigger to control pre-triggering)
    PRF = 1000  # based on function generator
    print('Pulse Repetition Frequency', PRF)
    frame_time_stamps = np.arange(0, sample_data.shape[2], 1) / PRF

    # Print atb file information
    print('\nCapture File Header')
    for k, v in capture_file_header.__dict__.items():
        print(k, v)
    print('\nChannel File Header')
    for i in range(len(channel_file_header_array)):
        print('Channel', i)
        for k, v in channel_file_header_array[i].__dict__.items():
            if '__' not in k:
                print(k, v)
    print('\nsample_data', sample_data.shape, '(depth, channel, frame)')

    ###############
    '''Plot Time'''
    ###############

    depthRange = [320, 1540]
    # depthRange = [500, 750]
    print('Depth Range:', depthRange, 500 * 1540 * np.asarray(depthRange) / capture_file_header.m_dSamplesPerSecond, 'mm')
    frameAvg = 1  # number of frames to average for purposes of reducing noise

    # Average along frames
    if frameAvg > 1:
        sig = np.zeros([sample_data.shape[0], sample_data.shape[1], sample_data.shape[2] // frameAvg])
        for d in range(sig.shape[0]):
            for c in range(sig.shape[1]):
                for f in range(sig.shape[2]):
                    sig[d, c, f] = np.mean(sample_data[d, c, f * frameAvg:(f + 1) * frameAvg])
    else:
        sig = sample_data


    # crop depth
    ch1 = np.asarray(sig[depthRange[0]:depthRange[1], 0, :], dtype=np.dtype('float64'))
    ch3 = np.asarray(sig[depthRange[0]:depthRange[1], 2, :], dtype=np.dtype('float64'))
    ch4 = np.asarray(sig[depthRange[0]:depthRange[1], 3, :], dtype=np.dtype('float64'))
    depthX = 500 * 1540 * np.linspace(depthRange[0], depthRange[1], ch1.shape[0]) / capture_file_header.m_dSamplesPerSecond
    # remove DC
    ch1 -= np.mean(ch1)
    ch3 -= np.mean(ch3)
    ch4 -= np.mean(ch4)

    # # Plot time domain RF signal
    # for ch, c in zip([ch3, ch1, ch4], ['1', '2', '3']):
    #     plt.figure()
    #     plt.plot(np.linspace(depthRange[0], depthRange[1], ch1.shape[0]), ch[:, 0])
    #     # plt.axvline(x=320, color='r')
    #     # plt.text(920, -1.5, 'Region of interest', horizontalalignment='center', color='r')
    #     # plt.axvline(x=1520, color='r')
    #     plt.title('WUS-' + c)
    #     plt.xlabel('Depth (Sample)')
    #     plt.ylabel('Amplitude')
    #     plt.grid()


    # # Plot time domain variance RF signal of envelope
    # figtime, axtime = plt.subplots(nrows=3, sharex=True, sharey=True)
    # axtime[0].plot(np.var(ch1, axis=1))
    # axtime[1].plot(np.var(ch3, axis=1))
    # axtime[2].plot(np.var(ch4, axis=1))
    #
    # Plot m-mode
    # depth_min = depthX[0]  # mm
    # depth_max = depthX[1]  # mm
    depth_min = depthRange[0]  # sample
    depth_max = depthRange[1]  # sample
    time_min = 0  # seconds
    time_max = numFrames / PRF  # seconds
    mn = 0
    mx = 0.4
    figmmode, axmmode = plt.subplots(ncols=3, sharex=True, sharey=True)
    pos1 = axmmode[0].imshow(np.abs(hilbert(ch3)), aspect='auto', extent=[time_min, time_max, depth_max, depth_min], vmin=mn, vmax=mx)
    axmmode[0].set_title('WUS-1')
    # axmmode[0].set_xlabel('Time (s)')
    # axmmode[0].set_ylabel('Depth (Sample)')
    figmmode.colorbar(pos1, ax=axmmode[0])
    pos2 = axmmode[1].imshow(np.abs(hilbert(ch1)), aspect='auto', extent=[time_min, time_max, depth_max, depth_min], vmin=mn, vmax=mx)
    axmmode[1].set_title('WUS-2')
    # axmmode[1].set_xlabel('Time (s)')
    figmmode.colorbar(pos2, ax=axmmode[1])
    pos3 = axmmode[2].imshow(np.abs(hilbert(ch4)), aspect='auto', extent=[time_min, time_max, depth_max, depth_min], vmin=mn, vmax=mx)
    axmmode[2].set_title('WUS-3')
    # axmmode[2].set_xlabel('Time (s)')
    figmmode.colorbar(pos3, ax=axmmode[2])

    # # Plot a single channel to compare frame averaging effect
    # figmmode, axmmodeavg = plt.subplots()
    # pos1 = axmmodeavg.imshow(np.abs(hilbert(ch3)), aspect='auto', extent=[time_min, time_max, depth_max, depth_min])
    # axmmodeavg.set_title('WUS-1')
    # axmmodeavg.set_xlabel('Time (s)')
    # axmmodeavg.set_ylabel('Depth (Sample)')
    # figmmode.colorbar(pos1, ax=axmmodeavg)

    # # Plot m-mode as 3d
    # depth_axis = np.linspace(0, 1.54 * (1/Fs) / (depthRange[1] - depthRange[0]), ch1.shape[0])
    # time_axis = np.linspace(0, numFrames/PRF, ch1.shape[1])
    # X, Y = np.meshgrid(time_axis, depth_axis)
    # print(X.shape, Y.shape, ch1.shape)
    # fig = plt.figure()
    # ax = plt.axes(projection='3d')
    # ax.plot_surface(X, Y, np.abs(hilbert(ch3)), cmap='viridis')

    ###############
    '''Plot Freq'''
    ###############
    # scaling FIR filter
    db3_g = [0.035226291882100656,
             -0.08544127388224149,
             -0.13501102001039084,
             0.4598775021193313,
             0.8068915093133388,
             0.3326705529509569]
    # wavelet FIR filter
    db3_h = [-0.3326705529509569,
             0.8068915093133388,
             -0.4598775021193313,
             -0.13501102001039084,
             0.08544127388224149,
             0.035226291882100656]

    # sample spacing
    T = 1.0 / 125000000.0

    scal1 = np.divide(db3_g, np.sqrt(2))
    wav1 = np.divide(db3_h, np.sqrt(2))
    wav2 = np.divide(np.convolve(upsample(wav1, 2), scal1), np.sqrt(2))
    scal2 = np.divide(np.convolve(upsample(scal1, 2), scal1), np.sqrt(2))
    wav3 = np.divide(np.convolve(upsample(wav2, 2), scal2), np.sqrt(2))
    scal3 = np.divide(np.convolve(upsample(scal2, 2), scal2), np.sqrt(2))
    wav4 = np.divide(np.convolve(upsample(wav3, 2), scal3), np.sqrt(2))
    scal4 = np.divide(np.convolve(upsample(scal3, 2), scal3), np.sqrt(2))
    wav5 = np.divide(np.convolve(upsample(wav4, 2), scal4), np.sqrt(2))
    scal5 = np.divide(np.convolve(upsample(scal4, 2), scal4), np.sqrt(2))
    wav6 = np.divide(np.convolve(upsample(wav5, 2), scal5), np.sqrt(2))
    scal6 = np.divide(np.convolve(upsample(scal5, 2), scal5), np.sqrt(2))
    wav7 = np.divide(np.convolve(upsample(wav6, 2), scal6), np.sqrt(2))
    scal7 = np.divide(np.convolve(upsample(scal6, 2), scal6), np.sqrt(2))

    w1, h1 = freqz(wav1)
    w2, h2 = freqz(wav2)
    w3, h3 = freqz(wav3)
    w4, h4 = freqz(wav4)
    w5, h5 = freqz(wav5)
    w6, h6 = freqz(wav6)
    w7, h7 = freqz(wav7)

    w1 = w1 * (1 / (T * 10 ** 6)) / (2 * np.pi)
    w2 = w2 * (1 / (T * 10 ** 6)) / (2 * np.pi)
    w3 = w3 * (1 / (T * 10 ** 6)) / (2 * np.pi)
    w4 = w4 * (1 / (T * 10 ** 6)) / (2 * np.pi)
    w5 = w5 * (1 / (T * 10 ** 6)) / (2 * np.pi)
    w6 = w6 * (1 / (T * 10 ** 6)) / (2 * np.pi)
    w7 = w7 * (1 / (T * 10 ** 6)) / (2 * np.pi)

    h1 = np.abs(h1) / np.max(np.abs(h1))
    h2 = np.abs(h2) / np.max(np.abs(h2))
    h3 = np.abs(h3) / np.max(np.abs(h3))
    h4 = np.abs(h4) / np.max(np.abs(h4))
    h5 = np.abs(h5) / np.max(np.abs(h5))
    h6 = np.abs(h6) / np.max(np.abs(h6))
    h7 = np.abs(h7) / np.max(np.abs(h7))

    # FFT of along depth axis
    N = depthRange[1] - depthRange[0]
    yf1 = scipy.fftpack.fft(ch1[:, 0] - np.mean(ch1[:, 0]))
    yf3 = scipy.fftpack.fft(ch3[:, 0] - np.mean(ch3[:, 0]))
    yf4 = scipy.fftpack.fft(ch4[:, 0] - np.mean(ch4[:, 0]))
    for frame in range(1, ch1.shape[1]):
        yf1 += scipy.fftpack.fft(ch1[:, frame] - np.mean(ch1[:, frame]))
        yf3 += scipy.fftpack.fft(ch3[:, frame] - np.mean(ch3[:, frame]))
        yf4 += scipy.fftpack.fft(ch4[:, frame] - np.mean(ch4[:, frame]))
    xf = np.linspace(0.0, 1.0 / (2.0 * T), N // 2)
    yf1 = (2.0 / N * np.abs(yf1[:N // 2]) / np.max((2.0 / N * np.abs(yf1[:N // 2]))))
    yf3 = (2.0 / N * np.abs(yf3[:N // 2]) / np.max((2.0 / N * np.abs(yf3[:N // 2]))))
    yf4 = (2.0 / N * np.abs(yf4[:N // 2]) / np.max((2.0 / N * np.abs(yf4[:N // 2]))))

    # # Plot freq spectrum along depth axis
    # figfreq, axfreq = plt.subplots(nrows=3, sharex=True)
    #
    # axfreq[0].plot(xf / 10 ** 6, yf1)
    # axfreq[0].plot(w1, h1)
    # axfreq[0].plot(w2, h2)
    # axfreq[0].plot(w3, h3)
    # axfreq[0].plot(w4, h4)
    # axfreq[0].plot(w5, h5)
    # axfreq[0].plot(w6, h6)
    # axfreq[0].plot(w7, h7)
    #
    # axfreq[1].plot(xf / 10 ** 6, yf3)
    # axfreq[1].plot(w1, h1)
    # axfreq[1].plot(w2, h2)
    # axfreq[1].plot(w3, h3)
    # axfreq[1].plot(w4, h4)
    # axfreq[1].plot(w5, h5)
    # axfreq[1].plot(w6, h6)
    # axfreq[1].plot(w7, h7)
    #
    # axfreq[2].plot(xf / 10 ** 6, yf4)
    # axfreq[2].plot(w1, h1)
    # axfreq[2].plot(w2, h2)
    # axfreq[2].plot(w3, h3)
    # axfreq[2].plot(w4, h4)
    # axfreq[2].plot(w5, h5)
    # axfreq[2].plot(w6, h6)
    # axfreq[2].plot(w7, h7)

    # # Plot pretty FFT
    # N = depthRange[1] - depthRange[0]
    # yf = scipy.fftpack.fft(ch4[:, 0] - np.mean(ch4[:, 0]))
    # xf = np.linspace(0.0, 1.0 / (2.0 * T), N // 2)
    # yf = (2.0 / N * np.abs(yf[:N // 2]) / np.max((2.0 / N * np.abs(yf[:N // 2]))))
    # plt.figure()
    # plt.plot(xf / 10 ** 6, yf, label='FFT of ROI')
    # plt.plot(w1, h1, label='D1 Bandwidth')
    # plt.plot(w2, h2, label='D2 Bandwidth')
    # plt.plot(w3, h3, label='D3 Bandwidth')
    # plt.plot(w4, h4, label='D4 Bandwidth')
    # plt.plot(w5, h5, label='D5 Bandwidth')
    # plt.plot(w6, h6, label='D6 Bandwidth')
    # plt.xscale('log', basex=10)
    # plt.xlim([0.1, 100])
    # plt.xlabel('Frequency (MHz)')
    # plt.ylabel('Normalized Amplitude')
    # # plt.legend(loc='upper center', bbox_to_anchor=(1.2, 1.0))
    # plt.legend()
    # plt.grid()

    # # Plot DWT levels
    # approx = ch4[:, 0]
    # levels = [1, 2, 3, 4, 5, 6]
    # fig, axdwt = plt.subplots(nrows=len(levels)+1)
    # waveletname = 'db3'
    # axcount = 0
    # axdwt[axcount].plot(ch4[:, 0], color='C0')
    # axdwt[axcount].plot(ch4[:, 0], '.', color='C0', markersize=5)
    # axdwt[axcount].set_ylabel('ROI')
    # axdwt[axcount].set_ylim([-1, 1])
    # axdwt[axcount].grid()
    # # Discrete Wavelet Transform applied along depth axis
    # for lvl in range(1, levels[-1]+1):
    #     # recursively obtain wavelet 'detail' coefficients using previous approx coefficients
    #     (approx, detail) = dwt(approx, waveletname, 'zero')
    #     if lvl in levels:
    #         axcount += 1
    #         axdwt[axcount].plot(detail, color='C0')
    #         axdwt[axcount].plot(detail, '.', color='C0', markersize=5)
    #         axdwt[axcount].set_ylabel('D' + str(lvl))
    #         axdwt[axcount].set_ylim([-1, 1])
    #         axdwt[axcount].grid()
    # axdwt[axcount].set_xlabel('Depth (sample)')


    # # FFT along frame axis
    # N = ch1.shape[1]
    # yf1 = scipy.fftpack.fft(ch1[0, :] - np.mean(ch1[0, :]))
    # yf3 = scipy.fftpack.fft(ch3[0, :] - np.mean(ch3[0, :]))
    # yf4 = scipy.fftpack.fft(ch4[0, :] - np.mean(ch4[0, :]))
    # for depth in range(1, ch1.shape[0]):
    #     yf1 += scipy.fftpack.fft(ch1[depth, :] - np.mean(ch1[depth, :]))
    #     yf3 += scipy.fftpack.fft(ch3[depth, :] - np.mean(ch3[depth, :]))
    #     yf4 += scipy.fftpack.fft(ch4[depth, :] - np.mean(ch4[depth, :]))
    # xf = np.linspace(0.0, PRF / (2.0 * frameAvg), N // 2)
    # yf1 = (2.0 / N * np.abs(yf1[:N // 2]) / np.max((2.0 / N * np.abs(yf1[:N // 2]))))
    # yf3 = (2.0 / N * np.abs(yf3[:N // 2]) / np.max((2.0 / N * np.abs(yf3[:N // 2]))))
    # yf4 = (2.0 / N * np.abs(yf4[:N // 2]) / np.max((2.0 / N * np.abs(yf4[:N // 2]))))

    # # Plot freq spectrum along frame axis
    # figtime, axtime = plt.subplots(nrows=3, sharex=True, sharey=True)
    # axtime[0].plot(xf, yf3)
    # axtime[1].plot(xf, yf1)
    # axtime[2].plot(xf, yf4)

    plt.show()

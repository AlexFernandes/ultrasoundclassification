import time

from python.src.zrfManager import ZRFData
from python.src.signalProcessor import average_into_zcols_by_frame
import numpy as np
from matplotlib import pyplot as plt
import scipy
from scipy.signal import freqz
from pywt._dwt import dwt

N = 216  # sample points per 5 mm
T = 30 / 10 ** 9  # sampling period
Fs = 1 / T


def fft5mm(sig):
    L = sig.shape[0] // 7
    sig = sig - np.mean(sig)  # dc filtering
    yfreq = np.empty([7, L])
    yfreq[0, :] = scipy.fftpack.fft(sig[0:L])
    yfreq[1, :] = scipy.fftpack.fft(sig[L:L * 2])
    yfreq[2, :] = scipy.fftpack.fft(sig[L * 2:L * 3])
    yfreq[3, :] = scipy.fftpack.fft(sig[L * 3:L * 4])
    yfreq[4, :] = scipy.fftpack.fft(sig[L * 4:L * 5])
    yfreq[5, :] = scipy.fftpack.fft(sig[L * 5:L * 6])
    yfreq[6, :] = scipy.fftpack.fft(sig[L * 6:L * 7])
    return yfreq


def centerfreq(xfreq, yfreq):
    My = np.zeros([yfreq.shape[0]])
    Mx = np.zeros([yfreq.shape[0]])
    M = np.zeros([yfreq.shape[0]])
    for i in range(1, yfreq.shape[1] // 2):
        for r in range(yfreq.shape[0]):
            My[r] += i * abs(yfreq[r, i])
            Mx[r] += (abs(yfreq[r, i]) ** 2) / 2
            M[r] += abs(yfreq[r, i])
    cfreq = np.zeros([yfreq.shape[0], 2])
    for r in range(yfreq.shape[0]):
        cfreq[r, 0] = xfreq[int(My[r] / M[r])]
        cfreq[r, 1] = Mx[r] / M[r]
    return cfreq


def plotfft5mm(xfreq, yfreq, ax, title):
    cfreq = centerfreq(xfreq, yfreq)
    print(cfreq)
    L = yfreq.shape[1]
    ax[0].plot(xfreq[:] / 10 ** 6, (2.0 / L * yfreq[0, 0:L // 2]) / np.max(2.0 / L * yfreq[0, 0:L // 2]))
    ax[0].plot(cfreq[0, 0] / 10 ** 6, 0, 'o')
    ax[1].plot(xfreq[:] / 10 ** 6, (2.0 / L * yfreq[1, 0:L // 2]) / np.max(2.0 / L * yfreq[1, 0:L // 2]))
    ax[1].plot(cfreq[1, 0] / 10 ** 6, 0, 'o')
    ax[2].plot(xfreq[:] / 10 ** 6, (2.0 / L * yfreq[2, 0:L // 2]) / np.max(2.0 / L * yfreq[2, 0:L // 2]))
    ax[2].plot(cfreq[2, 0] / 10 ** 6, 0, 'o')
    ax[3].plot(xfreq[:] / 10 ** 6, (2.0 / L * yfreq[3, 0:L // 2]) / np.max(2.0 / L * yfreq[3, 0:L // 2]))
    ax[3].plot(cfreq[3, 0] / 10 ** 6, 0, 'o')
    ax[4].plot(xfreq[:] / 10 ** 6, (2.0 / L * yfreq[4, 0:L // 2]) / np.max(2.0 / L * yfreq[4, 0:L // 2]))
    ax[4].plot(cfreq[4, 0] / 10 ** 6, 0, 'o')
    ax[5].plot(xfreq[:] / 10 ** 6, (2.0 / L * yfreq[5, 0:L // 2]) / np.max(2.0 / L * yfreq[5, 0:L // 2]))
    ax[5].plot(cfreq[5, 0] / 10 ** 6, 0, 'o')
    ax[6].plot(xfreq[:] / 10 ** 6, (2.0 / L * yfreq[6, 0:L // 2]) / np.max(2.0 / L * yfreq[6, 0:L // 2]))
    ax[6].plot(cfreq[6, 0] / 10 ** 6, 0, 'o')
    plt.xlabel('Frequency (MHz)')
    ax[0].set_title(title)
    ax[0].set_ylabel('FFT 0-5mm')
    ax[1].set_ylabel('FFT 5-10mm')
    ax[2].set_ylabel('FFT 10-15mm')
    ax[3].set_ylabel('FFT 15-20mm')
    ax[4].set_ylabel('FFT 20-25mm')
    ax[5].set_ylabel('FFT 25-30mm')
    ax[6].set_ylabel('FFT 30-35mm')


def print_elapsed_time(startTime):
    elapsed_time = time.time()
    hours, rem = divmod(elapsed_time - startTime, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Elapsed Time: {:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))


def upsample(x, I):
    xnew = [0] * (I * len(x))
    xnew[::I] = x
    return xnew


if __name__ == "__main__":
    # Time Start
    startTime = time.time()
    ###########
    """Setup"""
    ###########
    # zrfDir = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/zrfFiles/"
    zrfDir = "D:/Code/zrfFiles/"
    subjectPaths = ["AlexFingerMovement/FingerMovement/",
                    "AlexRightFingerMovement/RightFingerMotions/",
                    "fm_sub1_l/FingerMotions/",
                    "fm_sub1_r/FingerMotions/",
                    "fm_sub2_l/FingerMotions/",
                    "fm_sub2_r/FingerMotions/",
                    "fm_sub3_l/FingerMotions/",
                    "fm_sub3_r/FingerMotions/",
                    "fm_sub4_l/FingerMotions/",
                    "fm_sub4_r/FingerMotions/"]
    fingerNames = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    numTrials = 3

    ################
    """Obtain FFT"""
    ################
    xfreq = np.linspace(0.0, 1.0 / (2.0 * T), int(N / 2))
    yfreq = np.zeros([7, N])
    wav1freq = np.zeros([760])
    wav2freq = np.zeros([760])
    wav3freq = np.zeros([760])
    wav4freq = np.zeros([760])
    wav5freq = np.zeros([760])
    waveletname = 'db3'
    numZcols = 6
    for subPath in subjectPaths:
        filePath = zrfDir + subPath
        print(subPath)
        for fingerID in range(len(fingerNames)):
            for trial in range(1, numTrials + 1):
                zrf = ZRFData(filePath + fingerNames[fingerID] + str(trial) + '.zrf')
                for frame in range(zrf.rfsig.shape[2]):
                    for col in range(zrf.rfsig.shape[1]):
                        yfreq += np.abs(fft5mm(zrf.rfsig[:, col, frame]))
                    sig = average_into_zcols_by_frame(zrf.rfsig[:, :, frame], numZcols)
                    for col in range(numZcols):
                        (a, d1) = dwt(sig[:, col] - np.mean(sig[:, col]), waveletname, 'zero')
                        (a, d2) = dwt(a, waveletname, 'zero')
                        (a, d3) = dwt(a, waveletname, 'zero')
                        (a, d4) = dwt(a, waveletname, 'zero')
                        (a, d5) = dwt(a, waveletname, 'zero')
                        wav1freq += np.abs(scipy.fftpack.fft(d1, n=wav1freq.shape[0]))
                        wav2freq += np.abs(scipy.fftpack.fft(d2, n=wav2freq.shape[0]))
                        wav3freq += np.abs(scipy.fftpack.fft(d3, n=wav3freq.shape[0]))
                        wav4freq += np.abs(scipy.fftpack.fft(d4, n=wav4freq.shape[0]))
                        wav5freq += np.abs(scipy.fftpack.fft(d5, n=wav5freq.shape[0]))

        print_elapsed_time(startTime)

    #####################
    """Obtain Wavelets"""
    #####################

    # db3 coefficients
    db3_g0 = [0.3326705529509569,
              0.8068915093133388,
              0.4598775021193313,
              -0.13501102001039084,
              -0.08544127388224149,
              0.035226291882100656]
    db3_g1 = [0.035226291882100656,
              0.08544127388224149,
              -0.13501102001039084,
              -0.4598775021193313,
              0.8068915093133388,
              -0.3326705529509569]

    # Recursive approach
    db3_scal1 = np.divide(db3_g0, np.sqrt(2))
    db3_wav1 = np.divide(db3_g1, np.sqrt(2))
    db3_scal2 = np.divide(np.convolve(upsample(db3_scal1, 2), db3_scal1), np.sqrt(2))
    db3_wav2 = np.divide(np.convolve(upsample(db3_wav1, 2), db3_scal1), np.sqrt(2))
    db3_scal3 = np.divide(np.convolve(upsample(db3_scal2, 2), db3_scal2), np.sqrt(2))
    db3_wav3 = np.divide(np.convolve(upsample(db3_wav2, 2), db3_scal2), np.sqrt(2))
    db3_scal4 = np.divide(np.convolve(upsample(db3_scal3, 2), db3_scal3), np.sqrt(2))
    db3_wav4 = np.divide(np.convolve(upsample(db3_wav3, 2), db3_scal3), np.sqrt(2))
    db3_scal5 = np.divide(np.convolve(upsample(db3_scal4, 2), db3_scal4), np.sqrt(2))
    db3_wav5 = np.divide(np.convolve(upsample(db3_wav4, 2), db3_scal4), np.sqrt(2))

    db3_scal_w1, db3_scal_h1 = freqz(db3_scal1)
    db3_scal_w2, db3_scal_h2 = freqz(db3_scal2)
    db3_scal_w3, db3_scal_h3 = freqz(db3_scal3)
    db3_scal_w4, db3_scal_h4 = freqz(db3_scal4)
    db3_scal_w5, db3_scal_h5 = freqz(db3_scal5)
    db3_wav_w1, db3_wav_h1 = freqz(db3_wav1)
    db3_wav_w2, db3_wav_h2 = freqz(db3_wav2)
    db3_wav_w3, db3_wav_h3 = freqz(db3_wav3)
    db3_wav_w4, db3_wav_h4 = freqz(db3_wav4)
    db3_wav_w5, db3_wav_h5 = freqz(db3_wav5)

    ##########
    """Plot"""
    ##########
    # fig, ax1 = plt.subplots(nrows=7, sharex=True)
    # plotfft5mm(xfreq, yfreq, ax1, 'All subjects, all fingers every 5mm')
    # for i in range(7):
    #     ax1[i].plot(Fs * db3_wav_w1 / (2 * np.pi * 10 ** 6), np.abs(db3_wav_h1) / np.max(np.abs(db3_wav_h1)))
    #     ax1[i].plot(Fs * db3_wav_w2 / (2 * np.pi * 10 ** 6), np.abs(db3_wav_h2) / np.max(np.abs(db3_wav_h2)))
    #     ax1[i].plot(Fs * db3_wav_w3 / (2 * np.pi * 10 ** 6), np.abs(db3_wav_h3) / np.max(np.abs(db3_wav_h3)))
    #     ax1[i].plot(Fs * db3_wav_w4 / (2 * np.pi * 10 ** 6), np.abs(db3_wav_h4) / np.max(np.abs(db3_wav_h4)))
    #     ax1[i].plot(Fs * db3_wav_w5 / (2 * np.pi * 10 ** 6), np.abs(db3_wav_h5) / np.max(np.abs(db3_wav_h5)))
    # plt.subplots_adjust(right=0.7)
    # ax1[0].legend(['FFT spectrum',
    #                'center freq',
    #                'wav 1',
    #                'wav 2',
    #                'wav 3',
    #                'wav 4',
    #                'wav 5'],
    #               bbox_to_anchor=(1.05, 1), loc=2)

    fig, ax_wavfreq = plt.subplots()
    ax_wavfreq.plot(np.linspace(0, Fs / (2 * 10**6), wav1freq.size // 2), wav1freq[0:wav1freq.size // 2] / np.max(wav2freq[0:wav2freq.size // 2]))
    ax_wavfreq.plot(np.linspace(0, Fs / (2 * 10**6), wav2freq.size // 2), wav2freq[0:wav2freq.size // 2] / np.max(wav2freq[0:wav2freq.size // 2]))
    ax_wavfreq.plot(np.linspace(0, Fs / (2 * 10**6), wav3freq.size // 2), wav3freq[0:wav3freq.size // 2] / np.max(wav2freq[0:wav2freq.size // 2]))
    ax_wavfreq.plot(np.linspace(0, Fs / (2 * 10**6), wav4freq.size // 2), wav4freq[0:wav4freq.size // 2] / np.max(wav2freq[0:wav2freq.size // 2]))
    ax_wavfreq.plot(np.linspace(0, Fs / (2 * 10**6), wav5freq.size // 2), wav5freq[0:wav5freq.size // 2] / np.max(wav2freq[0:wav2freq.size // 2]))
    plt.xlabel('Frequency (MHz)')
    plt.ylabel('Normalized FFT')
    plt.legend(['D1',
                'D2',
                'D3',
                'D4',
                'D5'])
    plt.title('Frequency spectrum for the computed detail coefficients')

    fig, ax = plt.subplots()

    ax.plot(xfreq[1:] / 10 ** 6,
            2.0 / N * np.sum(yfreq[:, 1:N // 2], axis=0) / np.max(2.0 / N * np.sum(yfreq[:, 1:N // 2], axis=0)),
            label='fft')

    ax.plot(db3_wav_w1 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_wav_h1) / np.max(np.abs(db3_wav_h1)),
            label='db3 wav 1')
    ax.plot(db3_wav_w2 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_wav_h2) / np.max(np.abs(db3_wav_h2)),
            label='db3 wav 2')
    ax.plot(db3_wav_w3 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_wav_h3) / np.max(np.abs(db3_wav_h3)),
            label='db3 wav 3')
    ax.plot(db3_wav_w4 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_wav_h4) / np.max(np.abs(db3_wav_h4)),
            label='db3 wav 4')
    ax.plot(db3_wav_w5 * (Fs / 10 ** 6) / (2 * np.pi), np.abs(db3_wav_h5) / np.max(np.abs(db3_wav_h5)),
            label='db3 wav 5')
    plt.subplots_adjust(right=0.7)
    ax.set_title("Frequency spectrum across all recorded RF signals")
    plt.legend()
    plt.xlabel('Frequency (MHz)')
    plt.ylabel('Normalized FFT')
    plt.xscale('log', basex=10)

    # fig, ax_wav = plt.subplots(nrows=7, sharex=True)
    # L = yfreq.shape[1]
    # ax_wav[0].plot(xfreq / 10 ** 6, (2.0 / L * yfreq[0, 0:L // 2]) / np.max(2.0 / L * yfreq[0, 0:L // 2]))
    # ax_wav[1].plot(xfreq / 10 ** 6, (2.0 / L * yfreq[1, 0:L // 2]) / np.max(2.0 / L * yfreq[1, 0:L // 2]))
    # ax_wav[2].plot(xfreq / 10 ** 6, (2.0 / L * yfreq[2, 0:L // 2]) / np.max(2.0 / L * yfreq[2, 0:L // 2]))
    # ax_wav[3].plot(xfreq / 10 ** 6, (2.0 / L * yfreq[3, 0:L // 2]) / np.max(2.0 / L * yfreq[3, 0:L // 2]))
    # ax_wav[4].plot(xfreq / 10 ** 6, (2.0 / L * yfreq[4, 0:L // 2]) / np.max(2.0 / L * yfreq[4, 0:L // 2]))
    # ax_wav[5].plot(xfreq / 10 ** 6, (2.0 / L * yfreq[5, 0:L // 2]) / np.max(2.0 / L * yfreq[5, 0:L // 2]))
    # ax_wav[6].plot(xfreq / 10 ** 6, (2.0 / L * yfreq[6, 0:L // 2]) / np.max(2.0 / L * yfreq[6, 0:L // 2]))
    # plt.xlabel('Frequency (MHz)')
    # ax_wav[0].set_title('All subjects')
    # ax_wav[0].set_ylabel('FFT 0-5mm')
    # ax_wav[1].set_ylabel('FFT 5-10mm')
    # ax_wav[2].set_ylabel('FFT 10-15mm')
    # ax_wav[3].set_ylabel('FFT 15-20mm')
    # ax_wav[4].set_ylabel('FFT 20-25mm')
    # ax_wav[5].set_ylabel('FFT 25-30mm')
    # ax_wav[6].set_ylabel('FFT 30-35mm')

    plt.show()

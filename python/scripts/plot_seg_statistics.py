from xlrd import open_workbook
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy import stats
from python.src.zrfPlot import plot_confusion_matrix

def loadScores(excelFile, numSegs):
    wb = open_workbook(excelFile)
    f1scores = np.zeros([len(numSegs), wb.nsheets - 1, 5])
    classacc = np.zeros([len(numSegs), wb.nsheets - 1])
    s = -1
    for sheet in wb.sheets():
        if sheet.name != 'Statistics':
            s = s + 1
            z = -1
            for zcol in numSegs:
                z += 1
                cnf_matrix = np.zeros([5, 5], np.uint32)
                for row in range(5):
                    for col in range(5):
                        cnf_matrix[row][col] = sheet.cell((zcol - 1) * 6 + row + 1, col).value
                acc = 0
                for i in range(5):
                    for j in range(5):
                        if i == j:
                            acc = acc + cnf_matrix[i][j]
                acc = acc / np.sum(cnf_matrix)
                f1 = np.zeros([5], np.double)
                for f in range(5):
                    p = cnf_matrix[f][f] / np.sum(cnf_matrix, 0, np.uint32)[f]
                    r = cnf_matrix[f][f] / np.sum(cnf_matrix, 1, np.uint32)[f]
                    f1[f] = 2 * p * r / (p + r)
                    f1scores[z, s, f] = f1[f]
                classacc[z, s] = acc
    return f1scores, classacc


def loadSumCnf(excelFile, numSegs):
    wb = open_workbook(excelFile)
    cnf_matricies = np.zeros([len(numSegs), 5, 5], np.uint32)
    for sheet in wb.sheets():
        if sheet.name != 'Statistics':
            z = -1
            for zcol in numSegs:
                z += 1
                for row in range(5):
                    for col in range(5):
                        cnf_matricies[z, row, col] += sheet.cell((zcol - 1) * 6 + row + 1, col).value
    return cnf_matricies


if __name__ == "__main__":
    ###########
    """Setup"""
    ###########

    dataDir = 'D:/Code/ultrasoundclassification/data/accuracy/'
    # dataDir = '/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/ultrasoundclassification/' \
    #           'data/accuracy/'

    numSegs = np.asarray([_ for _ in range(32, 550, 32)])
    # numSegs = np.asarray([_ for _ in range(1, 101, 1)])
    numSegs = numSegs[numSegs != 49]

    zcol6_file = 'linfit_fwe_segs1to544_zcol-6.xlsx'
    # zcol6_file = 'linfit_fwe_segs1to100_zcol-6.xlsx'
    zcol6_f1, zcol6_ca = loadScores(dataDir + zcol6_file, numSegs)
    zcol6_cnf_matricies = loadSumCnf(dataDir + zcol6_file, numSegs)

    font = {'family': 'normal',
            'weight': 'normal',
            'size': 16}
    matplotlib.rc('font', **font)

    colour_zcol6 = 'blue'

    dashed_line_weight = 0.5

    colour_fingers = ['blue', 'red', 'green', 'purple', 'black']
    finger_names = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']

    #############################
    """Classification Accuracy"""
    #############################
    m_ca = np.median(zcol6_ca[:, :], axis=1) * 100
    print('Max CA', np.amax(m_ca), 'at num segs', numSegs[np.where(m_ca == np.amax(m_ca))])
    q75_ca = np.percentile(zcol6_ca, q=75, axis=1) * 100
    q25_ca = np.percentile(zcol6_ca, q=25, axis=1) * 100
    fig, axCA = plt.subplots()
    axCA.scatter(numSegs, q75_ca, marker='^', c='b')
    axCA.scatter(numSegs, m_ca, c='r')
    axCA.scatter(numSegs, q25_ca, marker='v', c='g')
    p2, p1, p0 = np.polyfit(numSegs, q75_ca, 2)
    axCA.plot(numSegs, p2*numSegs*numSegs + p1*numSegs + p0, 'b')
    p2, p1, p0 = np.polyfit(numSegs, m_ca, 2)
    axCA.plot(numSegs, p2*numSegs*numSegs + p1*numSegs + p0, 'r')
    p2, p1, p0 = np.polyfit(numSegs, q25_ca, 2)
    axCA.plot(numSegs, p2*numSegs*numSegs + p1*numSegs + p0, 'g')
    axCA.set_ylim(ymin=50, ymax=100)
    plt.xlabel('Number of segments')
    plt.ylabel('Classification Accuracy (%)')
    # plt.title('Classification acurracy with N=6 reconstructed RF signals')
    plt.legend(['upper quartile trend line',
                'median trendline',
                'lower quartile trendline',
                'upper quartile',
                'median',
                'lower quartile'], bbox_to_anchor=(1.0, 1.0))
    plt.grid()


    #########################################
    """Per subject Classification Accuracy"""
    #########################################
    fig, axsub = plt.subplots()
    for sub in range(10):
        # axsub.scatter(numSegs, zcol6_ca[:, sub])
        p2, p1, p0 = np.polyfit(numSegs, zcol6_ca[:, sub], 2)
        axsub.plot(numSegs, p2*numSegs*numSegs + p1*numSegs + p0)
    # axsub.set_ylim(ymin=0, ymax=1)
    axsub.legend([
                  # 'sub1 l arm',
                  # 'sub1 r arm',
                  # 'sub2 l arm',
                  # 'sub2 r arm',
                  # 'sub3 l arm',
                  # 'sub3 r arm',
                  # 'sub4 l arm',
                  # 'sub4 r arm',
                  # 'sub5 l arm',
                  # 'sub5 r arm',
                  'sub1 l trendline',
                  'sub1 r trendline',
                  'sub2 l trendline',
                  'sub2 r trendline',
                  'sub3 l trendline',
                  'sub3 r trendline',
                  'sub4 l trendline',
                  'sub4 r trendline',
                  'sub5 l trendline',
                  'sub5 r trendline'],
                 loc='lower right')
    plt.xlabel('Number of segments')
    plt.ylabel('Overall classification accuracy')
    plt.title('Classification accuracy using\nlinear regression method')
    plt.grid()

    ###############
    "Plot Box Plot"
    ###############
    figCAbp, axCAbp = plt.subplots()
    ca_data = []
    ca_pos = []
    wid = []
    i = 0
    for z in numSegs:
        if z in numSegs:
            print('6 zcol CA median seg =', z, np.median(zcol6_ca[i, :] * 100))
            ca_data.append(zcol6_ca[i, :] * 100)
            ca_pos.append(z)
            wid.append(2)
        i += 1
    box = axCAbp.boxplot(ca_data, positions=ca_pos, widths=wid)
    axCAbp.set_ylim(ymin=30, ymax=100)
    axCAbp.set_title('Box plot across all subject arms')
    axCAbp.set_ylabel('Classification Accuracy (%)')
    axCAbp.set_xlabel('Number of segments')

    ######################
    """Compare F1 Score"""
    ######################
    figF1bp, axF1bp = plt.subplots()
    f1_data = []
    f1_pos = []
    wid = []
    i = 0
    f1scores = np.mean(zcol6_f1[:, :, :], axis=2)
    for z in numSegs:
        if z in numSegs:
            print('6 zcol F1 median seg =', z, np.median(f1scores[i, :] * 100))
            f1_data.append(f1scores[i, :] * 100)
            f1_pos.append(z)
            wid.append(2)
        i += 1
    box = axF1bp.boxplot(f1_data, positions=f1_pos, widths=wid)
    axF1bp.set_ylim(ymin=30, ymax=100)
    axF1bp.set_title('Box plot across all subject arms')
    axF1bp.set_ylabel('F1 Scores (%)')
    axF1bp.set_xlabel('Number of segments')

    ################
    """Per Finger"""
    ################
    figfinger, axfinger = plt.subplots()
    for f in range(len(finger_names)):
        axfinger.scatter(numSegs, np.mean(zcol6_f1[:, :, f], axis=1), label=finger_names[f])
    axfinger.plot(numSegs, np.mean(np.mean(zcol6_f1[:, :, :], axis=2), axis=1), label='Average across fingers')
    plt.xlabel('Number Segments')
    plt.ylabel('F1 Score')
    plt.legend()
    plt.grid()


    ##########
    plt.show()
    ##########

"""Classify finger motions using linear fit as features

"""
from python.src.zrfManager import ZRFData
from python.src.zrfPlot import plot_confusion_matrix
import xlsxwriter
import python.src.patternClassification as pc
import numpy as np
import time
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import SelectFwe, f_classif


def print_elapsed_time(startTime):
    elapsed_time = time.time()
    hours, rem = divmod(elapsed_time - startTime, 3600)
    minutes, seconds = divmod(rem, 60)
    print("Elapsed Time: {:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))


def write_cnf_worksheet(worksheet, cnf_matrix, zcols, accmean, accstd, row, col):
    worksheet.write(row, col, 'zcols:')
    worksheet.write(row, col + 1, zcols)
    worksheet.write(row, col + 2, 'acc:')
    worksheet.write(row, col + 3, accmean)
    worksheet.write(row, col + 4, accstd)
    for thumb, index, middle, ring, pinky in cnf_matrix:
        worksheet.write(row + 1, col, thumb)
        worksheet.write(row + 1, col + 1, index)
        worksheet.write(row + 1, col + 2, middle)
        worksheet.write(row + 1, col + 3, ring)
        worksheet.write(row + 1, col + 4, pinky)
        row += 1


if __name__ == "__main__":
    # Time Start
    startTime = time.time()

    # dataDir = '/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/' \
    #           'ultrasoundclassification/data/'
    dataDir = "D:/Code/ultrasoundclassification/data/"

    workbook = xlsxwriter.Workbook(dataDir + 'accuracy/linfit_seg20_fwe-a5.xlsx')

    fingerNames = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    fingerLabel = {'Thumb': 0, 'Index': 1, 'Middle': 2, 'Ring': 3, 'Pinky': 4}
    numSegments = 20
    # numZColumns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 40, 50, 60, 70, 80, 90,
    #                100, 110, 120, 127]
    numZColumns = [1, 2, 3, 4, 6, 8, 12, 16, 25, 40, 60, 127]

    numTrials = 3
    numFrames = 180
    frameGroupSize = 3  # keep this a multiple of numFrames

    # zrfDir = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/zrfFiles/"
    zrfDir = "D:/Code/zrfFiles/"

    subjectPaths = ["AlexFingerMovement/FingerMovement/",
                    "AlexRightFingerMovement/RightFingerMotions/",
                    "fm_sub1_l/FingerMotions/",
                    "fm_sub1_r/FingerMotions/",
                    "fm_sub2_l/FingerMotions/",
                    "fm_sub2_r/FingerMotions/",
                    "fm_sub3_l/FingerMotions/",
                    "fm_sub3_r/FingerMotions/",
                    "fm_sub4_l/FingerMotions/",
                    "fm_sub4_r/FingerMotions/"]

    for subjectPath in subjectPaths:
        filePath = zrfDir + subjectPath

        print(subjectPath)
        print('numSegments', numSegments)
        print('numZcolumns', numZColumns)
        print('numTrials', numTrials)
        print('numFrames', numFrames)
        print('frameGroupSize', frameGroupSize)

        worksheet = workbook.add_worksheet(subjectPath.split('/')[0])

        for zcols in numZColumns:

            samples = pc.DataContainer()  # container to put extracted features and samples to then classify

            for fingerID in range(len(fingerNames)):
                for trial in range(1, numTrials + 1):
                    # print(fingerNames[fingerID], trial, end=' ')
                    # print_elapsed_time(startTime)

                    # load ultrasound .zrf file
                    zrf = ZRFData(filePath + fingerNames[fingerID] + str(trial) + '.zrf')

                    # extract linear fit features for each frame of the rf data
                    linearFitFeatures = []  # list of a list: [numFrames][zcols*numSegments*2]; 2 is the slope and intercept value
                    for frame in range(numFrames):
                        # extract_linear_fit_segments returns a list of features extracted for the given frame
                        linearFitFeatures.append(pc.exctract_linear_fit_segments(zrf.rfsig[:, :, frame],  # rf signal
                                                                                 zcols,  # num z columns to average to
                                                                                 numSegments))  # number of depth segments

                    # obtain difference between frames as features
                    differenceFeatures = []  # same as linearFitFeatures
                    for frame in range(numFrames):
                        frameFeatures = []
                        for feature in range(len(linearFitFeatures[frame])):
                            frameFeatures.append(
                                linearFitFeatures[frame][feature] - linearFitFeatures[frame - 1][feature])
                        differenceFeatures.append(frameFeatures)

                    # group frames into group size and obtain min/max values and add the samples to data set
                    for frame in range(0, numFrames, frameGroupSize):
                        frameFeatures = []
                        for feature in range(len(linearFitFeatures[frame])):
                            frameFeatures.append(
                                np.min([linearFitFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.max([linearFitFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.min([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))
                            frameFeatures.append(
                                np.max([differenceFeatures[frame + f][feature] for f in range(frameGroupSize)]))

                        # each frame is represented as a sample
                        samples.add_sample(np.asarray(frameFeatures), fingerID, trial)

            ############################
            """Pattern Classification"""
            ############################

            # Normalize Data
            samples.X = StandardScaler().fit_transform(samples.X)

            # Feature Selection
            a = 0.05
            selector = SelectFwe(f_classif, alpha=a)
            print("Feature Selector: Fwe alpha =", a)

            # Classifier
            lda = LinearDiscriminantAnalysis()

            cnf_matrix = None
            y_pred = []
            y_test = []
            crossvalacc = []

            # classify with test samples represented as the current trial number
            for trial in range(1, numTrials + 1):
                samples.test_train_split_by_trial(trial)

                # Remove features
                X_train_new = selector.fit_transform(samples.X_train, samples.y_train)
                X_test_new = selector.transform(samples.X_test)

                pred = lda.fit(X_train_new, samples.y_train).predict(X_test_new)

                for s in range(len(pred)):
                    y_pred.append(pred[s])
                    y_test.append(samples.y_test[s])

                print('Trial', trial)
                print('Features:', len(samples.X_train[0]), '->', len(X_train_new[0]))
                trial_cnf = confusion_matrix(samples.y_test, pred)
                crossvalacc.append(accuracy_score(samples.y_test, pred))
                print(trial_cnf)
                print(accuracy_score(samples.y_test, pred))

            print('# zcols:', zcols)
            print('# samples:', len(samples.X))
            print('# features:', len(samples.X[0]))
            cnf_matrix = confusion_matrix(y_test, y_pred)
            print(cnf_matrix)
            print('Accuracy: {} +/- {} %'.format(np.mean(crossvalacc) * 100, np.std(crossvalacc) * 100))
            print_elapsed_time(startTime)

            write_cnf_worksheet(worksheet, cnf_matrix, zcols, np.mean(crossvalacc), np.std(crossvalacc),
                                (zcols - 1) * 6, 0)
    workbook.close()

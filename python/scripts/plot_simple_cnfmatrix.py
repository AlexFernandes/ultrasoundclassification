import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import itertools

if __name__ == '__main__':
    fingernames = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    cmap = plt.cm.Blues
    LDA_cm = np.asarray([[520, 59, 0, 0, 21],
                         [98, 502, 0, 0, 0],
                         [0, 22, 535, 21, 22],
                         [0, 0, 22, 497, 81],
                         [0, 0, 34, 22, 544]])

    MLP_cm = np.asarray([[561, 39, 0, 0, 0],
                         [0, 579, 21, 0, 0],
                         [21, 59, 457, 42, 21],
                         [0, 0, 21, 579, 0],
                         [0, 0, 0, 0, 600]])

    print(LDA_cm)
    for i in range(5):
        print(np.sum(LDA_cm[i]))
    print(MLP_cm)
    for i in range(5):
        print(np.sum(MLP_cm[i]))

    # font = {'family': 'normal',
    #         'weight': 'normal',
    #         'size': 12}
    # matplotlib.rc('font', **font)

    fig, ax = plt.subplots(ncols=2)
    im = ax[0].imshow(LDA_cm, interpolation='nearest', cmap=cmap)
    im.set_clim(0, 600)
    # ax[0].set_title('(a)')
    tick_marks = np.arange(len(fingernames))
    ax[0].set_xticks(tick_marks)
    ax[0].set_yticks(tick_marks)
    ax[0].set_xticklabels(fingernames, rotation=45)
    ax[0].set_yticklabels(fingernames)
    fmt = '.2f'
    thresh = LDA_cm.max() / 2.
    for i, j in itertools.product(range(LDA_cm.shape[0]), range(LDA_cm.shape[1])):
        ax[0].text(j, i, '{:,}'.format(LDA_cm[i, j], fmt),
                     horizontalalignment="center",
                     color="white" if LDA_cm[i, j] > thresh else "black")
    plt.tight_layout()
    # ax[0].set_ylabel('True Label')
    # ax[0].set_xlabel('Predicted Label')

    im = ax[1].imshow(MLP_cm, interpolation='nearest', cmap=cmap)
    # ax[1].set_title('(b)')
    ax[1].set_xticks(tick_marks)
    ax[1].set_yticks(tick_marks)
    ax[1].set_xticklabels(fingernames, rotation=45)
    ax[1].set_yticklabels(fingernames)
    thresh = MLP_cm.max() / 2.
    for i, j in itertools.product(range(MLP_cm.shape[0]), range(MLP_cm.shape[1])):
        ax[1].text(j, i, '{:,}'.format(MLP_cm[i, j], fmt),
                     horizontalalignment="center",
                     color="white" if MLP_cm[i, j] > thresh else "black")
    plt.tight_layout()
    # ax[1].set_xlabel('Predicted Label')
    cbar = ax[1].figure.colorbar(im, ax=ax[1])


    plt.show()

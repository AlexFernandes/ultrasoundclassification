"""signalProcessor

Module designed to contain various signal processing methods to 1D-signals and rf signals

"""
import numpy as np
from numpy.polynomial.polynomial import polyfit
from scipy.signal import hilbert
from numba import jit


def linear_fit(sig: np.array(float)) -> (float, float):
    """Linear Regression (y = mx + b) it to 1-D signal

    :param sig: 1-D numpy array
    :return: (slope, y-intercept)
    """
    return polyfit(np.arange(sig.shape[0]), sig, 1)


def remove_dc(sig: np.array(float)) -> np.array(float):
    """Remove any DC offset by subtracting the mean

    :param sig: 1-D numpy array
    :return: 1-D numpy array
    """
    return sig - np.mean(sig)



def gaussian_filter(sig: np.array(float), length: int, sigma: float = 1, mu: float = 0) -> np.array(float):
    """Apply Gaussian Filter (effectively a low pass filter)

    :param sig: 1-D numpy array
    :param length: length of gaussian filter
    :param sigma: standard deviation parameter in gaussian function
    :param mu: mean parameter in gaussian function
    :return: filtered 1-D numpy array
    """
    if length % 2 == 0:
        gauss_filter = np.concatenate(([i for i in range(-int(length / 2) + 1, 0, 1)],
                                       [0],
                                       [i for i in range(0, int(length / 2), 1)]))
    else:
        gauss_filter = [i for i in range(-int(length / 2), int(length / 2) + 1, 1)]

    def gauss_func(x, s, m):
        return (1 / (s * np.sqrt(2 * np.pi))) * np.exp(-0.5 * np.square((x - m) / s))

    for i in range(len(gauss_filter)):
        gauss_filter[i] = gauss_func(gauss_filter[i], sigma, mu)

    sig = np.convolve(sig, gauss_filter, 'same')

    return sig


def hilbert_envelope(sig: np.array(float)) -> np.array(float):
    """Obtain positive envelope of a sinusoidal signal with hilbert transform

    :param sig: 1-D numpy array
    :return: envelope 1-D numpy array
    """
    sig = np.abs(hilbert(sig))
    return sig


def log_compression(sig: np.array(float)) -> np.array(float):
    """Apply log function to signal

    :param sig: 1-D numpy array
    :return: log of 1-D numpy array
    """
    return np.nan_to_num(np.log(sig))


@jit
def average_into_zcols(rfsig: np.ndarray, zcols: int) -> np.ndarray:
    """Given an rf signal from a loaded zrf file, reduce the 127 lateral resolution into zcols lower columns

    :param rfsig: numpy array (1516, 127, 180); (depth, zcol, frame)
    :param zcols: number of zcols to average down to from 127
    :return: numpy array (1516, zcols, 180)
    """
    avgrfsig = np.zeros((rfsig.shape[0], zcols, rfsig.shape[2]))
    for f in range(rfsig.shape[2]):
        # reduce lateral resolution (127 -> # zcols)
        for z in range(zcols):
            for r in range(rfsig.shape[0]):
                s = 0
                for z_orig in range(int(rfsig.shape[1] * z / zcols), int(rfsig.shape[1] * (z + 1) / zcols)):
                    s += rfsig[r, z_orig, f]
                    avgrfsig[r, z, f] = s / (int(rfsig.shape[1] * (z + 1) / zcols) - int(rfsig.shape[1] * z / zcols))
    return avgrfsig


@jit
def average_into_zcols_by_frame(rfsig: np.ndarray, zcols: int) -> np.ndarray:
    """Given an rf signal from a loaded zrf file, reduce the 127 lateral resolution into zcols lower columns

    :param rfsig: numpy array (1516, 127); (depth, zcol)
    :param zcols: number of zcols to average down to from 127
    :return: numpy array (1516, zcols)
    """

    avgrfsig = np.zeros((rfsig.shape[0], zcols))
    # reduce lateral resolution (127 -> # zcols)
    for z in range(zcols):
        for r in range(rfsig.shape[0]):
            s = 0
            for z_orig in range(int(rfsig.shape[1] * z / zcols), int(rfsig.shape[1] * (z + 1) / zcols)):
                s += rfsig[r, z_orig]
            avgrfsig[r, z] = s / (int(rfsig.shape[1] * (z + 1) / zcols) - int(rfsig.shape[1] * z / zcols))
    return avgrfsig

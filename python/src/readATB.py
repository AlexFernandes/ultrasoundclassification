"""readATB

This python module is used to read AlazarTech ATB files which are in binary.
Created based on AlazarDsoGuide-1.1.73.pdf section 4.10 - User Manual which can be found at
https://www.alazartech.com/Product/AlazarDSO/

read_atb is the main method used by other python scripts and modules and the only method you should call unless manually
reading the binary file is necessary.
"""

__author__ = 'Alexander Fernandes'
__version__ = '0.1'

import numpy as np
from typing import Tuple, IO


class FileInfo(object):
    """A container to hold dynamic information when reading a file.

    This class is used to dynamically hold instantaneous variables when reading information from a file. It is
    specifically used to organize data being read by a file using the fread function. The only variable it will always
    have is position, which is used to determine the location it is pointing to in the file.
    """

    def __init__(self):
        self.position = 0  # intended as private variable for purposes of manually reading a binary file in units of bytes


def fread(fid: IO, nelements: int, dtype: np.dtype, finfo: FileInfo) -> np.ndarray:
    """Read specified size of data from file.

    Read desired amount of bytes based on nelements and dtype as element size into a numpy array.
    This function is intended to only be used by read_atb unless you know what you are doing.

    :param fid: file object
    :param nelements: number of dtype elements to read
    :param dtype: size of a single element to read
    :param finfo: FileInfo class that holds external file information
    :type fid: IO
    :type nelements: int
    :type dtype: numpy.dtype
    :type finfo: object

    :return: array of elements read from the file from the last position it read from
    :rtype: numpy.ndarray

    """
    if dtype is np.str:
        dt = np.uint8  # WARNING: assuming 8-bit ASCII for np.str!
    else:
        dt = dtype

    data_array = np.fromfile(fid, dt, nelements)
    data_array.shape = (nelements,)
    if nelements == 1:
        data_array = data_array[0]

    finfo.position += np.dtype(dtype).itemsize * nelements

    return data_array


class CAPTURE_FILE_HEADER(object):
    """Generic class to organize the binary data into easily accessible public instantiated variables

    An ATB file starts with a CAPTURE_FILE_HEADER structure.
    m_dwType
        |Specifies the file type. It must be ‘ATSb’.

    m_dwSize
        |Specifies the size in bytes of the CAPTURE_FILE_HEADER structure.

    m_nBitsPerSample
    m_nBytesPerSample
    m_uSamplesPerRecord
    m_dSamplesPerSecond
    m_dSecondsPerRecord
    m_uTrigDelay_samples
    m_nPreTrig_samples
    m_nRecordsPerCapture
        |Specifies the acquisition properties.

    m_nCapturesPerFile
        |Specifies the number of captures in the file.

    m_nChannels
        |Specifies the total number of CHANNEL_FILE_HEADER structures that follow.

    m_cComment[32]
        |Specifies a user-defined comment for this acquisition.

    m_dwFlags
        |Specifies attributes of the file. These attributes may include:
        |Value       Name                    Meaning
        |0x00000001  CFHF_SAMPLE_INTERLEAVED If this flag is set, samples in a record are channel
        |                                    interleaved (i.e. A-B-A-B). If not set, samples in a
        |                                    record are channel contiguous (i.e. A-A-B-B).
        |0x00000002  CFHF_CONTINUOUS         If set, each record is a transfer in a continuous
        |                                    acquisition.
        |0x00000004  CFHF_STREAM             This flag indicated how the sample data is
        |                                    organized in the file.
        |If this flag is not set, then the records are arranged as described in section 4.10.3.1 “Normalized records”

    m_dwDataOffset_bytes
        |Specifies the offset in bytes from the start of file of the sample data.

    m_dwStreamFlags
        |When the CFHF_STREAM bit is set, then this field contains flags that define how the
        |sample data is organized in the file.

    m_nTimestampMultiplier
        |This value specifies a board specific multiplier used in the conversion of timestamp
        |values from clock ticks to seconds.

    """

    def __init__(self):
        self.m_dwType = None  # type: str -> 4 characters
        self.m_dwSize = None  # type: np.uint32
        self.m_nBitsPerSample = None  # type: np.int32
        self.m_nBytesPerSample = None  # type: np.int32
        self.m_uSamplesPerRecord = None  # type: np.uint32
        self.m_dSamplesPerSecond = None  # type: np.double
        self.m_dSecondsPerRecord = None  # type: np.double
        self.m_nTrigDelay_samples = None  # type: np.int32
        self.m_uPreTrig_samples = None  # type: np.uint32
        self.m_nRecordsPerCapture = None  # type: np.int32
        self.m_nCapturesPerFile = None  # type: np.int32
        self.m_nChannels = None  # type: np.int32
        self.m_cComment = None  # type: str -> 32 characters
        self.m_dwFlags = None  # type: np.uint32
        self.m_dwDataOffset_bytes = None  # type: np.uint32
        self.m_dwStreamFlags = None  # type: np.uint32
        self.m_nTimestampMultiplier = None  # type: np.uint32


class CHANNEL_FILE_HEADER_ARRAY_ELEMENT(object):
    """Generic class to organize the binary data into easily accessible public instantiated variables

    A CHANNEL_FILE_HEADER structure specifies the properties of one input channel.
    A CHANNEL_FILE_HEADER_ARRAY structure follows the CAPTURE_FILE_HEADER structure in an ATB file.
    Note that the m_nChannels field from the CAPTURE_FILE_HEADER defines the number of channel file header
    structures in the file.

    m_bDisabled
        |Specifies if the channel is disabled. If the channel is disabled, then no records from the
        |channel are written to the file.

    m_dInputRange_volts
        |Specifies the input range of the sample data in volts

    m_dProbeAttenuation
        |Specifies the attenuation of the probes used during the acquisition.

    """

    def __init__(self):
        """Class structured as an array according to the number of channels specified

        This class is intended to be an array element encapsulated in a list (i.e. channel 1 indexed as 0)

        """
        self.m_bDisabled = None  # type: bool
        self.m_dInputRange_volts = None  # type: np.double
        self.m_dProbeAttenuation = None  # type: np.double


def read_atb(filename: str):
    """Read AlazarTech ATB binary files
\n
    ATB file format structure:
        1. CAPTURE_FILE_HEADER
        2. CHANNEL_FILE_HEADER array
        3. Binary sample data
\n
    Since the binary file is arranged in this order, read_atb will return them as a Tuple\n
\n
    :return: capture_file_header, channel_file_header_array, sample_data, frame_time_stamps
    :rtype: object, object, np.ndarray, np.array or None

    - capture_file_header: class that holds all header information about the binary file\n
    - channel_file_header_array: array of classes containing header information specific to each channel (zero-indexed)\n
    - sample_data: the signal data stored as a 3D array: (depth, channel, frame)\n
    - frame_time_stamps: if included in the binary file, 1D array of the slow-time timestamp for each frame\n
\n
    :Example:\n
    >>> atb_directory = "<Directory Location>"
    >>> capture_file_header, channel_file_header_array, sample_data, frame_time_stamps = read_atb(atb_directory + "filename.atb")
    >>> import matplotlib.pyplot as plt
    >>> plt.figure(1)
    >>> plt.plot(sample_data[:, 0, 0]) # plot a frame from the first channel
    >>> plt.figure(2)
    >>> plt.imshow(sample_data[:, 0, :], aspect='auto') # plot first channel in m-mode
\n
    **Authors**
        - Alexander Fernandes

    """

    finfo = FileInfo()
    capture_file_header = CAPTURE_FILE_HEADER()
    channel_file_header_array = []
    sample_data = None
    time_stamps = None

    fid = open(filename, 'r')

    ### C-type variables to python numpy types (based on experimenting and based on the documentation)
    # MS-DTYP   |   Numpy-Type (size)
    # DWORD     |   np.uint32
    # int       |   np.int32
    # UINT      |   np.uint32
    # double    |   np.float64
    # WCHAR     |   np.uint16 <- UTF-16
    # BOOL      |   np.uint32

    #
    # Capture File Header
    #

    # Specifies the file type. It must be 'ATSb' for ATS binary type
    capture_file_header.m_dwType = ''.join([chr(fread(fid, 1, np.ubyte, finfo)) for _ in range(4)])

    if capture_file_header.m_dwType == 'ATSb':
        capture_file_header.m_dwSize = fread(fid, 1, np.uint32, finfo)
        capture_file_header.m_nBitsPerSample = fread(fid, 1, np.int32, finfo)
        capture_file_header.m_nBytesPerSample = fread(fid, 1, np.int32, finfo)
        capture_file_header.m_uSamplesPerRecord = fread(fid, 1, np.uint32, finfo)
        m_dwReserved_0 = fread(fid, 1, np.uint32, finfo)
        capture_file_header.m_dSamplesPerSecond = fread(fid, 1, np.float64, finfo)
        capture_file_header.m_dSecondsPerRecord = fread(fid, 1, np.float64, finfo)
        capture_file_header.m_nTrigDelay_samples = fread(fid, 1, np.int32, finfo)
        capture_file_header.m_uPreTrig_samples = fread(fid, 1, np.uint32, finfo)
        capture_file_header.m_nRecordsPerCapture = fread(fid, 1, np.int32, finfo)
        capture_file_header.m_nCapturesPerFile = fread(fid, 1, np.int32, finfo)
        capture_file_header.m_nChannels = fread(fid, 1, np.int32, finfo)
        capture_file_header.m_cComment = ''.join([chr(fread(fid, 1, np.uint16, finfo)) for c in range(32)])
        capture_file_header.m_dwFlags = fread(fid, 1, np.uint32, finfo)
        capture_file_header.m_dwDataOffset_bytes = fread(fid, 1, np.uint32, finfo)
        capture_file_header.m_dwStreamFlags = fread(fid, 1, np.uint32, finfo)
        capture_file_header.m_nTimestampMultiplier = fread(fid, 1, np.uint32, finfo)
        m_dwReserved = fread(fid, 13, np.uint32, finfo)

        #
        # Capture File Header Array
        #

        for channel in range(capture_file_header.m_nChannels):
            channel_file_header_array_element = CHANNEL_FILE_HEADER_ARRAY_ELEMENT
            channel_file_header_array_element.m_bDisabled = bool(fread(fid, 1, np.uint32, finfo))
            m_dwReserved_0 = fread(fid, 1, np.uint32, finfo)
            channel_file_header_array_element.m_dInputRange_volts = fread(fid, 1, np.float64, finfo)
            channel_file_header_array_element.m_dProbeAttenuation = fread(fid, 1, np.float64, finfo)
            m_dwReserved = fread(fid, 4, np.uint32, finfo)

            channel_file_header_array.append(channel_file_header_array_element)

        # Advance to m_dwDataOffset_bytes position in file
        while (finfo.position < capture_file_header.m_dwDataOffset_bytes):
            fread(fid, 1, np.uint8, finfo)

        #
        # Sample Data
        #

        uFramesTotal = int(capture_file_header.m_nRecordsPerCapture * capture_file_header.m_nCapturesPerFile)

        # select sample size in bytes using numpy data types
        sample_dtype = np.byte
        if capture_file_header.m_nBytesPerSample == 1:
            sample_dtype = np.uint8
        elif capture_file_header.m_nBytesPerSample == 2:
            sample_dtype = np.uint16
        elif capture_file_header.m_nBytesPerSample == 4:
            sample_dtype = np.uint32
        elif capture_file_header.m_nBytesPerSample == 8:
            sample_dtype = np.uint64
        elif capture_file_header.m_nBytesPerSample == 16:
            sample_dtype = np.uint128


        bInterleaved = False
        bContinuous = False

        if capture_file_header.m_dwFlags & 0x00000001:  # CFHF_SAMPLE_INTERLEAVED
            bInterleaved = True
        if capture_file_header.m_dwFlags & 0x00000002:  # CFHF_CONTINUOUS
            bContinuous = True

        if capture_file_header.m_dwFlags == 0x00000004:  # CFHF_STREAM
            # TODO: Implement Stream. Follow "Raw records" section 4.10.3.2

            sample_data = np.zeros([capture_file_header.m_uSamplesPerRecord,
                                    capture_file_header.m_nChannels, # first index used as time stamp values
                                    uFramesTotal],
                                   sample_dtype)

            bCBF_AUTODMA = False
            bCBF_NO_PRETRIGGER = False
            bCBF_SAMPLE_INTERLEAVED = False
            bCBF_HAVE_HEADER = False
            bCBF_ADC_CALIBRATION_V_1 = False
            if capture_file_header.m_dwStreamFlags & 0x00000001:
                bCBF_AUTODMA = True
            if capture_file_header.m_dwStreamFlags & 0x00000002:
                bCBF_NO_PRETRIGGER = True
            if capture_file_header.m_dwStreamFlags & 0x00000010:
                bCBF_SAMPLE_INTERLEAVED = True
            if capture_file_header.m_dwStreamFlags & 0x00000004:
                bCBF_HAVE_HEADER = True
            if capture_file_header.m_dwStreamFlags & 0x00000008:
                bCBF_ADC_CALIBRATION_V_1 = True

            # 4.10.3.2.1 AutoDMA from FIFO
            if bCBF_AUTODMA and bCBF_SAMPLE_INTERLEAVED:
                pass  # TODO

            # 4.10.3.2.2 AutoDMA no-pretrigger mode
            elif bCBF_AUTODMA and not bCBF_SAMPLE_INTERLEAVED and bCBF_NO_PRETRIGGER:
                pass  # TODO

            # 4.10.3.2.3 AutoDMA pre-trigger mode
            elif bCBF_AUTODMA and not bCBF_SAMPLE_INTERLEAVED and not bCBF_NO_PRETRIGGER:
                # In this mode sample data is organized into records where a record from Ch A (if enabled) is followed
                # by a record from Ch B (if enabled). In total, there are m_nRecordsPerCapture for Ch A (if enabled),
                # and m_nRecordsPerCapture records for Ch B (if enabled).
                for frame in range(uFramesTotal):
                    if bCBF_HAVE_HEADER:
                        # TODO
                        pass
                    for channel in range(int(capture_file_header.m_nChannels)):
                        sample_data[:, channel, frame] = fread(fid,
                                                               int(capture_file_header.m_uSamplesPerRecord),
                                                               sample_dtype,
                                                               finfo)

        else:
            # Normalized records

            # [depth, (timestamp, channel A, channel B, ...), frames]
            sample_data = np.zeros([capture_file_header.m_uSamplesPerRecord,
                                    capture_file_header.m_nChannels, # first index used as time stamp values
                                    uFramesTotal],
                                   sample_dtype)
            if not bContinuous:
                time_stamps = np.zeros([uFramesTotal], np.float64)
            if bInterleaved:
                # Channel Interleaved

                # Note: timestamp only included if CFHF_CONTINUOUS flag is false
                # Capture 0 Record 0 Timestamp -> (only 1 value)
                # Capture 0 Record 0 Ch A & B & ... samples -> (m_nSamplesPerRecord * m_nChannels long)
                # Capture 0 Record 1 Timestamp
                # Capture 0 Record 1 Ch A & B & ... samples
                # ...
                # Capture 0 Record N-1 Timestamp
                # Capture 0 Record N-1 Ch A & B & ... samples
                # Capture 1 Record 0 Timestamp
                # Capture 1 Record 0 Ch A & B & ... samples
                # ...

                # TODO: implement interleaved
                pass

            else:
                # Channel Contiguous

                # Note: timestamp only included if CFHF_CONTINUOUS flag is false
                # Capture 0 Record 0 Timestamp -> (only 1 value)
                # Capture 0 Record 0 Ch A samples -> (m_nSamplesPerRecord long)
                # Capture 0 Record 0 Ch B samples
                # Capture 0 Record 1 Timestamp
                # Capture 0 Record 1 Ch A samples
                # Capture 0 Record 1 Ch B samples
                # ...
                # Capture 0 Record N-1 Timestamp
                # Capture 0 Record N-1 Ch A samples
                # Capture 0 Record N-1 Ch B samples
                # Capture 1 Record 0 Timestamp
                # Capture 1 Record 0 Ch A samples
                # Capture 1 Record 0 Ch B samples
                # ...

                for frame in range(uFramesTotal):
                    if not bContinuous:
                        time_stamps[frame] = fread(fid, 1, np.float64, finfo)
                    for channel in range(int(capture_file_header.m_nChannels)):
                        sample_data[:, channel, frame] = fread(fid,
                                                               int(capture_file_header.m_uSamplesPerRecord),
                                                               sample_dtype,
                                                               finfo)
    else:
        print("The file type is not 'ATSb'!\n\n")

    fid.close()

    return (capture_file_header, channel_file_header_array, sample_data, time_stamps)

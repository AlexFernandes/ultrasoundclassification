"""zrfPlot

Plot ultrasound signals
"""

from python.src.zrfManager import ZRFData
from matplotlib import pyplot as plt
import numpy as np
from matplotlib.widgets import Button
import itertools

class BModeGui(object):
    """B-mode specific graphical user interface

    """
    def __init__(self):
        self.play = False
        self.zrf = None

        plt.ion()

        self.fig, self.ax = plt.subplots()
        self.im = self.ax.imshow(np.zeros((758, 127)), aspect='auto', animated=True)
        self.ax.set_xlabel('z-scan')
        self.ax.set_ylabel('depth')
        plt.subplots_adjust(bottom=0.15)

        self.axpause = plt.axes([0.8, 0.01, 0.17, 0.075])
        self.btnpause = Button(self.axpause, label='pause / play')
        self.btnpause.on_clicked(self.toggleplay)

    def load_zrf(self, zrfFile: str):
        """Load and display zrf file

        :param zrfFile: file location + file name + .zrf
        """
        self.frame = 0
        self.ax.cla()
        self.zrf = ZRFData(zrfFile)
        self.im = self.ax.imshow(self.zrf.zsig[:, :, self.frame], aspect='auto', animated=True)
        self.ax.set_title(zrfFile.split('/')[-1])

    def run(self, frameDelay: int =1/3000):
        """play frame by frame

        :param frameDelay: frame by frame delay to control playback speed
        """
        while True:
            plt.sca(self.ax)
            self.im.set_data(self.zrf.zsig[:, :, self.frame])
            plt.draw()
            plt.pause(frameDelay)
            if self.play:
                self.frame += 1
                if self.frame == self.zrf.zsig.shape[2]:
                    self.frame = 0

    def toggleplay(self, event):
        self.play = not self.play

def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, '{:0.4f}'.format(cm[i, j], fmt),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, '{:,}'.format(cm[i, j], fmt),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')




if __name__ == "__main__":
    dataFolder = "/media/alexanderfernandes/6686E8B186E882C3/Users/alexanderfernandes/Code/zrfFiles" \
                 "/fm_sub2_l/FingerMotions/"

    b_gui = BModeGui()
    b_gui.load_zrf(dataFolder + "Pinky2.zrf")
    b_gui.run()

    plt.show()
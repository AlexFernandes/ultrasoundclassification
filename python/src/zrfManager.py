"""zrfManager

This module is intended to load and manage the ultrasound .zrf format data in a coordinated manner, intended as an
intermediate interface with the readZRF.py module for ease of data management.
"""

from python.src.readZRF import read_zrf, FileInfo
import numpy as np

class ZRFData(object):
    """A data object to manage loading .zrf specific format files

    Public instance variables if direct access is needed
    rfsig = ... # type: np.array([depth, zscan, frame], dtype=float) (1516, 127, 180)
    zsig = ... # type: np.array([depth, zscan, frame], dtype=float) (758, 127, 180)
    finfo = ... # type: FileInfo
    """
    def __init__(self, zrfFile: str, verbose=False):
        """Load .zrf file

        :param zrfFile: string variable of zrf file including location
        :param verbose: print loading file statement
        """
        if verbose:
            print("Loading: " + zrfFile + "\n")

        if zrfFile.endswith(".zrf"):
            self.finfo, self.zsig, self.rfsig = read_zrf(zrfFile, 1, 180)
        else:
            raise Exception("ZRFHandle can only load '.zrf' type files.")


class RFContainer(object):
    """A data container to manage loading multiple rf signal data from .zrf files

    _rfcontainer = ... # type: dict(label, rfsig)
    """
    def __init__(self):
        self._rfcontainer = {}

    def load_zrf(self, name: str, zrfFile: str, verbose: bool =False):
        """Load .zrf file but only keep rf signal data

        :param name: string label for the corresponding loaded rfsig data
        :param zrfFile: string variable of zrf file including location
        :param verbose: print loading file statement
        """
        zrf = ZRFData(zrfFile, verbose)
        del zrf.finfo
        del zrf.zsig
        self._rfcontainer[name] = zrf.rfsig

    def get_rfsig(self, name: str) -> np.ndarray:
        """Get rf signal data with corresponding loaded label

        :param name: string label for the corresponding loaded rfsig data
        :return: corresponding rfsig data
        """
        return self._rfcontainer[name]

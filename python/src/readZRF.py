"""readZRF

This python module is based on the MATLAB readZRF function and is intended to work in an almost similar fashion

By calling read_zrf, the function will return: (FileInfo, zsig, rfsig) as a tuple
"""

__author__ = 'Alexander Fernandes'
__version__ = '1.0'

import numpy as np
import os
from typing import Tuple, IO


class FileInfo(object):
    """A container to hold dynamic information when reading a file.

    This class is used to dynamically hold instantaneous variables when reading information from a file. It is
    specifically used to organize data being read by a file using the fread function. The only variable it will always
    have is position, which is used to determine the location it is pointing to in the file.

    **Instantaneous variables assigned by read_zrf**
        The read_zrf function instantiates the following variables to this object:
            - file_type
            - file_ver
            - file_size
            - spset_size
            - spset_ver
            - zrow
            - rfrow
            - col
            - id_size
            - nr_col_frame
            - nr_frames
            - zrow
    """

    def __init__(self):
        self.position = 0 # intended as private variable for purposes of manually reading a binary file


def fread(fid: IO, nelements: int, dtype: np.dtype, finfo: FileInfo) -> np.ndarray:
    """Read specified size of data from file.

    Read desired amount of bytes based on nelements and dtype as element size into a numpy array.
    This function is intended to only be used by read_zrf unless you know what you are doing.

    :param fid: file object
    :param nelements: number of dtype elements to read
    :param dtype: size of a single element to read
    :param finfo: FileInfo class that holds external file information
    :type fid: IO
    :type nelements: int
    :type dtype: numpy.dtype
    :type finfo: object

    :return: array of elements read from the file from the last position it read from
    :rtype: numpy.ndarray
    """
    if dtype is np.str:
        dt = np.uint8  # WARNING: assuming 8-bit ASCII for np.str!
    else:
        dt = dtype

    data_array = np.fromfile(fid, dt, nelements)
    data_array.shape = (nelements,)
    if nelements == 1:
        data_array = data_array[0]

    finfo.position += np.dtype(dtype).itemsize * nelements

    return data_array


def read_zrf(filename: str, begin_frame: int = 1, nr_frames: int = 180) -> Tuple[FileInfo, np.ndarray, np.ndarray]:
    """Read ART.LAB *.zrf file into zsig and rfsig numpy arrays

    This function is used to read ultrasound recordings in the format file type .zrf into numpy arrays.
    finfo, zsig and rfsig are global variables and also the return variables of this function.

    :param filename: name of the <*.zrf> file
    :param begin_frame: Begin frame (1 to max) default = 1
    :param nr_frames: Number of frames to read (1 to max) default = 180
    :type filename: str
    :type begin_frame: int
    :type nr_frames: int
    :returns: finfo, zsig, rfsig
    :rtype: FileInfo, ndarray, ndarray.

    **Return/Global Variables**
        - finfo: FileInfo class used to dynamically store variables read in form the zrf file\n
        - zsig: (depth=758, z_cols=127, frames=180)\n
        - rfsig: (depth=1516, z_cols=127, frames=180)

    **Authors**
        - Original Author: Peter Brands (MATLAB)
        - Rewritten by: Alexander Fernandes (Python)

    :Example:
    >>> zrf_directory = "./data/finger_motions_subject_0/"
    >>> finfo, zsig, rfsig = read_zrf(zrf_directory + "Index1.zrf")

    .. seealso:: Documentation of the :class:`FileInfo` class and the instantaneous variables assigned by read_zrf
    """

    finfo = FileInfo()  # type: FileInfo
    zsig = None  # type: np.ndarray
    rfsig = None  # type: np.ndarray

    fid = open(filename, 'r')

    #
    # Header
    #

    finfo.file_type = fread(fid, 1, np.int32, finfo)
    finfo.file_ver = fread(fid, 1, np.int32, finfo)
    fread(fid, 8, np.int8, finfo)
    finfo.file_size = fread(fid, 1, np.int32, finfo)

    if finfo.file_ver == 0:
        #
        # SPSET
        #

        finfo.spset_size = fread(fid, 1, np.int32, finfo)
        finfo.spset_ver = fread(fid, 1, np.int32, finfo)
        fread(fid, 14, np.int32, finfo)

        #
        # Read raw data struct
        #

        finfo.zrow = fread(fid, 1, np.int32, finfo)
        finfo.rfrow = fread(fid, 1, np.int32, finfo)
        finfo.col = fread(fid, 1, np.int32, finfo)

        #
        # Jump over SPSET
        #

        fread(fid, finfo.spset_size - 19 * 4, np.int8, finfo)

        #
        # Jump over ID struct
        #

        finfo.id_size = fread(fid, 1, np.int32, finfo)
        fread(fid, finfo.id_size - 4, np.int8, finfo)

        #
        # Read number of frames from Z label in raw data
        #

        zl = fread(fid, 16, np.short, finfo)
        finfo.nr_col_frame = zl[5]
        finfo.nr_frames = np.divide(finfo.col, zl[5])
        fid.seek(finfo.position - 32, os.SEEK_SET)
        finfo.position -= 32

        nrbsamp = finfo.zrow * (begin_frame - 1) * finfo.nr_col_frame
        fid.seek(finfo.position + nrbsamp * 2, os.SEEK_SET)
        finfo.position += nrbsamp * 2

        #
        # Read Z-signal (a number of frames)
        #

        nrtsamp = finfo.zrow * finfo.col
        nrlines = nr_frames * finfo.nr_col_frame
        nrlsamp = finfo.zrow * nrlines
        zsig = fread(fid, nrlsamp, np.ushort, finfo).reshape(finfo.zrow, nrlines, order='F')

        nrsamp = nrtsamp - nrbsamp - nrlsamp
        fid.seek(finfo.position + nrsamp * 2, os.SEEK_SET)
        finfo.position += nrsamp * 2

        #
        # Read RF-signal
        #

        nrbsamp = finfo.rfrow * (begin_frame - 1) * finfo.nr_col_frame
        fid.seek(finfo.position + nrbsamp * 2, os.SEEK_SET)
        finfo.position += nrbsamp * 2

        nrlsamp = finfo.rfrow * nrlines
        rfsig = fread(fid, nrlsamp, np.short, finfo).reshape(finfo.rfrow, nrlines, order='F')

    else:

        print('This script can only read ver "%d" files!\n\n' % finfo.file_ver)

    fid.close()

    #
    # Unwrap data into 3 dimensional matrix
    #

    i = zsig[1].argmin()
    r = rfsig[16:, :]
    z = zsig[16:, :]
    N = rfsig.shape[1]
    l = N - i
    r[:, 0:l] = rfsig[16:, i:]
    r[:, l + 1:] = rfsig[16:, 0:i - 1]
    z[:, 0:l] = zsig[16:, i:]
    z[:, l + 1:] = zsig[16:, 0:i - 1]

    # zsig data format
    # M = 758 depth
    # N = 127 scan lines
    # K = 180 frames

    N = 127
    M, K = z.shape
    K /= N
    K = int(K)
    zsig = np.reshape(z, (M, N, K), order='F')

    # rfsig data format
    # M = 1516 depth
    # N =  127 scan lines
    # K =  180 frames

    M = r.shape[0]
    rfsig = np.reshape(r, (M, N, K), order='F')

    return finfo, zsig, rfsig

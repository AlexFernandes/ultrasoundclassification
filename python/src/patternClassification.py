"""patternClassification

This module is designed to handle all feature extracting and processing to output into a format useable by scikit learn
machine learning modules

"""
import numpy as np
import python.src.signalProcessor as sigproc
from pywt._dwt import dwt


def exctract_linear_fit_segments(rfsig: np.ndarray, zcols: int, numsegs: int):
    """Extract along depth, linear fit features from one frame in an rf signal as one sample

    :param rfsig: numpy array with dimensions: (depth, zcol)
    :param zcols: number of zcols to average down to from 127
    :param numsegs: number of linear fits along the full depth of rf signal
    :return: features list of all linear fit segments for all columns of the given frame
    """
    features = []
    sig = sigproc.average_into_zcols_by_frame(rfsig, zcols)
    segsize = int(sig.shape[0] / numsegs)

    for z in range(zcols):
        sig[:, z] = sigproc.remove_dc(sig[:, z])
        sig[:, z] = sigproc.gaussian_filter(sig[:, z], 5)
        sig[:, z] = sigproc.hilbert_envelope(sig[:, z])
        sig[:, z] = sigproc.log_compression(sig[:, z])

        # make depth segments
        segments = []
        for seg in range(numsegs):
            segments.append(sig[seg * segsize:seg * segsize + segsize, z])
        # linear fit segments
        for seg in segments:
            m, b = sigproc.linear_fit(seg)
            features.append(m)
            features.append(b)
    return features


def extract_wavelet_coefficients(rfsig: np.ndarray, zcols: int, waveletname: str, levels: list, W: int):
    """Extract wavelet coefficients from one frame in an rf signal as one sample

    :param rfsig: numpy array with dimensions: (depth, zcol)
    :param zcols: number of zcols to average down to from 127
    :param waveletname: string following PyWavelets wavelet name convention
    :param levels: list of wavelet detail levels to return as features eg. [2,3] returns only 2nd and 3rd levels
    :param W: window size when performing mean(abs())
    :return: single list array in the form of all specified levels along all z columns
    """
    features = []
    maxlvl = np.max(levels)
    sig = sigproc.average_into_zcols_by_frame(rfsig, zcols)

    for z in range(zcols):
        approx = sig[:, z]
        if maxlvl == 0:  # no wavelet decomposition
            meanabs = np.convolve(np.abs(approx), np.ones(W) / W, mode='valid')
            for feature in meanabs[::W]:
                features.append(feature)  # downsample by W
        else:
            for lvl in range(1, maxlvl + 1):
                # recursively obtain wavelet 'detail' coefficients using previous approx coefficients
                (approx, detail) = dwt(approx, waveletname, 'zero')
                # if current lvl is listed in levels, use detail coefficients as features
                if lvl in levels:
                    # if detail.shape[0] % 2 == 0:
                    #     shortdetail = np.mean(np.abs(detail).reshape(-1, 2), axis=1)
                    # else:
                    #     shortdetail = np.mean(np.abs(detail[0:detail.shape[0] - 1]).reshape(-1, 2), axis=1)
                    # for d in shortdetail:
                    #     features.append(d)

                    meanabs = np.convolve(np.abs(detail), np.ones(W)/W, mode='valid')
                    for feature in meanabs[::W]:
                        features.append(feature)  # downsample by W
    return features


def extract_wavelet_linfit(rfsig: np.ndarray, zcols: int, waveletname: str, levels: list, numsegs: int):
    """Extract wavelet coefficients from one frame in an rf signal as one sample then linear fit segments

    :param rfsig: numpy array with dimensions: (depth, zcol)
    :param zcols: number of zcols to average down to from 127
    :param wavelet: string following PyWavelets wavelet name convention
    :param numsegs: number of linear fits along the full depth of rf signal
    :param levels: list of wavelet detail levels to return as features eg. [2,3] returns only 2nd and 3rd levels
    :return: features list
    """
    features = []
    maxlvl = np.max(levels)
    sig = sigproc.average_into_zcols_by_frame(rfsig, zcols)

    for z in range(zcols):
        approx = sig[:, z]
        for lvl in range(1, maxlvl + 1):
            # recursively obtain wavelet 'detail' coefficients using previous approx coefficients
            (approx, detail) = dwt(approx, waveletname, 'zero')
            # if current lvl is listed in levels, use detail coefficients as features
            if lvl in levels:
                # extract linear fit segments
                detail = sigproc.remove_dc(detail)
                detail = sigproc.gaussian_filter(detail, 5)
                detail = sigproc.hilbert_envelope(detail)
                detail = sigproc.log_compression(detail)

                segsize = int(detail.shape[0] / numsegs)

                # make depth segments
                for seg in range(numsegs):
                    m, b = sigproc.linear_fit(detail[seg * segsize:seg * segsize + segsize])
                    features.append(m)
                    features.append(b)
    return features


class DataContainer(object):
    """Class designed to act as a container organizing all sample data with corresponding features extracted

    """

    def __init__(self):
        self.numFeatures = 0

        # data samples format
        self.X = []  # feature information
        self.y = []  # label corresponding to the feature tuple index in X array
        self.trial = []  # keep track of sample's trial number

    def add_sample(self, features: np.ndarray, label: int, trial: int):
        """Add sample by giving corresponding feature values and label
        """
        self.X.append(features)
        self.y.append(label)
        self.trial.append(trial)

    def test_train_split_by_trial(self, trialNum: int):
        """Split X and y data in to train / test split

        :param trialNum: chosen trial number will be represented as the test data

        X and y will be converted into class instantiated variables:
        X_test
        X_train
        y_test
        y_train
        """
        self.X_train = []
        self.X_test = []
        self.y_train = []
        self.y_test = []
        for sample in range(len(self.X)):
            if self.trial[sample] != trialNum:
                self.X_train.append(self.X[sample])
                self.y_train.append(self.y[sample])
            else:
                self.X_test.append(self.X[sample])
                self.y_test.append(self.y[sample])

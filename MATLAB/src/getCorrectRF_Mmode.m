%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Purpose: this function unwraps and M Mode file to give access to the 
%         correct RF scanLine based on a user input. It adjust for the 
%         circular biffer wrapping
%
%NOTE: this only gives access to a single correct RF line at a time. In
%      order to get the corrected image from zsig see funtion unwrapMmode
%
%By: Jason Silver
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [rfsigCorrected] = getCorrectRF_Mmode(rfsig,zsig,scanLine)

[value index]=max(zsig(2,:));

temp1=zsig(2,index);
temp2=zsig(2,index+1);
i=2;

while (true)
  if temp2~=temp1
    index=index+i-1;
    break;
  else
    temp1=temp2;
    temp2=zsig(2,index+i);
  end
    
    i=i+1;
end
 index=index-1;

 if (index+scanLine)<5550
    rfsigCorrected=rfsig(:,index+scanLine);
   
else
    rfsigCorrected=rfsig(:,index-(5550-scanLine));
   
end

end 







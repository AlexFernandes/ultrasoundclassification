function [ f1scores, classacc, cnfmatricies ] = loadScores( filePath, N )
%loadScores
%   Input
%   filePath: string file path to the xlsx file (include the file type)
%   N: averaged RF columns to load (include as list)
%   Output
%   f1scores: [number of N's , number sub arms, number fingers: 5]
%   classacc: [number of N's, number sub arms]
%   cnfmatricies: [number of N's, actual fingers: 5, predicted fingers: 5]

f1scores = zeros(length(N), 10, 5);
classacc = zeros(length(N), 10);
cnfmatricies = zeros(length(N), 5, 5);

for arm = 1:10
    sheet = xlsread(filePath, arm+1);
    for n = 1:length(N)
        cnf_matrix = zeros(5, 5);
        acc = 0;
        for r = 1:5
            for c = 1:5
                cnf_matrix(r, c) = sheet((N(n)-1)*6 + r + 1, c);
                cnfmatricies(n, r, c) = cnfmatricies(n, r, c) + cnf_matrix(r, c);
                if r == c
                    acc = acc + cnf_matrix(r,c);
                end
            end
        end
        classacc(n, arm) = acc / sum(sum(cnf_matrix));
        for f = 1:5
            s1 = sum(cnf_matrix, 1);
            s2 = sum(cnf_matrix, 2);
            p = cnf_matrix(f, f) / s1(f);
            r = cnf_matrix(f, f) / s2(f);
            f1scores(n, arm, f) = 2 * p * r / (p+r);
        end
    end
end
end


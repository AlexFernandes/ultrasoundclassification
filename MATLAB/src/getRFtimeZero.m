%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Purpose: this function gets the time zero index found for use with
%         function getUnwrappedRFsignal
%
%
%
%By: Jason Silver
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [index]=getRFtimeZero(zsig)

[value index]=max(zsig(2,:));

temp1=zsig(2,index);
temp2=zsig(2,index+1);
i=2;
sz = size(zsig);
sz(2);
while (i+index < sz(2))
  if temp2~=temp1
    index=index+i-1;
    break;
  else
    temp1=temp2;
    temp2=zsig(2,index+i);
  end
    
    i=i+1;
end
 index=index-1;

end

function [capture_file_header, channel_file_header_array, sample_data] = readATB(filename)
%readATB Summary of this function goes here
%   Input 
%       filename = filepath include .atb
%   output:
%       capture_file_header = struct
%       channel_file_header_array = array of struct
%       sample_data = capture_file_header.m_uSamplesPerRecord,
%                     capture_file_header.m_nChannels,
%                     uFramesTotal
%   based on AlazarDsoGuide-1.1.73.pdf section 4.10 - User Manual which can 
%   be found athttps://www.alazartech.com/Product/AlazarDSO/
%   Also see readATB.py Python file for a programming reference guide

fid = fopen(filename,'r');

if (fid == -1)
   fprintf(1,'File "%s" does NOT exist in this directory!\n\n',filename)
else
    %
    % Capture File Header
    %
    capture_file_header.m_dwType = fread(fid, 4,'uchar');
    if (capture_file_header.m_dwType == [65;84;83;98])
        capture_file_header.m_dwSize = fread(fid, 1, 'uint32');
        capture_file_header.m_nBitsPerSample = fread(fid, 1, 'int32');
        capture_file_header.m_nBytesPerSample = fread(fid, 1, 'int32');
        capture_file_header.m_uSamplesPerRecord = fread(fid, 1, 'uint32');
        m_dwReserved_0 = fread(fid, 1, 'uint32');
        capture_file_header.m_dSamplesPerSecond = fread(fid, 1, 'float64');
        capture_file_header.m_dSecondsPerRecord = fread(fid, 1, 'float64');
        capture_file_header.m_uTrigDelay_samples = fread(fid, 1, 'int32');
        capture_file_header.m_uPreTrig_samples = fread(fid, 1, 'uint32');
        capture_file_header.m_nRecordsPerCapture = fread(fid, 1, 'int32');
        capture_file_header.m_nCapturesPerFile = fread(fid, 1, 'int32');
        capture_file_header.m_nChannels = fread(fid, 1, 'int32');
        capture_file_header.m_cComment = fread(fid, 32, 'uint16');
        capture_file_header.m_dwFlags = fread(fid, 1, 'uint32');
        capture_file_header.m_dwDataOffset_bytes = fread(fid, 1, 'uint32');
        capture_file_header.m_dwStreamFlags = fread(fid, 1, 'uint32');
        capture_file_header.m_nTimestampMultiplier = fread(fid, 1, 'uint32');
        m_dwReserved = fread(fid, 13, 'uint32');
                
        %
        % Channel file header array
        %
        channel_file_header_array = [];
        for channel = 1:capture_file_header.m_nChannels
            channel_file_header_array_element.m_bDisabled = fread(fid, 1, 'uint32');
            m_dwReserved_0 = fread(fid, 1, 'uint32');
            channel_file_header_array_element.m_dInputRange_volts = fread(fid, 1, 'float64');
            channel_file_header_array_element.m_dProbeAttenuation = fread(fid, 1, 'float64');
            m_dwReserved = fread(fid, 4, 'uint32');
            
            channel_file_header_array = [channel_file_header_array; channel_file_header_array_element];
        end
        
        % Advance to position in file where sample begins
        fread(fid, capture_file_header.m_dwDataOffset_bytes-capture_file_header.m_dwSize-40*capture_file_header.m_nChannels, 'uint8');
        
        
        %
        % Sample Data
        %
        uFramesTotal = capture_file_header.m_nRecordsPerCapture * capture_file_header.m_nCapturesPerFile;
        sample_dtype = 'uint8';
        if (capture_file_header.m_nBytesPerSample == 1)
            sample_dtype = 'uint8';
        elseif (capture_file_header.m_nBytesPerSample == 2)
            sample_dtype = 'uint16';
        elseif (capture_file_header.m_nBytesPerSample == 4)
            sample_dtype = 'uint32';
        elseif (capture_file_header.m_nBytesPerSample == 8)
            sample_dtype = 'uint64';
        end
        
        bInterleaved = false;
        bContinuous = false;
        if (bitand(capture_file_header.m_dwFlags, 0x00000001))
            bInterleaved = true;
        end
        if (bitand(capture_file_header.m_dwFlags, 0x00000002))
            bContinuous = true;
        end
        
        if (bitand(capture_file_header.m_dwFlags, 0x00000004))
            % TODO: Implement Stream. Follow "Raw records" section 4.10.3.2

            sample_data = zeros(capture_file_header.m_uSamplesPerRecord, ...
                                capture_file_header.m_nChannels, ...
                                uFramesTotal,...
                                sample_dtype);
            bCBF_AUTODMA = false;
            bCBF_NO_PRETRIGGER = false;
            bCBF_SAMPLE_INTERLEAVED = false;
            bCBF_HAVE_HEADER = false;
            bCBF_ADC_CALIBRATION_V_1 = false;
            if (bitand(capture_file_header.m_dwStreamFlags, 0x00000001))
                bCBF_AUTODMA = true;
            end
            if (bitand(capture_file_header.m_dwStreamFlags, 0x00000002))
                bCBF_NO_PRETRIGGER = true;
            end
            if (bitand(capture_file_header.m_dwStreamFlags, 0x00000010))
                bCBF_SAMPLE_INTERLEAVED = true;
            end
            if (bitand(capture_file_header.m_dwStreamFlags, 0x00000004))
                bCBF_HAVE_HEADER = true;
            end
            if (bitand(capture_file_header.m_dwStreamFlags, 0x00000008))
                bCBF_ADC_CALIBRATION_V_1 = true;
            end
            
            % 4.10.3.2.1 AutoDMA from FIFO
            if (bCBF_AUTODMA && bCBF_SAMPLE_INTERLEAVED)
                % TODO
            % 4.10.3.2.2 AutoDMA no-pretrigger mode
            elseif (bCBF_AUTODMA && not(bCBF_SAMPLE_INTERLEAVED) && bCBF_NO_PRETRIGGER)
                % TODO
            % 4.10.3.2.3 AutoDMA pre-trigger mode
            elseif (bCBF_AUTODMA && not(bCBF_SAMPLE_INTERLEAVED) && not(bCBF_NO_PRETRIGGER))
                for frame = 1:uFramesTotal
                    if (bCBF_HAVE_HEADER)
                        % TODO
                    else
                        for channel = 1:capture_file_header.m_nChannels
                            sample_data(:, channel, frame) = fread(fid, capture_file_header.m_uSamplesPerRecord, sample_dtype);
                        end
                    end
                end
            end
                
        else
            % Normalized records
            sample_data = zeros(capture_file_header.m_uSamplesPerRecord, ...
                                capture_file_header.m_nChannels, ...
                                uFramesTotal, ...
                                sample_dtype);
            if (bInterleaved)
                % Channel Interleaved
                % Note: timestamp only included if CFHF_CONTINUOUS flag is false
                % Capture 0 Record 0 Timestamp -> (only 1 value)
                % Capture 0 Record 0 Ch A & B & ... samples -> (m_nSamplesPerRecord * m_nChannels long)
                % Capture 0 Record 1 Timestamp
                % Capture 0 Record 1 Ch A & B & ... samples
                % ...
                % Capture 0 Record N-1 Timestamp
                % Capture 0 Record N-1 Ch A & B & ... samples
                % Capture 1 Record 0 Timestamp
                % Capture 1 Record 0 Ch A & B & ... samples
                % ...
                % TODO: implement interleaved
            else
                % Channel Contiguous
                % Note: timestamp only included if CFHF_CONTINUOUS flag is false
                % Capture 0 Record 0 Timestamp -> (only 1 value)
                % Capture 0 Record 0 Ch A samples -> (m_nSamplesPerRecord long)
                % Capture 0 Record 0 Ch B samples
                % Capture 0 Record 1 Timestamp
                % Capture 0 Record 1 Ch A samples
                % Capture 0 Record 1 Ch B samples
                % ...
                % Capture 0 Record N-1 Timestamp
                % Capture 0 Record N-1 Ch A samples
                % Capture 0 Record N-1 Ch B samples
                % Capture 1 Record 0 Timestamp
                % Capture 1 Record 0 Ch A samples
                % Capture 1 Record 0 Ch B samples
                % ...
                
                for frame = 1:uFramesTotal
                    if not(bContinuous)
                        fread(fid, 1, 'float64');
                    end
                    for channel = 1:capture_file_header.m_nChannels
                        sample_data(:, channel, frame) = fread(fid, capture_file_header.m_uSamplesPerRecord, sample_dtype);
                    end
                end
            end
        end
    else
        fprintf("The file type is not 'ATSb'!\n\n")
    end
    
end

end


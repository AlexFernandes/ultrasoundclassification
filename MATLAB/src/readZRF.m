function [finfo,zsig,rfsig] = readZRF(filename, begin_frame, nr_frames)
%[finfo,zsign,rfsig] = readZRF(filename, begin_frame, nr_frames)   Read ART.LAB *.zrf file   
%
%   Input
%   filename    : name of the <*.zrf> file
%   begin_frame : Begin frame (1 to max)
%   nr_frames   : Number of frames to read (1 to max)
%
%   Output
%   finfo       : File info
%   zsig        : Z-signal data matrix
%   rfsig       : RF-signal data matrix
%
%
%   Peter Brands
%

%
%   Open *.zrf file
%
fid = fopen(filename,'r');

%
%   Start interpertation of file
%
if (fid == -1)
   fprintf(1,'File "%s" does NOT exist in this directory!\n\n',filename)
else   
%
%   Header
%
    finfo.file_type    = fread(fid,1,'int');
    finfo.file_ver     = fread(fid,1,'int');
    tmp                = fread(fid,8,'char');
    finfo.file_size    = fread(fid,1,'int');

    if (finfo.file_ver == 0)
%
%   SPSET
%
        finfo.spset_size   = fread(fid,1,'int');
        finfo.spset_ver    = fread(fid,1,'int');
        tmp                = fread(fid,14,'int');
%
%   Read raw data struct
%
        finfo.zrow         = fread(fid,1,'int');
        finfo.rfrow        = fread(fid,1,'int');
        finfo.col          = fread(fid,1,'int');
%
%   Jump over SPSET
%
        tmp                = finfo.spset_size - 19 * 4;
        tmp                = fread(fid,tmp,'char');
%
%   Jump over ID struct
%
        finfo.id_size      = fread(fid,1,'int');
        tmp                = finfo.id_size - 4;
        tmp                = fread(fid,tmp,'char');
%
%   Read number of frames from Z label en raw data
%
        zl                 = fread(fid,16,'short');
        finfo.nr_col_frame = zl(6);
        finfo.nr_frames    = finfo.col ./ zl(6);
        fseek(fid, -32, 0);
        
        nrbsamp            = finfo.zrow * (begin_frame-1) * finfo.nr_col_frame;
        fseek(fid,(nrbsamp.*2),0);
                
%
%   Read Z-signal (a number of frames)
%    
        nrtsamp            = finfo.zrow * finfo.col;
        nrlines            = nr_frames * finfo.nr_col_frame;
        nrlsamp            = finfo.zrow * nrlines;        
        zsig               = reshape(fread(fid,nrlsamp,'ushort'),finfo.zrow,nrlines);
        
        nrsamp             = nrtsamp - nrbsamp - nrlsamp;  
        fseek(fid,(nrsamp.*2),0);
%
%   Read RF-signal
%
        nrbsamp            = finfo.rfrow * (begin_frame-1) * finfo.nr_col_frame;
        fseek(fid,(nrbsamp.*2),0);

        nrlsamp            = finfo.rfrow * nrlines;
        rfsig              = reshape(fread(fid,nrlsamp,'short'),finfo.rfrow,nrlines);

        fclose(fid);
    else
        fprintf(1,'This script can only read ver "%d" files!\n\n',finfo.file_ver)
        fclose(fid);
    end
end

%
%   END
%
    

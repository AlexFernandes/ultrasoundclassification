
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Time Correct art.lab buffer data
%
%Input: raw rfsignal and zsignal
%Output: time corrected rf and zsignal 
% 
%
%Note:only for M Mode
%
%Date: January 8, 2009
%Author: Jason Silver
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [rfCorrected zCorrected]=MmodeUnwrap(rfsig, zsig)
 
 %function to find time zero
 [index]=getRFtimeZero(zsig);
 
 %used to create time corrected signals
 [az bz]=size(zsig);
 [arf brf]=size(rfsig);
 
 %time corrected signals
 rfCorrected=zeros(arf,brf);
 zCorrected=zeros(az,bz);
 
 %rf unwrapping
%  rfCorrected(:,1:(length(rfsig)+1-index))=rfsig(:,index:(length(rfsig)));
%  rfCorrected(:,((length(rfsig)+1-index)+1):length(rfCorrected))=rfsig(:,1:index-1);
 
%  rfCorrected(:,index+1:length(rfsig))=rfsig(:,index:-1:1);
%  rfCorrected(:,1:index)=rfsig(:,length(rfsig):-1:(index+1));

 rfCorrected(:,1:length(rfsig)-index)=rfsig(:,(index+1):length(rfsig));
 rfCorrected(:,length(rfsig)-index+1:length(rfsig))=rfsig(:,1:index);
 
 %zsig unwrapping
%  zCorrected(:,1:(length(zsig)+1-index))=zsig(:,index:(length(zsig)));
%  zCorrected(:,((length(zsig)+1-index)+1):length(zCorrected))=zsig(:,1:index-1);
%  
%  zCorrected(:,index+1:length(zsig))=zsig(:,index:-1:1);
%  zCorrected(:,1:index)=zsig(:,length(zsig):-1:(index+1));
 
  zCorrected(:,1:length(zsig)-index)=zsig(:,(index+1):length(zsig));
 zCorrected(:,length(zsig)-index+1:length(zsig))=zsig(:,1:index);
 
 end
 
 
 
 
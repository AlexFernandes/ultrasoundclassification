function [ zsig, rfsig ] = readZRF_M( fileName,frStart,frEnd)
%READZRF_B Summary of this function goes here
%   Detailed explanation goes here

    if ~exist('frStart','var')
        frStart=1;
    end
    
    if ~exist('frEnd','var')
        frEnd=150;
    end
    

    %------------M Mode----------------------------------- 
    [finfo,zsig,rfsig] = readZRF(fileName, frStart, frEnd);
    %temporal re-organization
    [rfsig,zsig]=MmodeUnwrap(rfsig,zsig);
    %setting ROI 
    startLine=1;%Starting Scan Line
    endLine=5550;%End scan line max=5550
    % ----------------------------------------------------

    lineSize=endLine-startLine+1;

%     startTime=1;%650;%820    %start time 
    endTime=1532;%516%1149;%1319    % end time max 1516
    
    %remove offset and header info from rfsig
    sig=zeros(endTime-17+1,lineSize);
    for i=startLine:endLine
        sig(:,i-startLine+1)=rfsig(17:endTime,i)-mean(rfsig(17:endTime,i));
    end

    %remove header information
    rfsig=sig;
    zsig=zsig(17:finfo.zrow,:);






end


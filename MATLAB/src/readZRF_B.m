function [ zsig, rfsig ] = readZRF_B( fileName,frStart,frEnd)
%READZRF_B Summary of this function goes here
%   Detailed explanation goes here

    if ~exist('frStart','var')
        frStart=1;
    end
    
    if ~exist('frEnd','var')
        frEnd=180;
    end
    [~, zsig, rfsig]=readZRF(fileName,frStart,frEnd);
    [zsig, rfsig]=unwrap(zsig,rfsig);
        
    N=127; %Width of B-mode frame - 127 transducers in probe
    [M, K]=size(zsig);
    K=K/N;
    zsig=reshape(zsig,M,N,K);
    
    M=size(rfsig,1);    
    rfsig=reshape(rfsig,M,N,K);

end

function [zsig2, rfsig2] = unwrap(zsig, rfsig)
    [~,i]=min(zsig(2,:));
    rfsig2=rfsig(17:end,:);
    zsig2=zsig(17:end,:);
    N=size(rfsig,2);
    
    lengthWrapped=N-i+1;
    rfsig2(:,1:lengthWrapped)=rfsig(17:end,i:end);
    rfsig2(:,lengthWrapped+1:end)=rfsig(17:end,1:i-1);

    zsig2(:,1:lengthWrapped)=zsig(17:end,i:end);
    zsig2(:,lengthWrapped+1:end)=zsig(17:end,1:i-1);
end

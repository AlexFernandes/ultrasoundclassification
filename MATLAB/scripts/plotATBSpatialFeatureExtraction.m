clc; clear; close all;

%% Setup
addpath('MATLAB/src/')
zrfFolder = 'D:/Code/atbFiles/1kPRF_FingerFlexions_134channel/';
addpath(zrfFolder)
zrfFile = 'Index7.atb';

% Load RF signal
[capture_file_header, channel_file_header_array, sample_data] = readATB(zrfFile);

Fs = capture_file_header.m_dSamplesPerSecond;
rfChannel = 4;
frame = 1;
depthRange = [320, 1540];
u = double(sample_data(:, rfChannel, frame));
% Format to volts
dSampleZeroValue = bitshift(1, capture_file_header.m_nBitsPerSample - 1) - 0.5;
dSampleMaxValue = bitshift(1, capture_file_header.m_nBitsPerSample - 1) - 0.5;
nSampleBitShift = (capture_file_header.m_nBytesPerSample * 8) - capture_file_header.m_nBitsPerSample;
u(:) = bitshift(u(:), -nSampleBitShift);
dMaxVolts = channel_file_header_array(rfChannel).m_dInputRange_volts * channel_file_header_array(rfChannel).m_dProbeAttenuation;
u(:) = dMaxVolts * double((u(:) - dSampleZeroValue) / dSampleMaxValue);
% DC removal
u(:) = u(:) - mean(u(:));

% Region of interest
roi = u(depthRange(1):depthRange(2));
roi = roi - mean(roi);
%% Envelope and Log Compression
e = roi;
gauss_func = @(x) (1 / sqrt(2 * pi)) * exp(-0.5 * x.^2);
gauss_filter = gauss_func([-2, -1, 0, 1, 2]);
% Remove DC
e(:) = e(:) - mean(e(:));
% Gaussian Filter
e(:) = conv(e(:), gauss_filter, 'same');
% Hilbert Transform
[e(:), ~] = envelope(e(:));
% Log compression
e(:) = log(e(:));

%% FFT
L = length(roi);
Y = fft(roi);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

%% DWT
[A1, D1] = dwt(roi, 'db3');
[A2, D2] = dwt(A1, 'db3');
[A3, D3] = dwt(A2, 'db3');
[A4, D4] = dwt(A3, 'db3');
[A5, D5] = dwt(A4, 'db3');
[A6, D6] = dwt(A5, 'db3');

db3_g = [0.035226291882100656;
         -0.08544127388224149;
         -0.13501102001039084;
         0.4598775021193313;
         0.8068915093133388;
         0.3326705529509569];
db3_h = [-0.3326705529509569;
         0.8068915093133388;
         -0.4598775021193313;
         -0.13501102001039084;
         0.08544127388224149;
         0.035226291882100656];
scal1 = db3_g;
wav1 = db3_h;
wav2 = conv(upsample(wav1, 2), scal1);
scal2 = conv(upsample(scal1, 2), scal1);
wav3 = conv(upsample(wav2, 2), scal2);
scal3 = conv(upsample(scal2, 2), scal2);
wav4 = conv(upsample(wav3, 2), scal3);
scal4 = conv(upsample(scal3, 2), scal3);
wav5 = conv(upsample(wav4, 2), scal4);
scal5 = conv(upsample(scal4, 2), scal4);
wav6 = conv(upsample(wav5, 2), scal5);
scal6 = conv(upsample(scal5, 2), scal5);

[h1, w1] = freqz(wav1, 1);
[h2, w2] = freqz(wav2, 1);
[h3, w3] = freqz(wav3, 1);
[h4, w4] = freqz(wav4, 1);
[h5, w5] = freqz(wav5, 1);
[h6, w6] = freqz(wav6, 1);

%% Plot DWT
% Plot u(i)
maxY = (1*10^4);
figure(1)
plot(u)
ylabel('Amplitude (V)')
xlabel('Depth (sample)')
grid on;
set(gca, 'fontsize', 14);

% Plot freq dwt
dwtcolors = [[0 0 0];
             [0.4660 0.6740 0.1880];
             [0.8500 0.3250 0.0980]; 
             [0.9290 0.6940 0.1250]; 
             [0.4940 0.1840 0.5560];
             [0.6350 0.0780 0.1840]];
figure(2)
semilogx(f, P1 / max(P1));
hold on;
semilogx(w1 * Fs/(2*pi), abs(h1) / max(abs(h1)), '--', 'LineWidth', 2, 'Color', dwtcolors(1, :))
semilogx(w2 * Fs/(2*pi), abs(h2) / max(abs(h2)), '--', 'LineWidth', 2, 'Color', dwtcolors(2, :))
semilogx(w3 * Fs/(2*pi), abs(h3) / max(abs(h3)), '--', 'LineWidth', 2, 'Color', dwtcolors(3, :))
semilogx(w4 * Fs/(2*pi), abs(h4) / max(abs(h4)), '-', 'LineWidth', 2, 'Color', dwtcolors(4, :))
semilogx(w5 * Fs/(2*pi), abs(h5) / max(abs(h5)), '-', 'LineWidth', 2, 'Color', dwtcolors(5, :))
semilogx(w6 * Fs/(2*pi), abs(h6) / max(abs(h6)), '-', 'LineWidth', 2, 'Color', dwtcolors(6, :))
hold off;
ylabel('Amplitude (Normalized)')
xlabel('Frequency (Hz)')
grid on;
set(gca, 'fontsize', 14);

% Plot detail coefficients
figure(3)
maxmin = 1;
linewidth = 2;
markersize = 12;

subplot(7, 1, 1)
plot(roi, 'LineWidth', linewidth);
ylabel('ROI')
axis([1, length(roi), -maxmin, maxmin])
grid on;
set(gca, 'fontsize', 14);

subplot(7, 1, 2)
plot(D1, 'Color', dwtcolors(1, :), 'LineWidth', linewidth);
ylabel('D1')
axis([1, length(D1), -maxmin, maxmin])
grid on;
set(gca, 'fontsize', 14);

subplot(7, 1, 3)
plot(D2, 'Color', dwtcolors(2, :), 'LineWidth', linewidth);
ylabel('D2')
axis([1, length(D2), -maxmin, maxmin])
grid on;
set(gca, 'fontsize', 14);

subplot(7, 1, 4)
plot(D3, 'Color', dwtcolors(3, :), 'LineWidth', linewidth);
ylabel('D3')
axis([1, length(D3), -maxmin, maxmin])
grid on;
set(gca, 'fontsize', 14);

subplot(7, 1, 5)
plot(D4, 'Color', dwtcolors(4, :), 'LineWidth', linewidth);
hold on;
plot(D4, '.', 'Color', dwtcolors(4, :), 'MarkerSize', markersize);
hold off;
ylabel('D4')
axis([1, length(D4), -maxmin, maxmin])
grid on;
set(gca, 'fontsize', 14);

subplot(7, 1, 6)
plot(D5, 'Color', dwtcolors(5, :), 'LineWidth', linewidth);
hold on;
plot(D5, '.', 'Color', dwtcolors(5, :), 'MarkerSize', markersize);
hold off;
ylabel('D5')
axis([1, length(D5), -maxmin, maxmin])
grid on;
set(gca, 'fontsize', 14);

subplot(7, 1, 7)
plot(D6, 'Color', dwtcolors(6, :), 'LineWidth', linewidth);
hold on;
plot(D6, '.', 'Color', dwtcolors(6, :), 'MarkerSize', markersize);
hold off;
ylabel('D6')
axis([1, length(D6), -maxmin, maxmin])
grid on;
xlabel('Depth (sample)')
set(gca, 'fontsize', 14);

% %% Plot ENV-LR
% figure(4)
% 
% subplot(2, 1, 1)
% plot(e(:))
% ylabel('e(i)')
% xlabel({'Axial direction (i-axis)', 'a)'})
% grid on;
% set(gca, 'fontsize', 14);
% 
% % Plot segmented region with linear regression
% l = 151; seg = 6; region = seg*l:(seg+1)*l - 1; x = 1:length(region);
% subplot(2, 1, 2)
% scatter(region, e(region))
% P = polyfit(x, e(region)', 1);
% yfit = P(1)*x  + P(2);
% hold on;
% plot(region, yfit, 'r');
% hold off;
% ylabel('e(i)')
% xlabel({'Axial direction (i-axis)', 'b)'})
% grid on;
% set(gca, 'fontsize', 14);


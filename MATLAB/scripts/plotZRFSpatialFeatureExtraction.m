clc; clear; close all;

%% Setup
addpath('MATLAB/src/')
zrfFolder = 'D:/Code/zrfFiles/AlexFingerMovement/FingerMovement/';
addpath(zrfFolder)
zrfFile = 'Pinky1.zrf';

% Load RF signal
[finfo, z, r] = readZRF(zrfFile, 1, 180);


%% Lateral resolution reduction
T = 30 / 10 ^ 9;
Fs = 1 / T;
N = 6; % number of RF signals to reduce down to
rfCol = 3; frame = 1; % which one to plot
r = reshape(r, size(r,1), 127, 180);
r = r(17:1532, :, :); % account for first 16 flat values
u = zeros(1516, N, 180);

% Lateral averaging
M = floor(127/N); % number of RF columns to average
for i = 1:1516
    for j = 1:N
        for k = 1:180
            u(i, j, k) = sum(r(i, (j-1)*M + 1 : j*M, k)) / M;
        end
    end
end

u1 = u(:, rfCol, frame) - mean(u(:, rfCol, frame));

%% Envelope and Log Compression
e = u;
gauss_func = @(x) (1 / sqrt(2 * pi)) * exp(-0.5 * x.^2);
gauss_filter = gauss_func([-2, -1, 0, 1, 2]);
for k = 1:180
    for j = 1:N
        % Remove DC
        e(:, j, k) = e(:, j, k) - mean(e(:, j, k));
        % Gaussian Filter
        e(:, j, k) = conv(e(:, j, k), gauss_filter, 'same');
        % Hilbert Transform
        [e(:, j, k), ~] = envelope(e(:, j, k));
        % Log compression
        e(:, j, k) = log(e(:, j, k));
    end
end

%% FFT
L = length(u1);
Y = fft(u1);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;

%% DWT
[A1, D1] = dwt(u1, 'db3');
[A2, D2] = dwt(A1, 'db3');
[A3, D3] = dwt(A2, 'db3');

db3_g = [0.035226291882100656;
         -0.08544127388224149;
         -0.13501102001039084;
         0.4598775021193313;
         0.8068915093133388;
         0.3326705529509569];
db3_h = [-0.3326705529509569;
         0.8068915093133388;
         -0.4598775021193313;
         -0.13501102001039084;
         0.08544127388224149;
         0.035226291882100656];
scal1 = db3_g;
wav1 = db3_h;
wav2 = conv(upsample(wav1, 2), scal1);
scal2 = conv(upsample(scal1, 2), scal1);
wav3 = conv(upsample(wav2, 2), scal2);
scal3 = conv(upsample(scal2, 2), scal2);
wav4 = conv(upsample(wav3, 2), scal3);
scal4 = conv(upsample(scal3, 2), scal3);
wav5 = conv(upsample(wav4, 2), scal4);
scal5 = conv(upsample(scal4, 2), scal4);
wav6 = conv(upsample(wav5, 2), scal5);
scal6 = conv(upsample(scal5, 2), scal5);

[h1, w1] = freqz(wav1, 1);
[h2, w2] = freqz(wav2, 1);
[h3, w3] = freqz(wav3, 1);
[h4, w4] = freqz(wav4, 1);
[h5, w5] = freqz(wav5, 1);
[h6, w6] = freqz(wav6, 1);

%% Plot Original
maxY = (1*10^4);
figid = 1;
figure(figid); figid = figid+1;
plot(r(:, 10, 1) - mean(r(:, 10, 1)))
hold on
plot(envelope(r(:, 10, 1) - mean(r(:, 10, 1))))
hold off
ylabel('RF Signal')
xlabel('Axial direction')
grid on;
axis([0, 700, -inf, inf])
set(gca, 'fontsize', 14);

% B-mode
[~, rBmode] = readZRF_B(zrfFile, 1, 1);
figure(figid); figid = figid+1;
Bmodeimg = mat2gray(envelope(rBmode - mean(rBmode)));
% x = [0, 40];
% y = [0, 35];
x = [1, 127];
y = [1, 1516];
imagesc(x, y, adapthisteq(Bmodeimg, 'NumTiles', [20 20]))
ylabel('Axial direction')
xlabel('Lateral direction')
colormap('gray')

% M-mode
r_corrected = r;
% manual frame correction
startframe = 115;
r_corrected(:, :, 1:180-startframe) = r(:, :, startframe+1:180);
r_corrected(:, :, 180-startframe+1:180) = r(:, :, 1:startframe); 
scan = 122;
figure(figid); figid = figid+1;
Mmodeimg = mat2gray(envelope(reshape(r_corrected(:, scan, :) - mean(r_corrected(:, scan, :)), 1516, 180)));
imagesc(Mmodeimg);
ylabel('Axial direction')
xlabel('Frame direction')
colormap('gray')

%% Plot DWT
% Plot u(i)
maxY = (1*10^4);
figure(figid); figid = figid+1;
plot(u1)
ylabel('u(i)')
ylabel('RF Signal')
xlabel('Axial direction (i-axis)')
grid on;
axis([0, 700, -inf, inf])
set(gca, 'fontsize', 14);

% Plot freq dwt
dwtcolors = [[0 0 0];
             [0.4660 0.6740 0.1880];
             [0.8500 0.3250 0.0980]; 
             [0.9290 0.6940 0.1250]; 
             [0.4940 0.1840 0.5560];
             [0.6350 0.0780 0.1840]];
figure(figid); figid = figid+1;
semilogx(f, P1 / max(P1));
% hold on;
% semilogx(w1 * Fs/(2*pi), abs(h1) / max(abs(h1)), '-', 'LineWidth', 2, 'Color', dwtcolors(1, :))
% semilogx(w2 * Fs/(2*pi), abs(h2) / max(abs(h2)), '-', 'LineWidth', 2, 'Color', dwtcolors(2, :))
% semilogx(w3 * Fs/(2*pi), abs(h3) / max(abs(h3)), '-', 'LineWidth', 2, 'Color', dwtcolors(3, :))
% semilogx(w4 * Fs/(2*pi), abs(h4) / max(abs(h4)), '--', 'LineWidth', 2, 'Color', dwtcolors(4, :))
% semilogx(w5 * Fs/(2*pi), abs(h5) / max(abs(h5)), '--', 'LineWidth', 2, 'Color', dwtcolors(5, :))
% semilogx(w6 * Fs/(2*pi), abs(h6) / max(abs(h6)), '--', 'LineWidth', 2, 'Color', dwtcolors(6, :))
% hold off;
ylabel('Amplitude')
xlabel({'Frequency (Hz)', 'b)'})
grid on;
axis([10^5, Fs/2 0, 1])
set(gca, 'fontsize', 14);

% Plot detail coefficients
figure(figid); figid = figid+1;
subplot(3, 1, 1)
plot(D1, 'Color', dwtcolors(1, :));
ylabel('D1(i)')
grid on;
axis([-inf, inf, -maxY, maxY])
set(gca, 'fontsize', 14);

subplot(3, 1, 2)
plot(D2, 'Color', dwtcolors(2, :));
ylabel('D2(i)')
grid on;
axis([-inf, inf, -maxY, maxY])
set(gca, 'fontsize', 14);

subplot(3, 1, 3)
plot(D3, 'Color', dwtcolors(3, :));
ylabel('D3(i)')
grid on;
axis([-inf, inf, -maxY, maxY])
xlabel({'Axial direction (i-axis)', 'c)'})
set(gca, 'fontsize', 14);


%% Plot ENV-LR
figure(figid); figid = figid+1;

subplot(2, 1, 1)
plot(e(:, rfCol, frame))
ylabel('e(i)')
xlabel({'Axial direction (i-axis)', 'a)'})
grid on;
axis([0, 1600, 0, inf])
set(gca, 'fontsize', 14);

% Plot segmented region with linear regression
l = 151; seg = 6; region = seg*l:(seg+1)*l - 1; x = 1:length(region);
subplot(2, 1, 2)
scatter(region, e(region, rfCol, frame))
P = polyfit(x, e(region, rfCol, frame)', 1);
yfit = P(1)*x  + P(2);
hold on;
plot(region, yfit, 'r');
hold off;
ylabel('e(i)')
xlabel({'Axial direction (i-axis)', 'b)'})
grid on;
axis([seg*l, (seg+1)*l - 1, 0, inf])
set(gca, 'fontsize', 14);


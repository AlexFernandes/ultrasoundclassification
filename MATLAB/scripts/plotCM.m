% simple script to plot confusion matrix

% cm is based on WUS study using KNN classifier with 4 frame averaging and
% 25 prob averaging.
cm = [600 0 0 0 0; 8 592 0 0 0; 0 41 551 8 0; 0 0 0 600 0; 0 0 0 17 583];

% Plot the confusion matrix
fingernames = {'Thumb', 'Index', 'Middle', 'Ring', 'Pinky'};
h = heatmap(fingernames, fingernames, cm, 'Colormap', gray);
h.XLabel = 'Predicted Label';
h.YLabel = 'True Label';
set(gca,'Fontsize',16);

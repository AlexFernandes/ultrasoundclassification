clc; clear; close all;

%% Setup
addpath('MATLAB/src/')
numberSubArms = 10;
N = 1:127;
[DWTMAV_f1scores, DWTMAV_classacc, DWTMAV_cnfmatricies] = loadScores('data/accuracy/wav-db3_lvl1to3_groupsize-3_dsize-2_fwe-a5.xlsx', N);
[ENVLR_f1scores, ENVLR_classacc, ENVLR_cnfmatricies] = loadScores('data/accuracy/linfit_segs-100_zcol-1to127_groupsize-3.xlsx', N);

%% Plot Box Plot
figure(1);
boxplot(DWTMAV_classacc', N);
grid on;
figure(2);
boxplot(ENVLR_classacc', N);
grid on;